\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}

\title{154 Sous-espaces stables par un endomorphisme ou une famille
d'endomorphismes d'un espace vectoriel de dimension finie.
Applications}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

Dans cette leçon, $\K$ désigne un corps commutatif.

\section{Généralités}

\begin{proposition}
  Soient $u$ un endomorphisme de $E$ espace vectoriel de dimension finie
  non nulle $n$ et $E'$ un sous-espace vectoriel de $E$, stable par $u$
  de dimension $n' > 0$.

  Alors le polynôme de l'endomorphisme $u'$ de $E'$ induit par $u$
  divise le polyôme caractéristique de $u$.
\end{proposition}

\begin{proposition}
  Soient $u$ un endomorphisme de $E$ $\K$ espace vectoriel de dimension
  finie non nulle $n$, $\lambda$ une valeur propre de $u$, $\nu$ l'ordre
  de multiplicité de $\lambda$ et $p$ la dimension du sous-espace propre
  $E_{\lambda}$. Alors : \[ 1 \leqslant p \leqslant \nu \]
\end{proposition}

\begin{theoreme}
  Soit $u$ un endomorphisme de $E$ $\K$ espace vectoriel de dimension
  finie non nulle $n$. Les trois assertions suivantes sont équivalents :
  \begin{enumerate}[label=(\roman*)]
    \item $u$ est diagonalisable ;
    \item il existe une base $e$ de $E$ dans laquelle $u$ est réprésenté
      par une matrice diagonale ;
    \item le polyôme caractéristique $\chi_u$ de $u$ est scindé sur $\K$
      et, pout tout valeur propre de $u$, la dimension du sous-espace
      propre associé est égale à la multiplicité.
  \end{enumerate}
\end{theoreme}

\textsc{Cas particulier :} Tout endomorphisme $u$ d'un $\K$
espace-vectoriel de dimension finie non nulle $n$ admettant $n$ valeurs
propres est diagonalisable.

\section{Polynômes d'endomorphismes}

Définir puissance d'un endomorphisme ;

inclusion des noyaux itérés : $\{0\} \subset \ker u \subset u^2 \dots
\subset \ker u^r = E$

\begin{theoreme}
  L'application $\varphi_u$ qui au polyôme $P$ de $\K[X]$ associe
  l'endomorphisme $P(u)$ est un morphisme de la $\K$-algèbre $\K[X]$
  dans $\mathcal{L}(E)$.
\end{theoreme}

\begin{corollaire}
  L'image de $\K[X]$ par le morphisme d'algèbre $\varphi_u$ est la
  sous-algèbre commutative de $\mathcal{L}[E)$ engendrée par $u$.
\end{corollaire}

\begin{definition}
  On appelle cette sous-algèbre \emph{algèbre des polyômes de
  l'endomorphisme $u$} et on la note $\K[u]$.
\end{definition}

\begin{corollaire} Le noyau de $\varphi_u$ est un idéal de $\K[X]$.
\end{corollaire}

\begin{definition}
  On l'appelle \emph{idéal annulateur de $u$}, et on le note
  $\mathcal{I}_u$.
\end{definition}

\begin{theoreme}
  Soit $u$ un endomorphisme dont le polynôme annulateur $\mathcal{I}_u$
  n'est pas réduit au polynôme nul. Alors il existe un, et un seul
  polynôme unitaire qui engendre $\mathcal{I}_u$.
\end{theoreme}

\begin{definition}
  On appelle \emph{polynôme minimal} le polynôme annulateur unitaire qui
  engendre $\mathcal{I}_u$. On le note $\mu_u$.
\end{definition}

\begin{proposition}
  Tout endomorphisme $u$ d'un $\K$-espace vectoriel $E$ de dimension
  finie non nulle $n$ possède un polyôme minimal.
\end{proposition}

\begin{proposition}
  Soient $u$ un endomorphisme d'un $\K$-espace vectoriel de dimension
  finie non nulle $n$ admettant $\mu_u$ comme polyôme minimal et $E'$ un
  sous-espace vectoriel stable par $u$. Alors l'endomorphisme $u'$
  induit par $u$ sur $E'$ admet un polyôme minimal et $\mu_{u'}$ divise
  $\mu_u$.
\end{proposition}

\begin{proposition}
  Pour tout $P\in \K[X]$, $\ker P(u)$ et $\im P(u)$ sont stables par
  $u$.
\end{proposition}

\begin{proposition}
  Pour tout $(P,\lambda,q) \in \K[X] \times \K \times \mathbf{N}$ : \[
    \ker (u - \lambda \Id)^q \subset \ker (P(u)) - P(\lambda)Id)^q \]
\end{proposition}

\begin{theoreme}
  Soient $E$ un $\K$-espace vectoriel de dimension finie, $u$ un
  endomorphisme de $E$, et enfin, $P_1,\dots,P_p$ des polynômes de
  $\K[X]$ premier entre eux deux à deux et $P$ le produit de ces
  polynômes. On considère les sous-espaces de $E$ stables par $u$.
  \[ \mathcal{N} = \ker P(u) \ \ \mathcal{N}_i = \ker P_i(u),\ 1
  \leqslant i \leqslant p \]
  Alors $\mathcal{N}$ est la somme directe des $\mathcal{N}_i$.
\end{theoreme}

\begin{corollaire}
  Dans le cas particulier où $P \in \mathcal{I}_u$, et en supposant que
  $P(u) = 0$ on a ainsi $\mathcal{N} =E$ et donc $E = \sum_{i=1}^p
  \mathcal{N}_i$.
\end{corollaire}

\begin{theoreme}
  Soient $E$ un $\K$-espace vectoriel de dimension finies non nulle $n$
  et $u$ un endomorphisme de $E$ qui admet un polynôme caractéristique
  de la forme : \[ \chi_u(X) = \prod_{i=1}^p(X - \lambda_i)^{m_i},\
  \text{(les $\lambda_i$ deux à deux différents, les $m_i$ non nuls)} \]
  Alors le polynôme minimal de $u$ s'écrit : \[ \mu_u(X) =
    \prod_{i=1}^p(X - \lambda_i)^{r_i},\ 1 \leqslant r_i \leqslant m_i.
  \]
\end{theoreme}

\begin{definition}
  L'ordre de multiplicité $r_i$ de la racine $\lambda_i$ de $\mu_u$ est
  l'indice de l'endomorphisme $u - \lambda_i e$.
\end{definition}

\begin{proposition}
  Le sous-espace vectoriel de $E$ : \[ N_i = \bigcup_{k=0}^{+\infty}
    \ker (u - \lambda_i e)^k \] est aussi bien le noyau de $(u -
  \lambda_i e)^{r_i}$ que de $(u - \lambda_i e)^{m_i}$.
\end{proposition}

\begin{definition}
  On appelle ce dernier \emph{sous-espace caractéristique (ou spectral)}
  associé à la valeur propre $\lambda_i$.
\end{definition}

\begin{proposition}
  La dimension du sous-espace caractéristique associé à la valeur propre
  $\lambda_i$ est le degré $m_i$ de son polynôme caractéristique.
\end{proposition}

\begin{proposition}
  $E$ est la somme directe des $N_i,\ 1 \leqslant i \leqslant p$.
  L'endomorphisme $u_i$ induit par $u$ sur $N_i$ admet $(X -
  \lambda_i)^{m_i}$ pour polynôme caractéristique et $(X -
  \lambda_i)^{r_i}$ comme polynôme minimal ; l'endomorphisme $v_i = u_i
  - \lambda_i\Id_{N_i}$ est nilpotent d'indice $r_i$.
\end{proposition}

\begin{proposition}
  Un endomorphisme $u$ d'un $\K$-espace vectoriel de dimension finie non
  nulle $n$ est diagonalisable si et seulement si son polynôme minimal
  est scindé sur $\K$ et n'a que des racines simples.
\end{proposition}

\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Densité dans $\Co^0$ des fonctions continues nulles part
dérivables}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}[de Baire]
  Soit $(E,d)$ un espace métrique complet. Alors
  \begin{enumerate}
    \item Si $(\O_n)_{n\in\N^*}$ est une suite d'ouverts denses de $E$,
      alors $\bigcap_{n=1}^{\infty} \O_n$ est encore dense dans $E$.
    \item Si $(\Fe_n)_{n\in\N^*}$ est une suite de fermés d'intérieur
      vide de $E$, alors $\bigcup_{n=1}^{\infty} \Fe_n$ est encore
      d'intérieur vide dans $E$.
  \end{enumerate}
\end{theoreme}

\begin{proposition}
  L'ensemble $A$ des fonctions continues sur $[0,1]$ qui ne sont
  dérivables en aucun point contient une intersection dénombrable
  d'ouverts denses.
\end{proposition}

\section*{Preuve}
\emph{But :} on veut montrer qu'il existe une suite $(\O_i)_{i\in\N}$
d'ouverts denses telle que $\bigcap_{i\in\N} \O_i \subset A$. Comme
$(\Co^0([0,1]), \lVert \cdot \rVert_{\infty})$ est complet, on aura le
résultat.

Soit $B = A^c$ l'ensemble des fonctions dérivables en au moins un point
de $[0,1]$. Soit $f \in B$, alors il existe $x \in [0,1]$ tel que
$\frac{f(x + h) - f(x)}{h}$ est borné quand $h \to 0$.

Posons, pour $n\in \N^*$, \[ \Fe_n = \{ f \in \Co^0([0,1]) : \exists x
    \in [0,1] : \forall y \in [0,1], \lvert f(x) - f(y) \rvert \leqslant
n \lvert x - y \rvert \} . \]

On a donc $B \in \bigcup_{n=1}^{+\infty}\Fe_n$ et nous allons montrer
que $\Fe_n$ est fermé et que $\overset{o}{\Fe_n} = \varnothing$.
\begin{itemize}
  \item $\Fe_n$ est fermé.

    Soit $(f_k)$ une suite de $\Fe_n$ qui converge uniformément vers $f$
    dans $\Co^0([0,1])$. À chaque $f_k$ correspond $x_k \in [0,1]$ tel
    que pour tout $y \in [0,1]$, on ait \[ \lvert f_k(y) - f_k(x_k)
    \rvert \leqslant n \lvert y - x_k \rvert . \] De la suite $(x_k)$ on
    peut extraire une suite $(x_{\varphi(k)})$, convergeant vers $x_0
    \in [0,1]$. Soit $y \in [0,1]$. \begin{align*} \lvert f(x_0) - f(y)
        \rvert & \leqslant \lvert f(x_0) - f(x_k) \rvert + \lvert f(x_k)
        - f_k(x_k) \rvert \\ & + \lvert f_k(x_k) - f_k(y) \rvert +
        \lvert f_k(y) - f(y) \rvert \\ & \leqslant \underbrace{2 \lVert
      f - f_k \rVert_{\infty}}_{\xrightarrow[k\to \infty]{}0} +
      \underbrace{\lvert f(x_0) - f(x_k)
      \rvert}_{\underset{\substack{k\to \infty\\\text{par continuité de }
    f}]}{\to 0}} + \underbrace{n \lvert x_k - y \rvert}_{\underset{k \to
    \infty}{\to n \lvert x_0 - y \rvert}}
    \end{align*}
    Donc $f \in \Fe_n$ et donc $\Fe_n$ est fermé.
  \item $\overset{o}{\Fe_n} = \varnothing$.

    Pour tout $f \in \Fe_n$, pour tout $\varepsilon > 0$, on veut
    montrer que $\B(f,\varepsilon) \cap \Fe_n^c \neq \varnothing$. On a
    \[ \Fe_n^c = \{ f \in \Co^0([0,1]) : \forall x_0 \in  \in [0,1] :
        \exists y \in [0,1], \lvert f(x) - f(y) \rvert > n \lvert x - y
    \rvert \} . \]
    Comme les polynômes sont denses dans les fonctions continues, il
    existe un polynôme $P$ tel que $\lVert P - f \rVert_{\infty}
    \leqslant \frac{\varepsilon}2$. Soit $N$ un entier. On découpe
    l'intervalle $[0,1]$ en $\bigcup_{k=0}^{N-1} \left[ \frac{k}{N} ,
    \frac{k+1}{N} \right]$ et on considère la fonction $g_0$ périodique
    de période $\frac{1}N$. qui sur $\left[ 0 , \frac{1}N \right]$ est
    définie par \[ \begin{cases} g_0(x) = \frac{\varepsilon N}2 x,
        \text{ si } 0 \leqslant x \leqslant \frac{1}{2N} \\ g_0(x) =
        \frac{\varepsilon}2 - \frac{\varepsilon N}2 x, \text{ si }
    \frac{1}{2N} \leqslant x \leqslant \frac{1}{N}. \end{cases} \]
    \begin{center}
      \begin{tikzpicture}
        \draw[->] (-1,0) -- (7.7,0) ;
        \draw[->] (0,-1.6) -- (0,3.6) ;
        \draw (0,0) -- (1.5,1.5) -- (3,0) -- (4.5,1.5) -- (6,0) ;
        \draw [dashed] (0,1.5) -- (1.5,1.5) -- (1.5,0) ;
        \node at (0,1.5) [ left ] { $ \scriptsize \frac{\varepsilon}4
          $ } ;
        \node at (1.5,0) [ below ] { $ \scriptsize \frac{1}{2N} $ } ;
        \node at (3,0) [ below ] { $ \scriptsize \frac{1}{N} $ } ;
      \end{tikzpicture}
    \end{center}
    La fonction $g_0$ est continue sur $[0,1]$ et dérivalbe sauf en un
    nombre fini de points et aux points où elle est dérivable, on a
    $\lvert g'_0(x) \rvert = \frac{\varepsilon N}2$. De plus,
    $\sup_{x\in [0,1]} \lvert g_0(x) \rvert = \frac{\varepsilon}4$.
    Posons $g = P + g_0$, alors \begin{align*} \lVert f - g
      \rVert_{\infty} & \leqslant \lVert f - P \rVert_{\infty} + \lVert
        g \rVert_{\infty} \\ & \leqslant \frac{\varepsilon}2 +
      \frac{\varepsilon}4 \\ & < \varepsilon \end{align*}
    De plus, on a, pour tout $x \in [0,1]$, qu'il existe $y \in [0,1]$
    tel que \begin{align*} \lvert g(y) - g(x) \rvert & \geqslant
    \left\lvert \lvert g_0(y) - g_0(x)\rvert - \lvert P(y) - P(x) \rvert
      \right\rvert \\ & \geqslant \lvert \underbrace{\frac{\varepsilon
    N}2 \lvert x - y\rvert}_{\substack{\text{en utilisant la} \\
    \text{définition de }g_0}} - \underbrace{M \lvert y - x
    \rvert}_{\substack{\text{par le théorème des} \\ \text{accroissements
    finis}}} \rvert \text{ avec } M = \sup_{x\in [0,1]} \lvert P'(x)
  \rvert \\ & \geqslant \lvert \frac{\varepsilon N}2 - M \rvert \lvert x
  - y\rvert .  \end{align*}
  Posons $N$ tel que $ \frac{\varepsilon N}2 - M > n$, c'est-à-d-dire $N
  > \frac{2(n+M)}{\varepsilon}$. On a donc $g \in \B(f,\varepsilon) \cap
  \Fe_n^c$ qui est donc non vide. \qed
\end{itemize}
\end{document}

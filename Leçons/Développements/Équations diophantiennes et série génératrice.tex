\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Équations diophantiennes et série génératrice}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Soient $\alpha_1, \dots \alpha_{p}$ des entiers naturels non nuls
  premiers entre eux dans leur ensemble et $n \in \N$. On note
  $\delta_n$ le nombre de solutions $(n_1, \dots n_p) \in \N^p$ de
  l'équation $\alpha_1 n_1 + \cdots + \alpha_p n_p = n$.

  Alors \[ \delta_n \sim \frac{1}{\prod_{i=1}^p \alpha_i} \frac{n^{p-1}}
  {(p-1)!} . \]
\end{theoreme}

\section*{Preuve}

\begin{proof}[1]
  On pose $f(X) = \sum_{n=0}^{+\infty} \delta_n X^n$ la série formelle
  associée à $(\delta_n)$. Notons $A_n$ l'ensemble des solutions de
  l'équation $\alpha_1 n_1 + \cdots + \alpha_p n_p = n$.
  \begin{align*} f(X) & = \sum_{n=0}^{+\infty} \delta_n X^n =
  \sum_{n=0}^{+\infty} \# A_n X^n & = \sum_{n=0}^{+\infty} \left( \sum_{
  (n_1, \dots n_p) \in A_n } 1 \right) X^n = \sum_{n=0}^{+\infty} \left(
  \sum_{ (n_1, \dots n_p) \in A_n } X^n \right) \\ & =
  \sum_{n=0}^{+\infty} \sum_{ (n_1, \dots n_p) \in A_n } X^{ \alpha_1
  n_1 + \cdots + \alpha_p n_p} & = \sum_{n=0}^{+\infty} \sum_{ (n_1,
  \dots n_p) \in A_n } (X^\alpha_1)^{n_1} (X^\alpha_p)^{n_p} \\ & =
  \sum_{ (n_1, \dots n_p) \in \N^p} (X^\alpha_1)^{n_1}
  (X^\alpha_p)^{n_p} & = \prod_{i=1}^p \sum_{n_i \geqslant 0}
  (X^\alpha_i)^{n_i} = \prod_{i=1}^p \frac{1}{1 - X^{\alpha_i}}
  \end{align*}
  L'avant dernière égalité provient du fait que $\N^p$ est partitionné
  par les valeurs de $\alpha_1 n_1 + \cdots + \alpha_p n_p$.

  $f(X)$ est donc une fraction rationnelle dont les pôles sont les
  racines $\alpha_{i\text{ème}}$ de l'unité. On note $\Pi$ les pôles de
  $f$.
\end{proof}

\begin{proof}[2]
  Soit $\omega \in \Pi$.

  Supposons que $\omega^{\alpha_i} = 1, \forall i \in \llbracket 1, n
  \rrbracket$. Alors, avec l'égalité de Bezout, $\omega = 1$.

  1 est le seul pôle de $f$ de multiplicité $p$. D'où \[ f(X) =
    \frac{\alpha}{(1 - X)^p} + R(X) \text{ où } R(X) = \sum_{\omega \in
    \Pi} \left( \frac{a_{1,\omega}}{\omega - X} + \cdots +
  \frac{a_{p,\omega}}{ (\omega - X)^{p-1}} \right) \] avec $\alpha \in
  \C^*, a_{i,\omega} \in \C$.
\end{proof}

\begin{proof}[3]
  Calculons $\alpha$ :

  On pose $g(X) = (1 - X)^pf(X)$ Alors $\alpha = g(1)$. Or \[ g(X) =
    \prod_{i=1}^p \frac{1 - X}{1 - X^{\alpha_i}} = \prod_{i=1}^p
  \frac{1}{1 + X + \cdots + X^{\alpha_i - 1}} \] donc $g(1) = \frac{1}{
  \prod_{i=1}^p \alpha_i} = \alpha$.
\end{proof}

\begin{proof}[4]
  Évalutons $\frac{1}{(\omega -X)^k}, \omega \neq 0.$

  \[ \frac{1}{(\omega -X)^k} = \frac{1}{\omega} \frac{1}{1 -
    \frac{X}{\omega}} = \frac{1}{\omega} \sum_{n \geqslant 0} \frac{X^n}
    {\omega^n} .\] Dérivons ce résultat $(k-1)$ fois : \[ \frac{(k-1)!}{
    (\omega -X)^k} = \frac{1}{\omega} \sum_{n \geqslant 0} \frac{ n(n-1)
    \cdots (n- (k-1) + 1)X^{n - (k -1)}}{\omega^n} \] autrement dit
    \begin{align*} \frac{1}{(\omega -X)^k} & = \frac{1}{(k-1)!}
    \frac{1}{\omega} \sum_{n \geqslant 0} \frac{(n+k - 1) (n+k - 2) \cdots
    (n + 1)}{\omega^{n+k - 1}} X^n \\ & = \frac{1}{(k-1)!}
    \sum_{n \geqslant 0} \frac{ (n+k - 1)!}{n! \omega^{n+k}} X^n
  \end{align*}
  Si $\lvert \omega \rvert = 1, \frac{ (n+k - 1)!}{n! \omega^{n-k}} =
  O\,(n^{k-1})$. Donc $R(X) = \sum_{n \geqslant 0} a_n X^n$ avec $a_n =
  O\,(n^{p-2})$.

  On en déduit que $\delta_n = \alpha \frac{1}{(p-1)!} \frac{(n+p - 1)!}
  {n!} + O\,(n^{p-2})$.

  On a donc $\delta_n = \frac{1}{\prod_{i=1}^p \alpha_i} \frac{n^{p-1}}
  {(p-1)!} + O\,(n^{p-2})$ et donc \[ \delta_n \sim
  \frac{1}{\prod_{i=1}^p \alpha_i} \frac{n^{p-1}} {(p-1)!} . \]
\end{proof}

\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}


\title{206 Théorèmes du point fixe. Exemples et applications}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

Dans cette leçon, $\K$ désigne un corps commutatif.

On appelle point fixe d'une application $f$ d'un ensemble $E$ dans
lui-même un point $a\in E$ tel que $f(a) = a$.

Une des raisons de cette notion est la suivante :

Si on prend $E$ un espace vectoriel, et $g \colon E \to E$, résoudre en
$x$ l'équation $g(x) = b$ avec $b\in E$ fixé, revient à trouver le ou
les points fixes de la fonction $f$ définie par $f(x) = g(x) + x - b$.

\section{Sur les espaces compacts}

\subsection{Applications continues sur un segment}

\begin{theoreme}
  Soit $[a;b]$ un segment de $\R$. Une application continue de $[a;b]
  \to [a;b]$ possède au moint un point fixe.
\end{theoreme}

\begin{proposition}
  Soit $[a;b]$ un segment de $\R$.

  Soit une application continue $f \colon [a;b] \to \R$, telle que \[
  [a;b] \subset f([a;b]). \] Alors $f$ possède au moins un point fixe.
\end{proposition}

\subsection{Applications continues}

Application : il n'existe pas d'application continue de la boule unité
sur sa frontière.

\begin{theoreme}
  Soit $C$ un convexe fermé d'un espace normé, et $f \colon C \to C$ une
  application continue. Si $\overline{f(C)}$ est compacte, alors $f$
  admet un point fixe.
\end{theoreme}

\subsection{Applications lipschitziennes}

\begin{theoreme}
  Soit $(E,d)$ un espace métrique compact non vide, et une application
  $f \colon E \to E$ telle que : \[ \forall (x,y) \in E^2,\ x \neq y
  \implies d(f(x),f(y)) < d(x,y) .\] Alors $f$ possède un unique point
  fixe $l \in E$. De plus, pour tout $a \in E$, la suite récurrente
  donnée par : \[ u_0 = a ; u_{n+1} = f(u_n)\] converge vers $l$.
\end{theoreme}

Exemple et application : la fonction $\sin$ sur $[0;1]$.

\section{Sur les espaces complets}

\subsection{Le théorème de Picard}

\begin{theoreme}[de Picard]
  Soient $(E,d)$ un espace métrique complet et $f \colon E \to E$ une
  application contractante.

  Alors $f$ admet un unique point fixe $l$. De plus , pour tout $a \in
  E$, la suite récurrente donnée par : \[ u_0 = a ; u_{n+1} = f(u_n)\]
  converge vers $l$.
  \marginpar{Dvpt 1}
\end{theoreme}

\begin{remarques}
  \begin{itemize}[label=\textbullet]
    \item La démonstration fournit une estimation numérique de la
      distance $d(u_n,l)$.
    \item La motivation première de ce théorème a été la résolution
      d'équations différentielles avec le théorème de Cauchy-Lipschitz.
  \end{itemize}
\end{remarques}

\begin{corollaire}
  Soient $(E,d)$ un espace métrique complet et $f \colon E \to E$ une
  application. On suppose qu'il existe $r \in \N^*$ tel que
  l'application itérée $f^r$ soit contractante.

  Alors $f$ admet un unique point fixe.
\end{corollaire}

\subsection{Application à la résolution d'équations différentielles}

Soit $I$ un intervalle de $\R$ et $m\in \N$.

\begin{theoreme}[Cauchy-Lipschitz global]
  Soit $f \colon I \times R^m\to \R^m$ une application continue et
  globalement lipschitzienne en la seconde variable au point $(t_0,y_0)
  \in I\times R^m$.

  Alors le problème de Cauchy $y'(t) = f(t,y(t)), y(t_0) = y_0$ admet
  une unique solution définie sur $I$.
  \marginpar{Dvpt 2}
\end{theoreme}

\begin{exemple}
  Le problème de Cauchy $y'(t) = 3y(t) + t, y(0) = 0$ admet une unique
  solution sur $\R$.
\end{exemple}

\begin{theoreme}[Cauchy-Lipschitz local]
  Soit $f \colon I \times R^m\to \R^m$ une application continue et
  localement lipschitzienne en la seconde variable au point $(t_0,y_0)
  \in I\times R^m$.

  Alors le problème de Cauchy $y'(t) = f(t,y(t)), y(t_0) = y_0$ admet
  une unique solution maximale sur $I$.
\end{theoreme}

\subsection{Applications au calcul différentiel}

\begin{theoreme}
  Soient $E$ et $F$ deux espaces de Banach, $U$ un ouvert de $E$ et
  $f\in \mathcal{C}^1(U,F)$.

  Supposons qu'il existe $a\in U$ tel que $df_a \in GL_c(E,F)$.

  Alors il existe un voisinage ouvert $V$ de $a$ et un voisinage ouvert
  $W$ de $f(a)$ tel que :
  \begin{enumerate}
    \item $f|_V = g$ soit une bijection de $V$ sur $W$ ;
    \item $g^{-1} \in \mathcal{C}^1(W,V)$.
  \end{enumerate}
\end{theoreme}

\begin{theoreme}
  Soient $\Omega$ un ouvert de $\R^n\times\R^m$ et $f \colon \Omega \to
  \R^p$ une fonction de classe $\mathcal{C}^1$. Si $f(a,b) = 0$ et
  $\partial_2 f(a,b)$ est inversible, alors il existe un voisinage
  ouvert de  $\Omega$ de $(a,b)$, un voisinage $V$ de $a$ et une
  application $\varphi \colon V \to R^m$ de classe $\mathcal{C}^1$ tels
  que \[ f(x,y) = 0, \ (x,y) \in U \iff \varphi(x) = y \text{ et } x \in
  V. \] De plus, on a \[ d \varphi(a) = -\partial_2 f^{-1}|_{(a,b)}
  \circ \partial_1 f|_{(a,b)}.\]
\end{theoreme}

\section{Applications}

\subsection{Théorème du point fixe à un paramètre}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique complet non vide, $\Lambda$ un
  espace topologique, et $f: E\times \Lambda \to E$ une application. On
  suppose que pour tout $x\in E, \lambda \mapsto f(x,\lambda)$ est une
  application continue de $\Lambda$ dans $E$, et que pour tout $\lambda
  \in \Lambda, f_{\lambda} : x\mapsto f(x,\lambda)$ est une application
  $k$-contractante de $E$ dans $E$, $k$ étant indépendant de $\lambda$.
  En appellant $\alpha_{\lambda}$ l'unique point fixe de $f_{\lambda}$,
  l'application $\lambda \mapsto \alpha_{\lambda}$ est continue.
\end{theoreme}

\subsection{Résolution d'équations intégrales}

\begin{theoreme}
  Soient $\varphi \colon [a,b] \to \R$ et $K : [a,b] \times [a,b] \to
  \R$ continue et $\lambda \in \R$.

  Alors il existe une unique application continue $f \colon [a,b] \to
  \R$ vérifiant : \[ f(x) = \lambda \int_a^x K(x,y)f(y)dy + \varphi(x).
  \]
\end{theoreme}

\subsection{Application au calcul numérique}

\textsc{Méthode de Newton}

Soit $f : [c,d] \to \R $ de classe $\mathcal{C}^2$ avec $f(c) < 0 <
f(d)$ et $f' > 0$ sur $[c,d]$. On définit la suite récurrente \[ x_n =
F^n(x_0) \text{ et } F(t) = t - \frac{f(t)}{f'(t)} . \]

Alors $f$ admet un unique zéro $a$ et il existe un intervalle $[a -
\varepsilon ; a + \varepsilon ]$ stable par $F$ et que pour tout $x_0
\in [a - \varepsilon ; a + \varepsilon ]$, il existe $0 < C <
\frac1{\varepsilon}$ avec \[ C \lvert x_{n+1} - a \rvert \leqslant (C
\varepsilon)^{2n} . \]

\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\parindent0pt

\usepackage{babel}

\title{103 -- Exemples et applications des notions de sous-groupe
distingué et de groupe quotient}
\author{Vincent-Xavier \bsc{Jumel}}
\date{18/09/2016}

\begin{document}
\maketitle

\section{Sous-groupe distingué}

Automorphisme intérieur : Soit $G$ un groupe. À tout élément $a\in G$,
on peut assoicer l'application $\varphi_a : G \to G$ définie par
$x\mapsto axa^{-1}$.

\begin{proposition}
  L'application ainsi définie est un automorphisme.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item On a $\varphi_e = Id_G$
    \item Soient $(a,b) \in G^2$. $\forall x \in G,\ \varphi_a \circ
      \varphi_b(x) = \varphi_a(bxb^{-1}) = abxb^{-1}a^{-1} =
      (ab)x(ab)^{-1} = \varphi_{ab}(x)$
    \item $\varphi_a$ est une bijection, de bijection réciproque
      $\varphi_{a^{-1}}$.
    \item Étant donné $a\in G$, $\forall (x,y) \in G^2$, on a
      $(axa^{-1})(aya^{-1}) = a (xy) a^{-1}$ ce qui s'écrit aussi
      $\varphi_a(x) \varphi_a(y) = \varphi_a(xy)$.
  \end{itemize}
  $\varphi_a$ est donc bien un endomorphisme bijectif, donc un
  automorphisme.
\end{proof}

L'ensemble des automorphismes intérieurs de $G$ se note
$\mathcal{I}(G)$.

\begin{proposition}
  $\mathcal{I}(G)$ est un sous-groupe de $\mathop{Aut}(G)$
\end{proposition}
\begin{proof}
  L'égalité $\varphi_a \circ\varphi_b = \varphi_{ab}$ permet, en
  considérant l'application $a \mapsto \varphi_a$ qui peut-être vue
  comme un morphisme de groupe, de conclure que $\mathcal{I}(G)$ est un
  sous-groupe de $\mathop{Aut}(G)$.
\end{proof}

En cherchant le noyau de $a \mapsto \varphi_a$, on introduit $Z(G) = \{
a\in G \mid \forall x\in G, \ ax = xa \}$ qui l'ensemble des éléments
qui commutent avec tous les éléments de $G$. On l'appelle \emph{centre
de $G$}. $Z(G)$ est un sous-groupe de $G$.

$G$ est commutatif si et seulement si $Z(G) = G \iff \mathcal{I}(G) =
\{Id_G\}$ comme unique élément.

\begin{definition}
  On dit que deux éléments $x$ et $x'$ (resp. deux parties $X$ et $X'$)
  d'un groupe $G$ sont conjugués (par automorphisme intérieur) si et
  seulement s'il existe $a\in G$ tel que : $x' = axa^{-1}$ (resp. $X' =
  aXa^{-1}$, cad $X' = \varphi_a(X)$).
\end{definition}

\begin{definition}[sous-groupes distingués] On appelle sous-groupe
  distingué (ou invariant) d'un groupe $G$ tout sous-groupe $H$ qui est
  stable par tout automorphisme intérieur de $G$. On note $H
  \vartriangleleft G $
\end{definition}
On peut retenir : $H \vartriangleleft G \iff \forall (a,h) \in G\times
H, aha^{-1} \in H$.

\begin{proposition}
  Un sous-groupe distingué $H$ du groupe $G$ est non seulement stable,
  mais invariant par tout automorphisme intérieur de $G$.
\end{proposition}
\begin{proof}
  Soit $H \vartriangleleft G$. Il faut vérifier que $H \subset aHa^{-1}$
  ce qui nous donnera l'égalité avec le résultat précédent.

  Considérons un élement $(a,h)$ de $G\times H$.
  $\varphi_a\circ\varphi_{a^{-1}} = Id_G$ permet d'écrire que $h$ est
  l'image par $\varphi_a$ de $\varphi_{a^{-1}}(h)$ qui est un élément de
  $H$ par la stabilité de $\varphi_{a^{-1}}(h)$.
\end{proof}

On peut retenir : $H \vartriangleleft G \iff aH = Ha$.

\begin{exemples}
  \begin{enumerate}[label=\alph*)]
    \item Soit $G$ un groupe d'éléments neutre $e$. $\{e\}$ est un sous
      groupe distingué de $G$.
    \item Tout sous-groupe d'un groupe abélien est distingué.
    \item Le sous-groupe $\mathcal{I}(G)$ est un sous-groupe distingué
      de $\mathop{Aut}(G)$.
      \begin{proof} On vérifie en effet : $u\circ\varphi_a u^{-1} =
        \varphi_{u(a)}$ pour tout $(u,\varphi_a) \in \mathop{Aut}(G)
        \times \mathcal{I}(G)$.
      \end{proof}
  \end{enumerate}
\end{exemples}

\begin{theoreme}Soit $f : G \to G'$ un morphisme de groupes. Si $H$ est
  un sous-groupe distingué de $G$, $f(H)$ est un sous-groupe distingué
  de $F(G)$. Si $H'$ est un sous-groupe distingué de $G'$, alors
  $f^{-1}(H)$ est un sous-groupe distingué de $G$.
  \begin{proof}
    \begin{itemize}
      \item Soit $H \vartriangleleft G$. Il faut montrer que \[\forall
        (a',h') \in f(G) \times F(H)\ a'h'a'^{-1} \in f(H)\]
        c'est-à-dire : \[\forall (a,h) \in G\times H \
        f(a)f(h)(f(a))^{-1} \in f(H),\] or $f(a)f(h)(f(a))^{-1}$ est
        l'image par $f$ de $aha^{-1}$ qui est dans $H$ car $(a,h) \in G
        \times H$.
      \item Soit $H' \vartriangleleft G'$. Posons $H = f^{-1}(H')$. Il
        faut montrer que \[\forall (a,h) \in G \times H, \ aha^{-1}  \in
        H. \] Cela résulte de ce que $f(aha^{-1})$, qui s'écrit
        $f(a)f(h)(f(a))^{-1}$, est un élément de $H'$.
    \end{itemize}
  \end{proof}
\end{theoreme}

\begin{corollaire}
  Le noyau d'un morphisme de groupes $f : G \to G'$ est un sous-groupe
  distingué de $G$.
  \begin{proof}
    $\ker f = f^{-1}(\{e'\})$ et $\{e'\}$ est un sous-groupe distingué
    de $G'$.
  \end{proof}
\end{corollaire}

\section{Groupe quotient}

\begin{theoreme}
  Soit $G$ un groupe. Toute relation d'équivalence sur $G$ compatible
  avec la loi interne de $G$ est de la forme $x^{-1}y \in H$ où $H$ est
  un sous-groupe de $G$. Réciproquement, tout relation de ce type est
  une relation d'équivalence compatible avec la loi de $G$.
\end{theoreme}
\begin{proof}
  Soit $\mathscr{R}$ une relation d'équivalence compatible à gauche avec
  la loi de $G$. Désignons par $H$ la classe d'équivalence de l'élément
  neutre $e$ de $G$.

  $x\mathscr{R}y \implies x^{-1}x\mathscr{R}x^{-1}y \implies
  e\mathscr{R}x^{-1}y \implies x^{-1}y \in H$

  Les implications réciproques étant également vraies on a : \[ \forall
  (x,y) \in G^2, \  x\mathscr{R}y \iff x^{-1}y \in H. \] Montrons que
  $H$ est un sous-groupe de $G$. De $e \in H$, on déduit que $H \neq
  \varnothing$. Soit $(x,y) \in H^2$. \[ x\mathscr{R}e \wedge
  e\mathscr{R}y \implies x\mathscr{R}y \implies x^{-1}y \in H. \]
\end{proof}
\begin{proof}
  Réciproquement, soit $H$ un sous-groupe de $G$. Désignons par
  $\mathscr{R}$ la relation $x^{-1}y \in H$.

  Elle est symétrique car si $x^{-1}y \in H$ alors son inverse
  $(x^{-1}y)^{-1} = y^{-1}x \in H$.

  Elle est transitive car si $x^{-1}y \in H$ et $y^{-1}z \in H$ alors
  $x^{-1}z \in H$.
\end{proof}

\begin{definition}
  Soient $G$ un groupe et $H$ un sous-groupe, $a$ un élément de $G$.
  L'ensemble \[ aH = \{ah \mid h\in H \} \] est dit \emph{classe à
  gauche} de $a$ suivant $H$.
\end{definition}

\begin{proposition}
  La classe $aH$ est la classe d'équivalence de la relation \[ \forall
  (x,y) \in G^2, \  x\mathscr{R}_gy \iff x^{-1}y \in H. \]
\end{proposition}

\begin{remarques}
  \begin{itemize}
    \item Les classes à droite s'écrivent $Ha = \{ ha \mid h\in H \}$.
    \item Le relation d'équivalence associée est \[ \forall (x,y) \in
      G^2, \  x\mathscr{R}_dy \iff yx^{-1} \in H. \]
    \item L'ensemble des inverse de $aH$ est $Ha^{-1}$.
    \item Il existe une bijection de $G/\mathscr{R}_g$ sur $G/
      \mathscr{R}_d$.
  \end{itemize}
\end{remarques}

Supposons que l'un des $G/\mathscr{R}_g$ soit fini. Alors
$G/\mathscr{R}_d$ l'est aussi et de même cardinal.

\begin{definition}
  On appelle \emph{indice d'un sous-groupe $H$ dans $G$} le nombre noté
  $(G:H) = \#(G/\mathscr{R}_g)$.
\end{definition}

\begin{theoreme}
  Soit $G$ un groupe fini et $H$ un sous-groupe de $G$. L'ordre de $H$
  divise l'ordre de $G$ et le quotient de l'ordre de $G$ par celui de
  $H$ est l'indice $(G : H)$
\end{theoreme}

\begin{remarque}
  Si $K$ est un sous-groupe de $H$, on a : $(G:K) = (G:H) \cdot (H:K)$
\end{remarque}

\begin{theoreme} Soit $G$ un groupe. Les relations d'équivalence
  compatible avec la loi de $G$ sont les relations de la forme $x^{-1}y
  \in H$, où $H$ est un sous-groupe distingué de $G$.
  \begin{proof}
    Soit $\mathscr{R}$ une relation d'équivalence et $H$ la classe de
    l'élément neutre. D'après ce qui précéde, $H$ est un sous-groupe de
    $G$ et pour tout $x\in G$, $x\mathscr{R}y$ équivaut à la fois à $y
    \in xH$ et $y \in Hx$. On a donc \[ \forall x \in G,\ xH = Hx \] ce
    qui s'écrit aussi $H \vartriangleleft G$.
  \end{proof}
  \begin{proof}
    Inversement pour tout sous-groupe distingué $H \vartriangleleft G$,
    la relation $x^{-1} \in H$, qui équivaut à $yx^{-1} \in H$, est
    compatible avec la loi de $G$.
  \end{proof}
\end{theoreme}

\begin{remarque}Dans le cas d'un groupe abélien, tout sous-groupe est
  distingué et les relations d'équivalence sont de la forme $x -y \in H$
  où $H$ est un sous-groupe de $G$.
\end{remarque}

\begin{theoreme}
  Soit $G$ un groupe, $H$ un sous-groupe distingué de $G$ et $G/H$
  l'ensemble quotient de $G$ par la relation $x^{-1}y \in H$. Alors
  $G/H$, muni de la loi quotient, est un groupe. La surjection canonique
  $\varphi$ de $G$ sur $G/H$ est un morphisme de groupes. Si, de plus,
  $G$ est abélien, alors $G/H$ est abélien.
\end{theoreme}
\begin{definition}
  Soit $G$ un groupe et $H$ un sous-groupe distingué de $G$.

  L'ensemble quotient $G/H$ est appellé \emph{groupe-quotient}.
\end{definition}

\begin{remarque}
  Avec les notations précédentes, $H$ s'interprète comme $H = \ker
  \varphi$.
\end{remarque}


\begin{theoreme}
  Soient $f : G \to G'$ un morphisme de groupes, $\varphi$ la surjection
  canonique de $G$ sur $G/\ker f$ et $j$ l'injection canonique de
  $\mathop{\mathrm{im}} f$ dans $G'$. Alors il existe un et un seul
  morphisme de groupes $\tilde{f}$ de $G/\ker f$ dans
  $\mathop{\mathrm{im}} f$ tel que $f = j  \circ \tilde{f} \circ
  \varphi$, et c'est un isomorphisme.
\end{theoreme}
On peut résumer le théorème avec un diagramme commutatif :
\[ \begin{tikzcd}
    G \arrow[r, "f"] \arrow[d, "\varphi"] & G' \\
    G/\ker f \arrow [r, red, "\exists \tilde{f}"] & \mathop{\mathrm{im}} f \arrow[u, "j"]
\end{tikzcd} \]

\begin{proof}
  Il «suffit» de remarquer que $G/\ker f$ est en fait $G/\mathscr{R}$ et
  utiliser la décomposition canonique d'un morphisme.
\end{proof}

\textsc{Application :} Le centre $Z(G)$ d'un groupe $G$ en est un
sous-groupe distingué. L'ensemble $\mathcal{I}(G)$ des automorphismes
intérieurs de $G$ est un sous-groupe de $\mathop{Aut} G$, isomorphe à
$G/Z(G)$ ; on a vu $\mathcal{I}(G) \vartriangleleft \mathop{Aut} G$.

\section{Exemple fondamental}

\begin{theoreme}
  Les sous-groupes de $(\mathbf{Z},+)$ sont les $(n\mathbf{Z},+)$, $n\in
  \mathbf{N}$.
\end{theoreme}
\begin{proof} Il est aisé de vérifier que pour tout $n \in \mathbf{N},\
  n\mathbf{Z}$ est un sous-groupe de $\mathbf{Z}$.

  Réciproquement, soit $H$ un sous-groupe de $\mathbf{Z}$.

  Si $H = \{0\}$, on peut écrire $H = 0\mathbf{Z}$. Sinon, il existe un
  élément non nul dans $H$ et son opposé pour constater que $H$ contient
  au moins un entier strictement positif. $H \cap \mathbf{N}^*$ est
  ainsi une partie non-vide de $\mathbf{N}$, elle contient un plus petit
  élément, que nous notons $n$. D'après $\{n\} \subset H$, on a le
  sous-groupe $n\mathbf{Z}$ engendré par $n$ qui est inclus dans $H$.

  Pour tout $x\in H$, une division euclidienne donne \[ x = nq +r \ 0
  \leqslant r < n. \] De $x \in H$ et $nq \in H$, il vient que $r \in
  H$et donc $r = 0$ et donc $x \in n\mathbf{Z}$. Finalement $H \subset
  n\mathbf{Z}$.
\end{proof}

\begin{theoreme}
  Pour tout $n \in \mathbf{N}$, le groupe-quotient
  $\mathbf{Z}/n\mathbf{Z}$ est appelé groupe des entiers rationels
  modulo $n$. Il est monogène. Si $n = 0$, il est isomorphe à
  $\mathbf{Z}$ ; si $n > 0$, il est cyclique d'ordre $n$.
\end{theoreme}

\begin{proof}
  Soit $\varphi_n : \mathbf{Z} \to \mathbf{Z}/n\mathbf{Z}$ la surjection
  canonique. On vérifie : \[ \forall x \in \mathbf{Z},\ \varphi_n(x) = x
  \varphi_n(1). \] $\varphi_n(1)$ est donc générateur de
  $\mathbf{Z}/n\mathbf{Z}$ qui est monogène.
  \begin{itemize}
    \item Si $n=0$, la relation d'équivalence selon le groupe
      $0\mathbf{Z}$ est l'égalité ; $\mathbf{Z}/\{0\} \cong \mathbf{Z}$.
    \item Soit $n \geqslant 1$. Tout $X \in  \mathbf{Z}/n\mathbf{Z}$ est
      l'image par $\varphi_n$ d'un et un seul élément de l'ensemble
      $\{0,1,\dots,n-1\}$, à savoir les restes de la division
      euclidienne par $n$ des entiers qui constituent la classe de $X$.
      La restriction de $\varphi_n$ à $\{0,1,\dots,n-1\}$ est ainsi une
      bijection ; $\mathbf{Z}/n\mathbf{Z}$ a pour cardinal $n$ et peut
      s'écrire $\{\overline{0},\overline{1},\dots,\overline{n-1}\}$, en
      notant $\overline{x}$ pour $\varphi_n(x)$.
  \end{itemize}
\end{proof}

\begin{theoreme}
  Les générateurs de $\mathbf{Z}/n\mathbf{Z}$, ($n \geqslant 1$), sont
  les $\overline{x}$ tels que $0 \leqslant x \leqslant n - 1$ et que $x$
  est premier avec $n$.
\end{theoreme}
\begin{proof}
  Soit $\overline{x}$, avec $0 \leqslant x \leqslant n - 1$, un élément
  de $\mathbf{Z}/n\mathbf{Z}$. Il est générateur si et seulement s'il
  existe $p \in\mathbf{Z}$ tel que $p\overline{x} = \overline{1}$, ce
  qui signifie que $px \equiv 1 [n]$, autrement dit qu'il existe $p$ et
  $q$ tels que $px = qn + 1 \iff px - qn =1$ qui est l'égalité de
  Bézout.
\end{proof}

\begin{theoreme} Soit $G$ un groupe monogène. S'il est fini, il est
  isomorphe à $\mathbf{Z}$ ; s'il est fini, d'ordre $n$, il est
  isomorphe à $\mathbf{Z}/n\mathbf{Z}$. Dans les deux cas, il est
  abélien.
\end{theoreme}
\begin{proof}
  Soit $a$ un générateur de $G$, dont tous les éléments sont de la forme
  $a^p$, ($p\in \mathbf{Z}$). De $a^{p+p'} = a^pa^{p'}$ on en déduit que
  l'application $f$ de $\mathbf{Z}$ dans $G$ définie par $p \mapsto a^p$
  est un morphisme surjectif de groupes ; $G$ est donc isomorphe à
  $\mathbf{Z}/\ker f$, où $\ker f$, qui est un sous-groupe de
  $\mathbf{Z}$ est la forme $k\mathbf{Z}$.

  Si $G$ est infini, il en est de même de $\mathbf{Z}/\ker f$ et donc
  $\ker f = \{0\}$. $f$ est donc injectif et les éléments $a^p$, ($p\in
  \mathbf{Z}$) sont deux à deux distincts ; à la fois surjectif et
  injectif, $f$ est un isomorphisme de $G$ sur $\mathbf{Z}$.

  Si $G$ est fini, il en est de même de $\mathbf{Z}/\ker f$ et donc $k =
  n$. $G \cong \mathbf{Z}/n\mathbf{Z}$ ; on a $G = \{a^0, a^1, \dots,
  a^{n-1}\}$ et $n$ est le plus petit entier $m$ strictement positif tel
  que $a^m = e$.

  Enfin, $G$ isomorphe à un groupe abélien est lui-même abélien.
\end{proof}
\begin{definition}
  Soit $G$ un groupe, d'élément neutre $e$, et $a$ un élément de $G$; le
  sous-groupe engendré par $a$, $gr(a)$ est un groupe monogène.
  \begin{itemize}
    \item Si $gr(a)$ est infini, on dit que $a$ est un élément
      d'\emph{ordre infini} de $G$.
    \item Si $gr(a)$ est fini, son ordre est appelé \emph{ordre de $a$
      dans $G$} et noté $\omega(a)$.
  \end{itemize}
\end{definition}

\begin{proposition}
  Dans un groupe fini, tout élément est d'ordre fini et son ordre divise
  l'ordre du groupe.
\end{proposition}
\begin{proof}
  Le résultat provient de la définition et du théorème 12.
\end{proof}

\begin{corollaire}
  Soit $G$ un groupe fini d'ordre $n$, d'élément neutre $e$. Tout $a$ de
  $G$ vérifie $a^n = e$.
\end{corollaire}
\begin{proof}
  Soit $\omega$ l'ordre de $a$ dans $G$. ; on a $a^{\omega} = e$ ;
  d'après la proposition précédente, $n = q\omega$ donc $a^n = e^q = e$.
\end{proof}

\begin{corollaire}
  Tout groupe $G$ d'ordre $p$ premier est cyclique (donc abélien) ; il
  est engendré par l'un quelconque de ses éléments autres que l'élément
  neutre $e$.
\end{corollaire}
\begin{proof}
  Soit $a\in G \setminus\{e\}$. On a $\omega(a) > 1$ ; comme $\omega(a)$
  divise $p$ premier, on a $\omega(a) = p$ et $gr(a) = G$.
\end{proof}
\begin{corollaire}
  Le produit de deux groupes finis $G$ et $G'$ est cyclique si et
  seulement si ces deux groupes sont cycliques, et d'ordres $n$ et $m$
  premiers entre eux. En particulier $\mathbf{Z}/n\mathbf{Z} \times
  \mathbf{Z}/m\mathbf{Z}$ est cyclique et donc isomorphe à
  $\mathbf{Z}/nm\mathbf{Z}$ si et seulement si $n$ et $m$ sont premiers
  entre eux.
\end{corollaire}
\begin{proof}
  Soient $x\in G$ et $x'\in G'$, d'ordre $\omega$ et $\omega'$
  (diviseurs de $n$ et $m$) ; $(x,x')^q = (e,e')$ s'écrivant : $(x^q =
  e) \wedge (x'^q = e')$, l'ordre de $(x,x') \in G \times G'$ est le
  ppcm de $\omega$ et $\omega'$ ; il s'agit de l'ordre $nm$ de $G \times
  G'$ si et seulement si : \[ \omega = n \wedge \omega' = m \wedge
  \mathop{\mathrm{ppcm}}(n,m) = nm.\]
\end{proof}

\begin{exemple}
  \textsc{Les groupes d'ordre 4} : l'un d'eux est $V_4 =
  \mathbf{Z}/2\mathbf{Z} \times \mathbf{Z}/2\mathbf{Z}$, non cyclique,
  non abélien qui est dit \emph{groupe de Klein}.

  Inversement, soit $G = \{e, a_1, a_2, a_3\}$ un groupe d'ordre 4 non
  cyclique. Les $a_i$ sont d'ordre 2 et on $a_i^2 = e$ ; pour $i \neq
  j$, $a_ia_j \notin \{ a_k^2, a_ie, ea_j \}$ donc $a_ia_j = a_k$. Les
  groupes non cycliques d'ordre 4 sont donc deux à deux isomorphes et
  donc isomorphes à $V_4$.
\end{exemple}


\end{document}

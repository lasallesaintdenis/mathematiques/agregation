function res=rand_norm(m,s)
    X=rand_exp(1,1,1)
    u=rand()
    while 0.11*exp(-X)*u>2/sqrt(2*%pi)*exp(-(X^2)/2)
        X=rand_exp(1,1,1)
        u=rand()
    end
    res=sign(rand()-1/2)*X*s+m
endfunction

rand_norm(5,2)

x=[]
y=[]
for i=1:1000
    x=[x,rand_norm(0,1)];
    y=[y,mean(x)];
end
scf(0);
clf()
plot(y)
scf(1);
clf()
xx=-5:0.1:5; plot(xx,exp(-xx.^2/2)/sqrt(2*%pi),'r-')
histplot(20,x,2)
\documentclass[a4paper,12pt,french]{article}

\input{../commons-lecon.tex.inc}

\title{106 PGCD dans Z et K[X] où K est un corps commutatif, théorème de Bézout. Applications}
\author{Vincent-Xavier \bsc{Jumel}}

\begin{document}


\section{Notions de PGCD}

\subsection{Dans $\Z$}

\begin{proposition}\label{09alg:prop:somme_sous-groupe}
  Soient $G_1$ et $G_2$ deux sous-groupes de $\Z$. Alors $G_1 + G_2$ est un
  sous-groupe de $\Z$.
\end{proposition}

\begin{corollaire}
  Soient $a$ et $b$ deux entiers relatifs non nuls. $a\Z + b\Z$ est un
  sous-groupe de $\Z$.
\end{corollaire}

\begin{proposition}
  Soient $a$ et $b$ deux entiers relatifs non nuls. Il existe unique $d \in
  \N^*$ tel que $a\Z + b\Z = d\Z$.
\end{proposition}

\begin{definition}[PGCD]
  Le \emph{PGCD (plus grand commun diviseur)}  de $a$ et $b$ est l'entier
  $d$ tel que $d\Z = a\Z + b\Z$. On note $d = a \wedge b$.
\end{definition}

\begin{exemple}
  $2 \wedge 3 = $ ?\\
  $2\Z + 3\Z = \{ 2x + 3y \mid (x,y) \in \Z^2 \}$. On a $1 = 2×(-1) + 3×1$,
  donc $1 \in 2\Z + 3\Z$. Ainsi, $\Z \subset 2\Z + 3\Z \subset \Z$ et donc
  $2 \wedge 3 = 1$.
\end{exemple}

\begin{remarque}
  \begin{itemize}
    \item On trouve parfois la notation $\gcd(a,b)$.
    \item Le nom sera explicité plus tard.
    \item La définition pourra s'étendre au pgcd de $n$ nombres.
  \end{itemize}
\end{remarque}

\begin{proposition}
  \begin{itemize}
    \item $\forall (a,b) \in \Z^2,\ a \wedge b = b \wedge a$
    \item $\forall a \in \Z,\ a \wedge 0 = \abs{a}$ et $0 \wedge 0 = 0$
    \item Si $d = a\wedge b $ et $d ≠ 0$, alors $d \mid a$ et $d \mid b$ et
      $d$ est le maximum de l'ensemble des diviseurs.
  \end{itemize}
\end{proposition}

En partique, on utilise quelques propriétés faciles :
\begin{itemize}
  \item $a \wedge b = \abs{a} \wedge \abs{b}$
  \item $\forall m \in \Z,\ (ma) \wedge (mb) = \abs{m}(a \wedge b)$
  \item Si $\delta \mid a $ et $\delta \mid b$, alors
    $\brk*{\frac{a}{\delta} \wedge \frac{b}{\delta} } = \frac1{\abs{\delta}}
    (a \wedge b)$
  \item $d = a \wedge b \iff \begin{cases} a = da',\ a' \in \Z \\ b = b'd
      ,\ b' \in \Z \\
    a' \wedge b' = 1 \end{cases}$
\end{itemize}

En terme de vocabulaire, on dit que $a$ et $b$ sont «premiers entre eux» ou
«étrangers» si $a \wedge b = 1$.

\subsection{Dans $\K[X]$}

On rappelle que $\K[X]$ est euclidien, c'est-à-dire qu'on dispose de la
division euclidienne : $\forall (A,B) \in \K[X]^2, \exists! (Q,R) \in
\K[X]^2,\ A = B Q + R,\ \deg R < \deg B$ et même, l'anneau $\K[X]$ est
principal.

\subsection{PGCD}

\begin{proposition}
  La somme de deux idéeaux de $\K[X]$ est un idéal de $\K[X]$.
\end{proposition}

\begin{proposition}
  Pour $(A,B) \in \K[X]^2,\ J = A\K[X] + B\K[X]$ est un idéal de $\K[X]$ et
  il existe $D$ tel que $J = D\K[X]$.
\end{proposition}
\begin{definition}[PGCD de deux polynômes]
  Les polynômes $D$ tels que $D\k[X] = A\K[X] + B\K[X]$ sont appellés
  \emph{un PGCD} de $A$ et $B$.
\end{definition}

\begin{remarque}
  On appelle \emph{le pgcd} le polynôme $D$ unitaire.
\end{remarque}

On peut reprendre les différentes propriétés du PGCD dans $\Z$ :

\begin{proposition}
  \begin{itemize}
    \item $\forall (A,B) \in \K[X]^2,\ A \wedge B = B \wedge A$
    \item $\forall A \in \K[X],\ A \wedge 0 = D$, un polynôme associé à $A$
      et $0 \wedge 0 = 0$
  \end{itemize}
\end{proposition}

En partique, on utilise quelques propriétés faciles :
\begin{itemize}
  \item $A \wedge B = A_1 \wedge B_1$, où $A_1 = kA$ et $B_1 = k'B$, $k,k'$
    deux éléments non nuls de $\K$.
  \item $\forall P \in \K[X],\ (PA) \wedge (PB) = P_1(A \wedge B)$, où $P_1$
    est associé à $P$.
  \item Si $\Delta \mid A $ et $\Delta \mid B$, alors
    $\brk*{\frac{A}{\Delta} \wedge \frac{B}{\Delta} } = \frac1{\Delta_1}
    (a \wedge b)$, où $\Delta_1$ est un polynôme associé à $\Delta$.
  \item $D = A \wedge B \iff \begin{cases} A = DA_1,\ A_1 \in \K[X] \\ B = D
        B_1,\ B_1 \in \K[X] \\
    A_1 \wedge B_1 = 1 \end{cases}$
\end{itemize}

En terme de vocabulaire, on dit que $A$ et $A$ sont «premiers entre eux» ou
«étrangers» si $A \wedge B = k$.


\section{Théorème de Bezout}

\subsection{Dans $\Z$}

\begin{theoreme}[Bachet de Mezériac (1581-1638) -- Bezout(1730-1783)]
  \label{09alg:thm:bezout}
  $a$ et $b$ sont étrangers si et seulemnt s'il existe $(u,v) \in \Z^2$ tels
  que $au + bv = 1$.
\end{theoreme}

\begin{exemple}
  Calcul de $\np{1983} \wedge \np{2018}$. On cherche $u$ et $v$ entiers
  relatifs tels que $1983u + 2018v = 1$.\\
  Raisonnons modulo \np{1983}.\\
  $1983u + 2018v \equiv 1 \mod 1983 \iff 35v \equiv 1 \mod 1983 \iff 35v - 1
  $ est divisible par 1983. On trouve $v = 170$, puis $u = (1 -
  2018×170)/1983$. On vérifie : $-173×1983 + 2018×170 = 1$.
\end{exemple}

\begin{corollaire}[Théorème de Gauss]\label{09alg:thm:Gauss}
  Si $a \mid (bc)$ et $a \wedge b = 1$, alors $a \mid c$.
\end{corollaire}

\begin{exemple}
  Application à la résolution de $1983u = 2018v = 1$.\\
  On connaît la solution $u_0 = -173$ et $v_0 = 170$. Ainsi, l'équation est
  équivalente à $1983u + 2018v = -173×1983 + 2018×170$ soit encore $1983(u +
  173) = 2018(170 -v)$. Comme $1983 \wedge 2018 = 1$, 1983 divise $170 -v$.
  On peut donc écrire cette dernière sous la forme $k1983 = 170 - v$ et donc
  $v = 170 - k1983$. Pour ce $v$ là, on peut trouver $u$ et l'ensemble des
  solutions est donc $\{ (u,v) \in \Z^2 \mid u = -173 + 2018k, v = 170 -
  1983k, k \in \Z \}$.
\end{exemple}

\begin{proposition}
  Si $a \mid c$ et $b \mid c$ et $a \wedge b = 1$ alors $ab \mid c$.
\end{proposition}

\begin{proposition}
  Si $a \wedge b =1 $ et $a \wedge c = 1$, alors $a \wedge bc = 1$.
\end{proposition}

\begin{corollaire}
  Si $a \wedge b = 1$, alors, $\forall (m,n) \in \N^2,\ a^m \wedge b^n = 1$.
\end{corollaire}

On peut présenter l'algorithme d'Euclide qui constitue une méthode pratique
pour le caclul du PGCD. De plus, cet algorithme peut-être facilement étendu
pour calculer également les coefficients $(u,v)$ de Bezout.\\
Cet algorithme repose principalement sur la proposition qui suit.

\begin{proposition}
  $\forall (a,b,c) \in \Z,\ a \wedge b = a \wedge (b - ac)$.
\end{proposition}

\begin{exemple}Un tel algorithme peut s'écrire en Python :\\
  \begin{verbatim}
def pgcd(a,b):
    if b == 0 :
        return a
    else:
        return pgcd(b,a % b)
  \end{verbatim}
\end{exemple}

\begin{exemple}
  Dans le cas du calcul de $2002 \wedge 1983$ :\\
  \begin{tabular}{l|*{7}{c|}c}
    quotient &  & 1 & 104 & 2 & 1 & 2 & & \\ \hline
    reste & 2002 & 1983 & 19 & 7 & 5 & 2 & 1 & 0 \\
  \end{tabular}
\end{exemple}

On peut étendre l'algorithme ci-dessus, à une version qui permet le calcul
rapide des coefficients $u$ et $v$ de l'égalité de Bézout. On pose ainsi,
$u_{-1} = 1, u_{0} = 0$ et $v_{-1} = 0, v_{0} = 1$ et pour chaque itération,
$u_{n+1} = u_{n-2} - q_{n} u_{n}$ et $v_{n+1} = v_{n-1} - q_n v_n$.

\begin{exemple}
  Dans le cas du calcul de $2002 \wedge 1983$ :\\
  \begin{tabular}{l|*{7}{c|}c}
    $q_n$ &      & 1    & 104 & 2    & 1    & 2    &      &    \\ \hline
    $r_n$ & 2002 & 1983 & 19  & 7    & 5    & 2    & 1    &  0 \\ \hline
    $u_n$ & 1    & 0    & 1   & -104 & 209  & -313 & 835  &    \\ \hline
    $v_n$ & 0    & 1    & -1  & 105  & -211 & 316  & -843 &    \\
  \end{tabular}
\end{exemple}

Justification :
On pose $a = r_{-1}$ et $b = r_0$. $a = bq_0 + r_1,\ 0≤ r_1 < b$ et $b = r_1
q_1 + r_2,\ 0≤ r_2 < r_1$ et de façon générale $r_{k-2} = r_{k-1} q_{k-1} +
r_{k},\ 0 ≤ r_k < r_{k-1}$.\\
Ces restes peuvent s'exprimer comme $r_{-1} = 1 a + 0 b$, $r_0 = 0a + b$,
$r_1 = r_{-1} + r_0q_0$, …, $r_k = r_{k-2} - r_{k-1}q_{k-1}$, ce qui peut
s'écrire, avec les notations $u_n$ et $v_n$, $r_j = u_j a + v_j b$ et
$r_{j+1} = r_{j-1} - r_j q_j = u_{j-1}a + v_{j-1} b - q_j (u_j a + v_j b) =
(u_{j-1} - q_j u_j) a + (v_{j-1} - q_j v_j) b$.

\subsection{Dans $\K[X]$}

\begin{theoreme}\label{10alg:thm:Bezout_polynomes}
  $A$ et $B$ sont étrangers si et seulement si $\exists (U,V) \in \K[X]^2,\
  AU + BV = 1$.
\end{theoreme}

\begin{corollaire}[Théorème de Gauss]\label{10alg:thm:Gauss_polynomes}
  Si $A \mid (BC)$ et $A \wedge B = 1$, alors $A \mid C$.
\end{corollaire}



$U,V$ se trouvent en utilisant l'algorithme d'Euclide étendu :

\begin{tabular}{c*{3}{|c}}
  & $4X$ & $\frac16 X + \frac1{36}$ & \\ \hline
  $4X^3 +2X - 1$ & $X^2 - 1$ & $6X - 1$ & $\frac{-35}{36}$ \\ \hline
  1 & 0 & 1 & $\frac16 X - \frac1{36}$ \\ \hline
  0 & 1 & $-4X$ & $\frac23 X^2 + \frac19 X + 1$ \\ \hline
\end{tabular}

\section{Applications}

\subsection{}

\subsection{Décomposition en éléments simples}

\begin{lemme}\label{10alg:lemme:polesimple}
  Soit $F = \dfrac{A}B$ une fraction rationnelle irréductible. On suppose que
  $\deg F < 0$ et que $B = P_1 P_2$ où $P_1$ et $P_2$ sont des polynômes
  premiers entre-eux. Alors, il existe de manière unique des polynômes $A_1$
  et $A_2$ tels que $F = \dfrac{A_1}{P_1} + \dfrac{A_2}{P_2}$ et $\deg A_1 <
  \deg P_1$ et $\deg A_2 < \deg P_2$.
\end{lemme}
\begin{proof}
  On veut montrer l'existence et l'unicité de $A_1$ et $A_2$ tels que $A_1
  P_2 + A_2 P_1 = A,\ \deg A_1 < \deg P_1,\ \deg A_2 < \deg P_2$, sachant
  que $\deg P_1 + \deg P_2 > \deg A$.\\
  Comme $P_1 \wedge P_2 = 1$, on sait qu'il existe $(U,V) \in \K[X]^2, \
  P_1U + P_2 V = 1$. On a donc $P_1 (AU) + P_2 (BV) = A$ et donc $P_1 B_2 +
  P_2 B_1 = A$. Divisons $B_1$ par $P_1$ : $B_1 = P_1 Q_1 + A_1,\ \deg A_1 <
  \deg P_1$. On a donc $P_1 B _2 + P_2 (P_1 Q_1 + A_1) = A$ ou encore $P_1
  (B_2 + P_2Q_1) + P_2 A_1 = A$.\\
  Vérifions que $A_2 = B_2 + P_2Q_1$ est le polynôme $A_2$ cherché. $P_1 A_2
  = -P_2 A_1 + A$. \\
  $\deg P_1 + \deg A_2 ≤ \max(\deg P_2 A_2, \deg A)$ et comme $\deg A < \deg
  P_1 + \deg P_2$, on obtient que $\deg A_1 < \deg P_1$, puis comme $\deg
  P_1 A_2 < \deg P_1 + P_2$, on en déduit que $\deg A_2 < \deg P_2$.

  Pour l'unicité : si $A = A_1 P_2 + A_2 P_1 = B_1 P_2 + A_2 P_1$, alors on
  aurait $(A_1 - B_1)P_2 = (B_2 - A_2)P_1$. Comme $P_1 \wedge P_2 =
  1$, d'après le théorème de Gauss (\ref{10alg:thm:Gauss_polynomes}), $P_1$
  divise $A_1 - B_1$, mais comme $\deg A_1 < \deg P_1$ et $\deg B_1 < \deg
  P_1$, la seule possibilité est $A_1 - B_1 = O_{\K[X]}$. On a donc $A_1 =
  B_1$ et donc $A_2 = B_2$.
\end{proof}

\begin{question}
  \begin{enumerate}
    \item Calculer $(X - a) \wedge (X - b)$
    \item Calculer $(X^n - 1) \wedge (X^m - 1)$
  \end{enumerate}
\end{question}
\begin{solution}
  \begin{enumerate}
    \item $(X - a) \wedge (X - b) = 1$
    \item $(X^n - 1) \wedge (X^m - 1) = X^{n \wedge m} - 1$
  \end{enumerate}
\end{solution}

\begin{proposition}
  Soit $P \in \K[X]$, $P ≠ 0_{\K[X]}$. $P$ est à racines simples dans $\C$
  si et seulement si $P \wedge P' = 1$.
\end{proposition}

\subsection{Décomposition en produit de facteurs irréductibles}

Les notions développées ici cherchent à étendre la notion de nombres
premiers à un anneau de polynôme.

\begin{definition}[polynôme irréductible]
  Un polynôme $P$ est dit \emph{irréductible} dans $\K[X]$ lorsque
  \begin{itemize}
    \item $\deg P ≥ 1$
    \item $P$ a pour seuls diviseurs ses polynômes associés et les polynômes
      constants non nuls (les inversibles de l'anneau)
  \end{itemize}
\end{definition}

Dans $\C[X]$, tous les polynômes sont scindés.\\
Dans $\R[X]$, les irréductibles sont les $(X - a),\ a\in \R$ et lmes $X^2 +
pX + q$, où $p^2 - 4q < 0$.

\begin{proposition}
  Tout polynôme non constant dans $\K[X],\ \K \in \{ \R, \C \}$ admet une
  décomposition unique (à l'ordre près et modulo l'association des
  polynômes) en produit de facteurs irréductibles.
\end{proposition}

\end{document}

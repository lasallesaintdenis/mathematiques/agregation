\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\title{205 Espaces complets. Exemples et application}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

Dans cette leçon, $\K$ désigne un corps commutatif.

\section{Généralités}

Soit $(E,d)$ un espace métrique.

\begin{definition}[Suite de Cauchy]
  On appelle suite de Cauchy de $(E,d)$ tout suite $(x_n)_{n\in\N}$
  d'éléments de $E$ vérifiant : \[ \forall \varepsilon \in \R^*_+,\
    \exists N \in \N,\ \forall n \geqslant N,\ \forall p \geqslant N,\
  d(x_n,x_p) < \varepsilon .\]
\end{definition}

\begin{proposition}
  Toute suite convergente est une suite de Cauchy.
\end{proposition}

\begin{proposition}
  Soient $d$ et $\delta$ deux distances équivalentes sur $E$. Une suite
  $(x_n)_{n\in\N}$ d'éléments de de $E$ est de Cauchy dans $(E,d)$ si et
  seulement si elle est de Cauchy dans $(E,\delta)$.
\end{proposition}

\begin{theoreme}
  Toute suite de Cauchy est bornée.
\end{theoreme}

\begin{theoreme}
  Toute valeur d'adhérence d'une suite de Cauchy est limite de la suite.
\end{theoreme}

\begin{corollaire}
  Une suite de Cauchy a au plus une seule valeur d'adhérence.
\end{corollaire}

\begin{theoreme}
  Soient $(E,d)$ et $(F,\delta)$ deux espaces métriques, $f$ une
  application uniformément continue de $E$ dans $F$. Si $(x_n)_{n\in\N}$
  est une suite de Cauchy de $E$, alors $f(x_n)_{n\in\N}$ est une suite
  de Cauchy de $F$.
\end{theoreme}

\begin{definition}
  Un espace métrique $(E,d)$ est dit \emph{complet} si et seulement si
  toute suite de Cauchy de $(E,d)$ est convergente.
\end{definition}

\begin{exemple}
  $\R$ est par construction complet.
\end{exemple}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique, et $F$ une partie de $E$. Si le
  sous-espace $(F,d)$ est complet, alors $F$ est un fermé de $E$.
\end{theoreme}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique complet, et $F$ une partie de $E$.
  Le sous-espace $(F,d)$ est complet si et seulement si $F$ est un fermé
  de $E$.
\end{theoreme}

\begin{theoreme}
  Soit $(E_i,d_i)_{i\in \N_p}$ une famille finie d'espaces métriques
  complets. On désigne par $E$ l'ensemble-produit $E = \prod_{i=1}^p
  E_i$.

  Alors l'espace métrique $(E,d)$ où $d$ est une des trois distances
  standard (à voir) est complet.
\end{theoreme}

\begin{exemple}
  $\R^n$ est complet.
\end{exemple}

\begin{theoreme}[fermés emboîtés]
  Soient $(E,d)$ un espace métrique complet et $(F_n)_{n\in N}$ une
  suite de fermés non vides de $E$ vérifiant :
  \begin{enumerate}[label=\roman*)]
    \item $\forall n \in \N\ F_{n+1} \subset F_n$ ;
    \item $\lim \delta(F) = 0$ où $\delta$ est le diamètre d'une partie ;
  \end{enumerate}
  Alors $\displaystyle\bigcap_{n\in \N}F_n $ est réduit à un point.
\end{theoreme}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique, $(F,\delta)$ un espace métrique
  complet, $f$ une fonction de $E$ dans $F$. $A$ une partie de $D_f$ et
  $a \in \overline{A}$. Pour que $f$ admette une limite en $a$ suivant
  $A$, il faut et il suffit que : \[ \forall \varepsilon \in \R_+^*,\
    \exists \mathcal{U} \in \mathcal{V}(a) \ \forall (x,x')\in (U\cap
  A)^2 \ \delta(f(x),f(x')) < \varepsilon.\]
\end{theoreme}

\section{Application}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique complet non vide et $f: E\to E$ une
  application $k$-contractante. L'équation $f(x) = x$ admet une solution
  et une seule, obtenue comme limite de la suite récurrente $(x_n)_{n\in
  N}$ obtenue en choississant arbitrairement $x_0 \in E$ et en convenant
  que \[ \forall n \in \N,\ x_{n+1} = f(x_n).\]
\end{theoreme}

\begin{theoreme}
  Soient $(E,d)$ un espace métrique complet non vide, $\Lambda$ un
  espace topologique, et $f: E\times \Lambda \to E$ une application. On
  suppose que pour tout $x\in E, \lambda \mapsto f(x,\lambda)$ est une
  application continue de $\Lambda$ dans $E$, et que pour tout $\lambda
  \in \Lambda, f_{\lambda} : x\mapsto f(x,\lambda)$ est une application
  $k$-contractante de $E$ dans $E$, $k$ étant indépendant de $\lambda$.
  En appellant $\alpha_{\lambda}$ l'unique point fixe de $f_{\lambda}$,
  l'application $\lambda \mapsto \alpha_{\lambda}$ est continue.
\end{theoreme}


\end{document}

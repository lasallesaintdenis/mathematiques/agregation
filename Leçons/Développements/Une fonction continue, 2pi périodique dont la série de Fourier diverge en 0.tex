\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Une fonction continue, $2\pi$-périodique dont la série de Fourier
diverge en 0}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{exemple}
  La fonction $f \colon \R \to \R$, paire, $2\pi$-périodique définie, pour
  tout $x \in [0,\pi]$ par \[ f(x) = \sum_{p=1}^{+\infty} \frac{1}{p^2} \sin
  \left[ (2^{p^3} +1) \frac{x}2 \right] \] est continue mais sa série de
  Fourier diverve en 0.
\end{exemple}

\begin{proof}~\\[-6mm]
  \begin{itemize}
    \item La série $\sum_{p=1}^{+\infty} \frac{1}{p^2} \sin \left[ (2^{p^3}
      +1) \frac{x}2 \right]$ converge normalement donc $f$ est bien définie
      et continue sur $[0,\pi]$. De plus, $f$ est définie sur $[-\pi,0]$ par
      $f(x) = f(-x)$ donc $f$ est continue sur $[-\pi,\pi]$. Par
      $2\pi$-périodicité, $f$ est continue sur $\R$.
    \item Pour tout $d\in \N$, on pose \begin{align*} a_{n,d} & = \int_0^{\pi}
        \cos(nt) \sin \frac{(2d +1)t}2 \diff t \text{ pour tout } n \in \N
        \\
        s_{q,d} & = \sum_{i=0}^{q} a_{i,d} \text{ pour tout } q \in \N.
      \end{align*}
      On a \begin{align*}
        a_{n,d} & = \frac{1}2 \int_0^{\pi} \left( \sin \left( \left(
        \frac{2d + 1}2 + n \right) t \right) + \sin \left( \left( \frac{2d +
        1}2 - n \right) t \right) \right) \diff t \\
        & = \frac{1}2 \left[ - \frac{2}{2d + 1 + 2n} \cos \left( \left(
        \frac{2d + 1}2 + n \right) t \right) - \frac{2}{2d + 1 - 2n} \cos
        \left( \left( \frac{2d + 1}2 + n \right) t \right) \right]_0^{\pi}
        \\
        & = \frac{1}{2d + 1 + 2n} + \frac{1}{2d + 1 - 2n} \\
        & = \frac{4d + 2}{(2d+1)^2 - 4n^2}
      \end{align*}
      On a donc $a_{n,d} \geqslant 0$ pour $n \leqslant d$, d'où $s_{q,d}
      \geqslant 0$ pour $q \leqslant 0$.

      On remarque que les coefficients $a_{n,d}$ sont égaux, au coefficient
      $\frac{2}{\pi}$ près, aux coefficients de Fourier $a_n(g_0)$ de $g_0 :
      t \mapsto \vert \sin \left( \left( d + \frac{1}2 \right) t \right)
      \rvert$ sur $[0,\pi]$ puis prolongée par parité et $2\pi$-périodicité.
      La fonction $g_0$ étant continue, et $\Co^1$ par morceaux, sa série de
      Fourier converge vers $g_0$. En particulier, on a $\frac{a_0}2 +
      \sum_{n=1}^{+\infty} a_{n,d} = \frac{\pi}2 g_0(0) = 0$, donc la suite
      $(s_{q,d})_{q\in\N}$ converbe vers $\frac{a_0}2$.

      De plus, $a_{n,d}$ est positif pour $n \leqslant d$, négatif pour $n>
      d$, donc $(s_{q,d})_{q\in\N}$ est décroissante à partir de $q = d$.
      Comme elle converge vers $\frac{a_0}2$, on a, pour tout $q > d,
      s_{q,d} \geqslant \frac{a_0}2 \geqslant 0$.

      Donc $s_{q,d} \geqslant 0$ pour tous $q, d \in \N$.

      Soit à présent $d \in \N$. On a \begin{align*}
        s_{d,d} \geqslant \sum_{n=1}^d \frac{d + \frac{1}2}{(d + \frac{1}2)^2
        - n^2} & \geqslant \sum_{n=1}^d \int_{n-1}^n \frac{d + \frac{1}2}{(d
        + \frac{1}2)^2 - t^2} \diff t \\
        & \geqslant \int_0^d \frac{d + \frac{1}2}{(d+ \frac{1}2)^2 - t^2}
        \diff t = \frac{1}2 \ln(4d + 1)
      \end{align*}
      donc $s_{d,d} \geqslant \frac{\ln d}2$ pour tout $d \in \N^*$.
    \item On va montrer que la série de Fourier diverge en 0.

      Comme $f$ est paire, on a $b_n(f) = 0$ pour tout $n \in \N^*$. Soit $n
      \in \N^*$. On a \begin{align*}
        a_n(f) = \frac{2}{\pi} \int_0^{\pi} f(t) \cos nt \diff t & =
        \frac{2}{\pi} \int_0^{\pi} \sum_{p=1}^{+\infty} \frac{1}{p^2} \sin
        \left[ \left( 2^{p^3} + 1 \right) \frac{t}2 \right] \cos nt \diff t
        \\
        & = \frac{2}{\pi} \sum_{p=1}^{+\infty} \frac{1}{p^2} \int_0^{\pi}
        \sin \left[ \left( 2^{p^3} + 1 \right) \frac{t}2 \right] \cos nt
        \diff t \text{ CV normale} \\
        & = \frac{2}{\pi} \sum_{p=1}^{+\infty} \frac{1}{p^2} a_{n,2^{p^3 - 1
        }} .
      \end{align*} donc \begin{align*}
        S_{2^{p^3 - 1}} = \frac{2}{\pi} \sum_{k=1}^n a_k(f) & =
        \sum_{p=1}^{+\infty} \frac{1}{p^2} s_{n,2^{p^3 - 1}} \\
        & \geqslant \frac{1}{p^2} s_{2^{p^3 - 1},2^{p^3 - 1}} \\
        & \geqslant \frac{1}{2p^2} \ln(2^{p^3 - 1}) = \frac{p^3 - 1}{2p^2}
        \ln 2 \xrightarrow[p \to +\infty]{} +\infty
      \end{align*}
      Donc $S_{2^{p^3 - 1}} \xrightarrow[p \to +\infty]{} +\infty$ et donc
      $\sum a_n(f)$ diverge.

      Ainsi, la série de Fourier de $f$ diverge en 0.
  \end{itemize}
\end{proof}



\end{document}

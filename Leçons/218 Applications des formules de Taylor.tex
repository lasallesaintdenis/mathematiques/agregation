\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}


\title{218 Applications des formules de Taylor}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

Dans la suite, $E$ et $F$ sont deux espaces de Banach (sur $\R$ ou
$\C$), $\U$ est un ouvert de $E$ et $f$ est une application de $\U
\subset E \to F$

\section{Généralités}

\begin{theoreme}[Inégalité de la moyenne, des accroissements finis]
  Soit $f$ différentiable en tout point de $\U$. Soit $[a;b]$ un segment
  contenu dans $\U$. et $k$ un réel positif.

  Si $\forall x\in[a;b], \ \lVert D(f(x))\rVert_{\mathcal{L}(E,F)}
  \leq k$ alors $\lVert f(b) - f(a) \rVert_F \leq k\lVert b-a \rVert_E$
\end{theoreme}

Application : le nombre de Liouville $\sum\frac1{2^{n!}}$ est
transcendant.

\begin{theoreme}[Taylor-Young]
  Soit $f$ $(n-1)$ fois différentiable sur $\U \ni a$ et $n$ fois
  différentiable en $a$, alors \[ f(a+h) - f(a) - Df(a)h - \cdots -
  \frac1{n!}D^nf(a)h^n = O(\lVert h\rVert^n) \]
\end{theoreme}

Application : développements limités

\begin{theoreme}[Taylor avec reste intégral]
  Si $f$ est de classe $C^{n+1}$ sur $\U$ et si le segment $[a;a+h]$ est
  contenu dans $\U$, alors \[ f(a+h) - f(a) - Df(a)h - \cdots -
    \frac1{n!}D^nf(a)h^n = \int_0^1\frac{(1-t)^n}{n!}
  O(D^{n+1}f(a+h)h^{n+1})\mathrm{d}\-t \]
\end{theoreme}
\begin{corollaire}[Inégalité de Taylor-Lagrange]
  Sous les conditions précédentes, si on suppose de plus que

  $\forall a\in\U,\ \lVert D^nf(a)\rVert \leq M$, alors $f(a+h) - f(a) -
  Df(a)h - \cdots - \frac1{n!}D^nf(a)h^n \leq M\frac{\lVert h
  \rVert^{n+1}}{(n+1)!}$
\end{corollaire}

\section{Applications en analyse et en topologie}
\subsection{Théorème de Darboux}
\begin{theoreme}
  Soit $I$ un intervalle et $f:I\to\R$ une fonction dérivable.

  Alors $f'(I)$ est un intervalle.
\end{theoreme}
\subsection{Inégalité de Kolmogorov}

Soit $f\in C(\R,\C)$. Pour tout $k\in\N, k\leq n$, on pose $M_k =
\sup_{x\in\R}\lvert f^{(k)}(x) \rvert$. On suppose que $M_0$ et $M_n$
sont finis.

Alors $\forall k \in \llbracket 0,n \rrbracket, M_k$ est fini et $M_k
\leq 2^{\frac{k(n-k)}{2}}M_0^{1-\frac{k}{n}}M_n^{\frac{k}{n}}$

En particulier pour $n=2$, on a $\sup \lvert f'(x)\rvert \leq
\sqrt{2M_0M_2}$

\subsection{Théorème de Gloeser pour une variable}

Soit $f:\R\to\R$ une fonction de classe $C^2$, positive sur $\R$, alors
$\sqrt{f}$ est de classe $C^1$ si et seulement si en tout zéro $x_0$ de
$f$ on a $f''(x_0) = 0$

\section{Applications en géométrie}
\subsection{Convexité}

Soit $f:\U\subset\R^n \to \R$ 2 fois différentiable sur $\U$. $f$ est
convexe si et seulement si $D^2(f)$ est une forme quadratique positive
en tout point de $\U$.

\subsection{Extremas}

Soit $f:\U\subset E \to \R$ 2 fois différentiable
\begin{enumerate}
  \item si $f$ admet un minimum local en $a\in\U$, alors $D(f)(a) = 0$
    et $D^2(f)(a) \geq 0$ ;
  \item si $D(f)(a) = 0$ et $D^2(f)$ est définie positive alors $f$
    admet un minimum local strict en $a$.
\end{enumerate}

\subsection{Lemme de Morse à $n$ variables}

Soit $f:\U\subset\R^n\to\R, 0\in\U$, de classe $C^3$ sur $\U$. On suppose
que $D(f)(0) = 0$ et que $D^2(f)$ est non dégénérée, de signature
$(p,n-p)$.

Alors il existe un difféomorphisme $x\mapsto u = \varphi(x)$ et deux
voisinage de $\R^n$, de classe $C^1$, tel que $\varphi(0) = 0$ et $f(x)
- f(0) = u_1^2 + \dots + u_p^2 - u_{p+1}^2 - \dots - u_{n}^2$

\subsection{Étude locale d'un arc paramétré}

Soit $f:I\to\R^2$ un arc paramétré de classe «suffisante», $\Gamma$ sa
trajectoire. On note $p = \min \{ p\geq 1 | \overrightarrow{f^{(p)}}(t)
\neq \vec{0} \}$ et $q = \min \{ q>p | \overrightarrow{f^{(p)}}(t) ;
\overrightarrow{f^{(q)}}(t)\text{ est libre}\}$

Au voisinage de $M(t)$, $\Gamma$ a l'allure suivante :

\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,1.1) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,1.1) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{-\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,1.1) ;
  \draw plot [domain=0:0.7,smooth] (-\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,1.1) ;
  \draw plot [domain=0:0.7,smooth] (-\x,{-\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}
$p$ pair, $q$ pair : $p$ pair, $q$ impair : $p$ impair, $q$ pair : $p$
impair, $q$ impair

\section{Applications en analyse numérique}
\subsection{Méthode de Newton}
\begin{theoreme}
  Soit $f$ une fonction de classe $C^2([a;b],\R)$. Supposons qu'il
  existe une unique $\alpha\in[a;b]$ tel que $f(\alpha) = 0$ et $f'(x_0)
  \neq 0$ pour $x_0$ «assez proche» de $\alpha$.

  Alors la suite définie par $\left\lbrace\begin{array}{l}x_0 \\ x_{n+1}
= x_n - \frac{f(x_n)}{f'(x_n)}\end{array}\right.$ est bien définie et
converge vers $\alpha$ en moyenne quadratique.
\end{theoreme}

\begin{corollaire}
  Si de plus $f'' > 0$, alors on peut choisir $x_0$ quelconque dans
  $[a;b]$
\end{corollaire}

\subsection{Intégration numérique}

Soit $f:[a;b]\to\R$, $a=a_0<a_1 \dots < a_n =b$ une subdivision. On
approche $\int_a^b f$ par $\sum_{i=0}^{n-1}(a_{i+1}-a_i)
\sum_{j=0}^{l_i} \omega_{ij}f(x_{ij}$ où $\sum\omega_{ij} = 1$ et
$x_{ij} \in [a_i ; a_{i+1}]$.

On note $E(f)$ la différence $\int_a^b - \sum_k \lambda_kf(x_k)$

\begin{definition}
  Une méthode est dite d'ordre $N$ si elle est exacte pour tout polynome
  de degré inférieur ou égal à $N$ et inexacte pour au moins un polynome
  de degré $N+1$.
\end{definition}

\begin{theoreme}
  Soit une méthode d'ordre $N$. Si $f$ est de classe $C^{N+1}$ sur
  $[\alpha ; \beta]$ alors \[ E(f) = \frac1{n!}\int_\alpha^\beta
  K_N(t)f^{N+1}(t)\] avec $K_N(t) = E\left[x\mapsto(x-t)^N\right]$ pour
  $t\in[\alpha ; \beta ]$
\end{theoreme}

\subsection{Méthodes numériques à un pas}

Utilisées pour résoudre le problème de Cauchy
$\left\{\begin{array}{l}y'(t) = f(t,y) \\ y(t_0) =
  y_0\end{array}\right.$ sur $I$

\begin{definition}
  Les méthodes à un pas sont les méthodes de la forme \[ y_{n+1} = y_n
  +h_n \Phi(t_n,y_n,h_n),\ 0\leq n< N \]
\end{definition}

\end{document}

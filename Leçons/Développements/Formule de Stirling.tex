\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}



\title{Formule de Stirling}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}

\maketitle

$\ln(n!) = \ln 1 + \ln 2 + \cdots + \ln n$

$\frac{1}{n} \ln n ! - \ln n = \frac{1}{n} \left[ \ln\frac1n +
\ln\frac2n + \cdots + \ln\frac{n}{n} \right] = S_n$

On reconnaît une somme de Riemann pour $I = \int_0^1 \ln x\mathrm{d}\,x
= -1$

$S_n$ est l'approximation de $I$ par les rectangles.

On a $S_n > I$

\begin{center}
  \begin{tikzpicture}[xscale=1.5]
    \draw [->] (-0.1,0) -- (1.1,0) ;
    \draw [->] (0,-2.7) -- (0,0.3) ;
    \draw plot [domain=0.1:1,smooth,samples=200] (\x,{ln(\x)}) ;
    \draw (0,0) -- (0,{ln(0.1)}) -- (0.1,{ln(0.1)}) -- (0.1,0) -- cycle ;
    \foreach \i in {2,...,10} {
      \draw (\i/10,{ln(\i/10)}) -- ({(\i-1)/10},{ln(\i/10)}) ;
      \draw (\i/10,{ln(\i/10)}) -- (\i/10,0) ;
      \draw[red,very thin] (\i/10,{ln(\i/10)}) -- ({(\i-1)/10},{ln((\i-1)/10)}) ;
    }
  \end{tikzpicture}
\end{center}

Approximons par des trapèzes sur $\left[\frac1n ; 1\right]$.

On évite 0 car $\ln$ est non bornée en 0.

On a $\int_0^{\frac1n}\ln x\mathrm{d\,}x = \left[ x\ln x -
x\right]_0^{\frac1n} = - \frac{\ln n}{n} - \frac1n $

Soit $T_n = \int_0^{\frac1n}\ln x\mathrm{d\,}x + \frac12
\sum_{k=1}^{n-1} \left(\ln\frac{k}{n} + \ln\frac{k+1}{n} \right) $

Comme la fonction $\ln$ est concave, on a $T_n < I$. 

Et $S_n - T_n = \frac1{2n}\left(\ln\frac{n}{n} - \ln\frac1n \right) +
\frac1n = \frac{\ln n}{2n} + \frac1n$.

Donc $ 0 < S_n - I < \frac{\ln n}{2n} + \frac1n$.

On a donc $0 < \frac1n \ln n ! - \ln n + 1 < \frac{\ln n}{2n} + \frac1n$
d'où en multipliant par $n$ et en prenant l'exponentielle \[ 1 <
n!n^{-n} e^n < e\sqrt{n} \] ou encore $n! < \left(\frac{n}{e}\right)^n
e\sqrt{n}$

Il est naturel de considérer la suite $v_n =
\ln\frac{n!e^n}{n^n\sqrt{n}}$ qui est la somme partielle de la série de
terme $u_1 = 1$ et $u_n = \ln\frac{ne(n-1)^{n-1}\sqrt{n-1}}{n^n\sqrt{n}}
= \ln\left(e\left(1-\frac1n\right)^{n-\frac12}\right)$

$u_n = 1 + \left(n-\frac12\right)\ln\left(1-\frac1n\right) = 1 - \left(n
- \frac12 \right)\left(\frac1n + \frac1{2n^2} +
O\left(\frac1{n^3}\right)\right) = 1 -1 -\frac1{2n} + \frac1{2n} +
O\left(\frac1{n^2}\right)$

$u_n = O\left(\frac1{n^2}\right)$ et donc la série converge et $v_n$
pssède une limite finie $S$ et donc $\sigma_n =
\frac{n!e^n}{n^n\sqrt{n}}$ tend vers $e^S$.

Calcul de $S$ à partir des formules de Wallis : $I_n = \int_0^\frac\pi2
\sin^n x \,\mathrm{d}\-x$

À l'aide d'une double intégration par parties, on montre que $I_{n+1} =
\frac{n}{n+1}I_{n-1}$. On a aussi $I_0 = \frac\pi2$ et $I_1 = 1$ d'où \[
  \begin{array}{l}
    I_{2p} = \frac{1\cdot 3\cdot 5\cdots (2p-1)}{2\cdot 4 \cdot 6 \cdots
    2p} \frac\pi2 \\[0.7cm]
    I_{2p+1} = \frac{2\cdot 4 \cdot 6 \cdots 2p}{1\cdot 3\cdot 5\cdots
    (2p+1)}
  \end{array}\]
Comme $I_n$ est positive décroissante, on a $1\geq \frac{I_{n+1}}{I_n}
\geq \frac{I_{n+1}}{I_{n-1}}=\frac{n}{n+1}$

Et donc $\frac{I_{n+1}}{I_n} \to 1$.

$1 = \lim\frac{I_{2p+1}}{I_{2p}} = \lim\frac{(2\cdot 4 \cdot 6 \cdots
2p)^2}{(1\cdot 3\cdot 5\cdots (2p-1))^2}\frac2{(2p+1)\pi}$

Donc $\lim\sqrt{p}\frac{1\cdot 3\cdot 5\cdots (2p-1)}{2\cdot 4
\cdot 6 \cdots 2p} = \frac1{\sqrt{\pi}}$

Donc $\sqrt{\pi} = \lim\frac{2^{2n}(n!)^2}{2^{2n}(2n)!} =
\lim\frac{(\sigma_n)^2}{\sqrt{2}\sigma_{2n}} = \frac{e^S}{\sqrt2}$ d'où
$e^S = \sqrt{2\pi}$ et \[ n! =  n^n e^{-n}\sqrt{2\pi n}(1+\varepsilon_n)
\]


\end{document}

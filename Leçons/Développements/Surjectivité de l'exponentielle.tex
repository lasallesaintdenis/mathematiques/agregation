\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Surjectivité de l'exponentielle}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{rappel}[Théorème d'inversion local]
  Soit $U$ un ouvert de $\R^n$, $a \in U$ et $f \colon \U \to \R^n$ une
  application $\Co^1$.

  On suppose que $Df(a)$ est inversible, c'est-à-dire $\det Df(a) \neq
  0$.

  Alors il existe un ouvert $V \subset U$ qui contient $a$ et un ouvert
  $W$ qui contient $f(a)$ tel que $f|_V$ soit un $\Co^1$-difféomorphisme
  de $V$ sur $W = f(V)$.
\end{rappel}


\begin{theoreme}
  $\exp \colon \M_n(\C) \to \GL_n(\C)$ est surjective.
\end{theoreme}

\section*{Preuve}

\subsection*{Étape 1}
Soit $C \in \M_n(\C)$.

Montrons que $\exp(C) \in \C[C]$.

Comme $\C[C]$ est un sous espace vectoriel de $\M_n(\C)$, alors $\C[C]$
est fermé dans $\M_n(\C)$. Ainsi \[ \exp(C) = \lim_{N\to +\infty}
\sum_{k=0}^N \frac{1}{k!} C^k \] est une limite de polynômes en $C$ donc
d'éléments de $\C[C]$. On en déduit que $\exp(C) \in \C[C]$.

\subsection*{Étape 2}
Soit $C \in \M_n(\C)$.

Montrons que $\C[C]^{\times} = C[C] \cap \GL_n(\C)$

Tout d'abord, pour $\M \in \GL_n(\C)$, on sait que $M^{-1}$ est un
polynôme en $M$.

\begin{mdframed}
  En effet, le théorème de Cayley-Hamilton énonce que $\chi_M(M) = 0$.

  Or $\chi_M(M) = M^n + a_{n-1}M^{n-1} + \cdots + a_1M + (-1)^n \det(M)
  I_n$.

  Ainsi, $(-1)^{n+1} \det(M) I_n = Q(M)M$, c'est-à-dire \[ I_n =
  \frac{(-1)^{n+1}}{\det M} Q(M) M . \]
\end{mdframed}

Soit $M \in C[C] \cap \GL_n(\C)$ alors $M^{-1}$ est aussi un polynôme en
$M$ donc en $C$.

Ainsi, $M^{-1} \in \C[C]$ et on a donc $C[C] \cap \GL_n(\C) \subset
\C[C]^{\times}$. L'inclusion réciproque ne pose pas de problème, on a
donc l'égalité.

\subsection*{Étape 3}
Montrons que $\exp$ réalise un morphisme du groupe additif $\C[C]$ dans
le groupe multiplicatif $\C[C]^{\times}$.

L'ensemble $\C[C]$ est une sous-algèbre commutative de $\M_n(\C)$.

On peut donc appliquer la formule \[ \forall (A,B) \in \C[C]^2, \exp(A +
B) = \exp(A) \exp(B) .\] On sait, par l'étape 1, que $\exp(\C[C])
\subset \C[C]$.

Comme $\exp$ est à valeurs dans $\GL_n(\C)$, sa restriction à $\C[C]$
est bien un morphisme de groupes qui, d'après l'étape 2, prends ses
valeurs dans $\C[C]^{\times} = C[C] \cap \GL_n(\C)$.

\subsection*{Étape 4}
Montrons que $\C[C]^{\times}$ est un ouvert connexe de $\C[C]$.

$\C[C]^{\times}$ est ouvert comme intersection de $\C[C]$ et de l'ouvert
$\det^{-1}(\C^*) = \GL_n(\C) \subset \M_n(C)$.

Pour montrer que $\C[C]^{\times}$ est connexe, on va montrer qu'il est
connexe par arcs.

\begin{mdframed}
  En effet, un ouvert $U$ d'un $\C$-espace vectoriel est connexe si et
  seulement s'il est connexe par arcs.
  \begin{itemize}
    \item On suppose connu le fait que $[0,1]$ est connexe. 

      Supposons que $U$ est connexe par arcs.

      Soit $f \colon U\to \{0,1\}$ continue. Montrons que $f$ est
      constante.

      Soit $x,y \in U$. Alors il existe $\gamma \colon [0,1] \to U$
      continue telle que $\gamma(0) = x$ et $\gamma(1) = y$. Ainsi,
      $f(x) = f \circ \gamma(0)$ et $f(y) = f \circ \gamma(1)$. Or,
      $[0,1]$ est connexe et on considère l'application $f \circ \gamma
      [0,1] \to \{0,1\}$. Donc $f \circ \gamma$ est constante, et en
      particulier $f(x) = f(y)$ et donc $f$ est constante.

    \item Supposons que $U$ est connexe.

      Soit $x_0 \in U$. Posons \[ C = \{ x \in U : \exists \gamma \colon
          [0,1] \to U \text{ continue telle que } \gamma(0) = x_0 \text{
      et } \gamma(1) = x\}. \] Montrons que $C$ est ouvert.

      Soit $x \in C$. Comme $U$ est ouvert, il existe $\varepsilon > 0$
      tel que $\B(x,\varepsilon) \subset U$. Soit $y \in
      \B(x,\varepsilon)$. Posons \[ \begin{array}{rcl}
          \gamma_0 \colon [0,1] & \to & U \\
                              t & \mapsto tx + (1 - t)y\end{array} . \]
      L'image de ce chemin est dans $\B(x,\varepsilon)$ qui est convexe
      et donc c'est bien inclus dans $U$.

      En prenant $\gamma_1 = \gamma * \gamma_0$, on obtient bien le
      résultat.

      Montrons que $C$ est fermé.

      Soit $(x_n)$ une suite d'éléments de $C$ qui converge vers $x \in
      U$.

      Comme $U$ est ouvert, il existe $\varepsilon > 0$ tel que
      $\B(x,\varepsilon) \subset U$. Il existe $n_0 \in \N$ tel que
      $x_{n_0} \in \B(x,\varepsilon)$ et $x_{n_0 - 1} \notin
      \B(x,\varepsilon)$. Il suffit alors de prendre le chemin de $x_0$
      à $x_{n_0}$ et celui de $x_{n_0}$ à $x$.
  \end{itemize}
\end{mdframed}

Soit $M,N \in \C[C]^{\times}$. On sait que pour tout $z \in \C$, $zM +
(1 - z)N \in \C[C]$. On doit trouver un chemin de cette forme qui soit
aussi dans $\GL_n(\C)$.

On remarque que la fonction $P \colon z \to \det(zM + (1 - z)N)$ est
polynômiale en $z$.

Cmme c'est un polynôme non nul ($P(0) = \det(N)$ et que $N$ est
inversible), il ne s'annule que sur un nombre fini de points.

Il est donc aisé de construire une fonction $t \mapsto z(t)$ qui évite
ces points, tels que $z(0) = 0$ et $z(1) = 1$. En posant $\gamma(t) =
z(t) M + (1 - z(t)) N$. On obtient bien le résultat.

\subsection*{Étape 5} Montrons que $\exp(\C[C])$ est ouvert et germé
dans $\C[C]^{\times}$.

Montrons que $\exp(\C[C])$ est ouvert.

On sait que $D_{0_{M_n(\C)}} \exp = I_n$ donc $\det(D_{0_{M_n(\C)}})
\neq 0$. On sait aussi que $\exp$ est $\Co^1$. On consdière $\exp \colon
\C[C] \to \C[C]$. Le théorème d'inversion locale permet d'affirmer qu'il
existe un ouvert $U$ contenant $0_{M_n(\C)}$ dans  $\C[C]$ et $V$
contenant $I_n$ dans $\C[C]$ tels que $\exp \colon U \to V$ soit un
$\Co^1$-difféomorphisme. En particulier, $\exp(\C[C])$ contient un
voisinage de $I_n$.

Soit $A \in \C[C]$. Comme $\exp|_{\C[C]}$ est un morphisme de groupes,
on a \[ \exp(A + U) = \exp(A) \exp(U) = \exp(A)V. \] Comme $A$ est
inversible, on a \[ \begin{array}{rcl}
    f \colon \C[C] & \to & \C[C] \\
M & \mapsto & \exp(A)^{-1}M \end{array} \] est un $\Co^1$-difféomorphisme.
Ainsi, $\exp(A)V = f^{-1}(V)$ est un ouvert. Comme $I_n \in V,
\exp(A) \in \exp(A)V$.

Ainsi, $\exp(A)V$ est un voisinage ouvert de $\exp(A)$ dans $\C[C]$ et
par conséquent, $\exp(\C[C])$ contient un voisinage de chaucun de ses
points, c'est donc un ouvert.

\begin{mdframed}
  Soit $G$ un groupe et $H$ un sous groupe. On sait que les classes à
  gauche forment une partition de $G = \bigcup_{a\in G}aH$.

  Ainsi, on obtient $G \setminus H = \bigcup_{\substack{a\in G \\ a
  \notin H}} aH = \bigcup_{a \in G \setminus H} aH$.
\end{mdframed}

Montrons que $\exp(\C[C])$ est fermé. Pour cela, on montre que son
complémentaire est ouvert. Si on interprète le complémentaire comme une
réunion d'ouverts de la forme $A \exp(\C[C])$, on obtient que
$\C[C]^{\times} \setminus \exp(\C[C])$ est ouvert donc $\exp(\C[C])$ est
fermé.

\subsection*{Étape 6}
Montrons que $\exp \colon \M_n(\C) \to \GL_n(\C)$ est surjective.

D'après ce qui précède, $\exp(\C[C])$ est fermé, ouvert, non vide. On a
donc $\exp(\C[C]) = \C[C]^{\times}$. Soit $C \in \GL_n(\C)$. On a
clairement que $C \in \C[C]$ et donc $C \in \C[C]^{\times}$. Par ce qui
précède, $C \in \exp(\C[C])$ et en particulier, $C \in \Im(\exp)$. \qed


\end{document}

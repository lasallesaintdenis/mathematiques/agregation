\documentclass[a4paper,10pt,,twocolumn,landscape,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Ccal}{\mathcal{C}}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{\today}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother


\newcommand{\K}{\mathbf{K}}
\newcommand{\ssi}{si, et seulement si }

\renewcommand{\implies}{\DOTSB \;\Rightarrow \;}
\renewcommand{\iff}{\DOTSB \;\Leftrightarrow \;}

\everymath{\displaystyle\everymath{}}

\begin{document}
\maketitle

Dans cette leçon, $f$ est une fonction de $\R$ dans une partie de $\C$.

\section{Introduction, définitions et premières propriétés}

Soit $G=\{a\in\R | \forall x\in\R, f(x+a)=f(x)\}$ un sous groupe additif
de $\R$.

\begin{definition}
  S'il existe $a\in G\backslash\{0\}\neq\emptyset$, on dit que $a$ est
  la période de la fonction et que $f$ est $a$-périodique.
\end{definition}

Si $f$ est continue, $G$ est fermé et ne peut être que :
\begin{itemize}
  \item $R$ et $f$ est constante ;
  \item $\{0\}$ et $f$ n'est pas périodique ;
  \item $T\mathbf{Z}$, avec $T>0$, la plus petite période de $f$.
\end{itemize}

$\Ccal$ est l'ensemble des fonctions continues $2\pi$-périodiques.
C'est une algèbre de Banach pour le produit usuel et pour la norme
infinie ($\lVert f \rVert_{\infty}=
\substack{\mathop{Sup}\\0<t<2\pi}\lvert f(t)\rvert$)

$f\in\Ccal \iff \exists g\in\mathcal{C}^0(\mathcal{U}), f(t) =
g(e^{it}) \ \forall t\in\R$ où $\mathcal{U}$ est l'ensemble des nombres
complexes de module 1.

$\lVert f\rVert_p = \frac1{2\pi}
\left(\int_0^{2\pi}(f(t)^p\mathrm{d}t\right)^{\frac1p}$ est une norme
d'espace vectoriel sur $\Ccal$, pour $p<\infty$ pour les fonctions de
$L^p$

$\langle f,g\rangle =
\frac1{2\pi}\int_0^{2\pi}f(t)\overline{g(t)}\mathrm{d}t$ est un produit
scalaire sur $L^2$ et les $(e_n)_{n\in\mathbf{Z}}$ forment un système
orthonormal de l'espace de Hilbert $L^2$.

Les fonctions $e_n\colon t\mapsto e^{it} \in \Ccal$ sont les polynômes
trigonométriques. $\sum_{n\in \mathbf{Z}}c_ne_n$ est une série
trigonométrique. Les polynômes trigonométriques forment un sous-espace
vectoriel dans $\Ccal$, avec $a_n\in\C$.

On peut également écrire les polynômes trigonométriques sous la forme
suivante $\frac{a_0}2 + \sum_1^N (a_n\cos nt +b_n\sin nt)$, en posant
$a_n=c_n+c_{-n}$ et $b_n=i(c_n-c_{-n})\ \forall n$.

\begin{proposition}
  Si $\sum_{n\in\mathbf{N}}\lvert c_n\rvert$ et
  $\sum_{n\in\mathbf{N}}\lvert c_{-n}\rvert$
  ($\sum_{n\in\mathbf{N}}\lvert a_n\rvert$ et
  $\sum_{n\in\mathbf{N}}\lvert b_n\rvert$) convergent, alors
  $\sum_{n\in\mathbf{Z}} c_ne_n$ converge normalement et sa somme
  définit une fonction continue $2\pi$-périodique sur $\R$.
\end{proposition}

\begin{proposition}
  Si les suites $(c_n)_{n\in\N}$ et $(c_{-n})_{n\in\N}$ sont
  réelles, décroissantes, de limite nulle, alors $\sum_{n\in\Z}c_ne_n$
  converge simplement sur $\R$ et uniformément sur les intervalles de la
  forme $[2k\pi+\alpha,2(k+1)\pi+\alpha]$, $0<\alpha<\pi$.
\end{proposition}

Si $f\in L^1 \ c_n(f) =
\frac1{2\pi}\int_0^{2\pi}f(t)e^{-int}\mathrm{d}t$

Si $f\in L^2 \ c_n(f) = \langle f,e_n\rangle$

Pour $f\in L^2$, on pose $\gamma(f)=(c_n(f))_{n\in\mathbf{Z}}$.

On pose $S_N(f)=\sum_{-N}^Nc_n(f)e_n$, la somme partielle des
coefficients.

Si $f$ est dans $\Ccal$ et $\Ccal^1$ par morceaux, alors $c_n(f') =
inc_n(f)$

Si $\sum_{-N}^N\alpha_ne_n$ converge uniformément vers $f$, alors
$f\in\Ccal$ et $\alpha_n=c_n(f)$

\newcommand{\lambdainf}{\lvert\lambda\rvert\rightarrow\infty}

\begin{lemme}[de Riemann-Lebesgue]
  Soit $a,b \in \R\ a<b,\ f\in L^1[a,b],\ \lambda\in\R$. Alors :
  \[\left\lbrace\begin{array}{ccccc}
      \lim_{\lambdainf}\int_a^bf(t)e^{i\lambda t}\mathrm{d}t & = &
      \lim_{\lambdainf}\int_a^bf(t)\cos\lambda t\mathrm{d}t & & \\
      & = & \lim_{\lambdainf}\int_a^bf(t)\sin\lambda t\mathrm{d}t & = & 0
  \end{array}\right.\]
\end{lemme}

\begin{proposition}
  \begin{enumerate}
    \item Les $S_N(f)$ convergeant uniformément forment un espace de
      Banach $U$ pour la norme $\lVert f\rVert_U=\mathop{sup}_{N\geq 0}
      \lVert S_N(f)\rVert_{\infty}$ ;
    \item Les $S_N(f)$ convergeant normalement forment une algèbre de
      Banach $A$ incluse dans l'espace précédent. Cette algèbre est une
      algèbre de Banach pour le produit usuel et pour la norme $\lVert
      f\rVert_A=\sum_{-\infty}^{\infty}\lvert f\rvert$.
  \end{enumerate}
\end{proposition}

Pour $f$ et $g$ dans $L^1$,
$\frac1{2\pi}\int_0^{2\pi}f(x-t)g(t)\mathrm{d}t =: (f*g)(x)$ définit un
produit de convolution en $x$ de $f$ et $g$.

\begin{proposition}
  $L^1$ est une algèbre de Banach commutative (non unitaire) pour le
  produit de convolution et la norme $\lVert\phantom{f}\rVert_1$.
\end{proposition}

\begin{proposition}
  L'application $\gamma$ est un homomorphise d'algèbre de norme 1 de
  $L^1$ dans $c_0$, cad : $\gamma$ est linéaire et $\gamma(f*g) =
  \gamma(f)\gamma(g)$.
\end{proposition}

$c_0$ est l'ensemble des suites complexes de limites nulles.

\newcommand{\Sp}{\mathop{\mathrm{Sp}}}
\begin{definition}
  $\Sp f := \{n\in\mathbf{Z}|c_n\neq0\}$ est le spectre de $f$.
\end{definition}

\begin{definition}~\\[-9mm]

  $D_N=\sum_{-N}^Ne_n$ est le noyau de Dirichlet d'ordre $N$
\end{definition}

$S_N(f) = D_N*f,\ \forall f\in L^1,\ \forall N\geq 0$

$\sigma_N(f) = \frac{S_0(f) + S_1(f) + \dots + S_{N-1}(f)}N$ est la
somme de Fejér d'indice $N$ de $f$.

\section{Principaux théorèmes de convergence}

\begin{theoreme}[Théorème de convergence de Fejér]
  \begin{enumerate}
    \item Soit $f\in\Ccal$ ; alors $\lVert \sigma_N(f)\rVert_\infty \leq
      \rVert f\lVert_\infty\ \forall N\geq 1$, et $\lim_{N\to\infty}
      \lVert\sigma_N(f) - f\rVert_\infty = 0$ .
    \item Soit $f\in L^p\ (1<p<\infty)$ ; alors $\lVert \sigma_N(f)\rVert_p \leq
      \rVert f\lVert_p\ \forall N\geq 1$, et $\lim_{N\to\infty}
      \lVert\sigma_N(f) - f\rVert_p = 0$ .
  \end{enumerate}
\end{theoreme}

\begin{theoreme}
  \begin{enumerate}
    \item Soit $f\in\Ccal,\ x_0\in\R$; si $S_N(f,x_0)\to l$,
      alors $l=f(x_0)$ ;
    \item Soit $f\in U$ ; alors $f=\sum_{-\infty}^\infty c_n(f)e_n$ ;
    \item $(e_n)_{n\in\mathbf{Z}}$ forment une base orthonormale de
      $L^2$ ; en particulier $f\in L^2 \implies \sum_{-\infty}^\infty
      \lvert c_N(f)\rvert^2 = \lVert f\rVert_2^2$ et $\lim_{N\to\infty}
      \lVert S_N(f) - f\rVert =0$.
    \item Soit $f\in\Ccal,\ f\in C^1$ par morceaux ; alors $f\in A$ et $f
      = \sum_{-\infty}^\infty c_n(f)e_n$
    \item Si $f,g\in \Ccal, \gamma(f)=\gamma(g) \iff f = g$ ; si $f,g\in
      L^1, \gamma(f)=\gamma(g) \iff f = g$ presque partout.
  \end{enumerate}
\end{theoreme}

\begin{theoreme}[Théorème de Dirichlet]
  Soit $f \colon \R \to \C$, $2\pi$-périodique, mesurable, intégrable
  sur $[0 ; 2\pi]$ ; soit $x_0\in\R$ ; on suppose que :
  \begin{itemize}
    \item[] $\lim_{t\stackrel{>}{\to}0}f(x_0+t) =\colon f^+$ et
      $\lim_{t\stackrel{<}{\to}0}f(x_0+t) =\colon f^-$ existent.
    \item[] $\int_0^\delta\frac{\lvert
      f(x_0+t)-f^+\rvert}t\mathrm{d}t<\infty$ $\int_0^\delta\frac{\lvert
      f(x_0-t)-f^-\rvert}t\mathrm{d}t<\infty$
  \end{itemize}
  Alors on a :
  \[ \lim_{N\to\infty} S_N(f,x_0) = \frac12(f^++f^-) \]
\end{theoreme}

\begin{theoreme}[Égalité de Parseval]
  Soit $f\colon\R\to\C$, continue par morceaux et vérifiant les
  hypothèses du théorème de Dirichlet.

  Alors : $\sum_{n\in\Z}\lvert c_n(f)\rvert^2$, $\sum_{n\in\N}\lvert
  a_n(f)\rvert^2$ et $\sum_{n\in\N}\lvert b_n(f)\rvert^2$ convergent et
  \[ \sum_{-\infty}^\infty \lvert c_n(f)\rvert^2 =
    \frac1{2\pi}\int_0^{2\pi} \lvert f(t)\rvert^2\,\mathrm{d}t =
    \frac{a_0^2}4 + \frac12\sum_1^\infty (\lvert a_n\rvert ^2 + \lvert
  b_n\rvert^2)\]

\end{theoreme}

\section{Exemples de développements en série de Fourier}

Soit $f$ une fonction de $L^1$. Alors :

$f$ paire $\implies c_n(f) = \frac1\pi\int_0^\pi f(t)\cos nt\mathrm{d}t$
et $S_N(f,t) = c_0(f) + 2\sum_0^N c_n(f)\cos nt$.

$f$ impaire $\implies c_n(f)i = \frac{-i}\pi\int_0^\pi f(t)\sin
nt\mathrm{d}t$ et $S_N(f,t) = 2i\sum_0^N c_n(f)\sin nt$.

\begin{exemple}[Fonction signal]
  Soit $\e\in ]0;\pi[$ et $\sigma_\e$ la fonction de $L^\infty$ telle
  que $\sigma_\e=1$ si $\lvert t\rvert\leq\e$ et 0 sinon.

  $\sigma_\e$ est paire et $c_n(\sigma_\e)=\frac1\pi\int_0^\e\cos
  nt\,\mathrm{d}t=\left\lbrace\begin{array}{cl}\frac{\sin n\e}{n\pi} &
  \text{si } n\neq0\\
\frac\e\pi & \text{si } n=0\end{array}\right.$

  Ici $S_N(\sigma_\e,t) = \frac\e\pi + 2\sum_{-\infty}^\infty \frac{\sin
  n\e}{n\pi}\cos nt$.

\end{exemple}

En utilisant le théorème de Dirichlet au point $x_0=\e$, avec
$\sigma_\e^+=0$ et $\sigma_\e^-=1$, on obtient
$S_N(\sigma_\e,\e)=\frac12$. En posant $a=2\e$, on obtient
\[ \sum_1^\infty\frac{\sin na}n = \pi\left(\frac12
-\frac{a}{2\pi}\right) = \frac{\pi-a}2 \]
pour $0<a<2\pi$.

\renewcommand{\triangle}{\Delta_\e}
\begin{exemple}[Fonction triangle]
Soit $\e\in ]0;\pi[$ et $\triangle$ la fonction de $\Ccal$ définie par
  $\triangle(t)=1-\frac{\lvert t\rvert}{\e}$ si $\lvert t\rvert\leq\e$
  et $\triangle(t)=0$ sinon. $\triangle$ est une fonction paire et une
  intégration par partie montre que pour $n\neq0$, $c_n(\triangle) =
  \frac{1 - \cos n\e}{n^2\pi\e}$ et $c_0(\triangle) = \frac\e{2\pi}$.

  $\sum_{-\infty}^\infty\lvert c_n(\triangle)\rvert) < \infty$, donc on
  $\triangle\in A\subset U$

  \[\triangle(t) = \lim_{N\to\infty}S_N(\triangle,t) = \lim_{N\to\infty}
    \left(\frac\e{2\pi} + 2\sum_1^N \frac{1-\cos n\e}{n^2\pi\e}\cos
  nt\right)\]

  \[\triangle(t) = \frac\e{2\pi} + 2\sum_1^\infty \frac{1-\cos
  n\e}{n^2\pi\e}\cos nt\]
\end{exemple}

En posant $\e=\pi$ et $t=0$, on a $\sum_{n\neq0} \lvert c_n(\Delta_\pi)
\rvert =\frac12$, ce qui conduit aux égalités suivantes :

\[ \sum_0^\infty \frac1{(2p+1)^2} = \frac{\pi^2}8 \text{ et }
  \sum_0^\infty \frac1{n^2} = \frac43 \sum_0^\infty \frac1{(2p+1)^2} =
\frac{\pi^2}6\]

\begin{exemple}[Exponentielle apériodique]
  Soit $a\in\C\backslash\Z$ et $f\in L^\infty$ définie par :
  $f(t)=e^{iat}$ si $-\pi\leq t<\pi$.

  $c_n(f)=(-1)^n\frac{\sin \pi a}{\pi(a-n)}$

  $f$ vérifie les hypothèses du théorème de Dirichlet :

  $S_N(f,\pi)=\cos\pi a$

  \[\pi\cot\pi a =\frac1a +2a\sum_1^\infty \frac1{a^2-n^2} \ \
  (a\in\C\backslash\Z)\]
\end{exemple}

On trouve les applications suivantes :

\begin{exemple}[Équation de la chaleur]
  Application «historique« : résolution d'équation aux dérivées
  partielles.

  On pose $Q=]0,L[\times]0,+\infty[$ et $\overline{Q} =
  [0,L]\times[0,+\infty[$, et on cherche une fonction $u\in
  C^0(\overline{Q})$ et $u\in C^2(Q)$, qui satisfait
  \[ \frac{\partial u}{\partial t} - \frac{\partial^2 u}{\partial x^2}
  =0\ \ \text{dans}\ Q\]

  On cherche une solution de la forme $u(x,t)=f(x)g(t)$, ce qui est
  équivalent à $\frac{f''(x)}{f(x)} = \frac{g'(t)}{g(t)},\ \forall x\in
  ]0,L[,\ \forall t\in ]0,+\infty[$ (on suppose que $f$ et $g$ ne
  s'annule pas).

  La seule possibilité est d'avoir $f''(x)=\lambda f(x)$ et
  $g'(t)=\lambda g(t)$.

  Les solutions avec $\lambda\geq 0$ conduisent à des contradictions
  avec les conditions initiales : $u(0,t)=u(L,t) =0$ et $u(x,0)=h(x)$.

  La famille de solutions qui conviennent est de la forme $u_n(x,t) =
  b_n\sin\frac{n\pi}Lxe^{-\frac{n^2\pi^2}{L^2}t}$ et une combinaison
  linéaire des éléments de cette famille est encore solution. Prenons
  $u(x,t)=\sum_{n=1}^{+\infty}b_n\sin\frac{n\pi}Lxe^{-\frac{n^2\pi^2}{L^2}t}$.

  En supposons qu'on puisse dériver sous le signe somme, cette fonction
  satisfait aux conditions aux limites. Pour $t=0,\ h(x) =
  \sum_{n=1}^{+\infty}b_n\sin\frac{n\pi}Lx$, et cette égalité traduit le
  fait que les coefficients $b_n$ sont les coefficients de Fourier d'une
  certaine série imapire.
\end{exemple}

\begin{exemple}[Inégalité isopérimétrique]
  Soit $[a;b]$ un intervalle compact de $\R$ et $\gamma\colon[a;b]\to\C$
  une courbe de classe $C^1$ par morceaux de longueur $L$ enfermant une
  surface $S$.

  Alors : $L^2\geq 4\pi S$ et $L^2=4\pi S \iff \gamma$
  définit un cercle parcouru une fois (peu importe le sens).
\end{exemple}

\end{document}

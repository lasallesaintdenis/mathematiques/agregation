\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème de Stone Weierstrass}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

Soit $(X,d)$ un compact non vide.

On note $\Co(X,\R) = \{ f \colon X \to \R \text{ continue} \}$ munie de
la norme $\lVert \cdot \rVert_{\infty}$ définie par $\lVert f
\rVert_{\infty} = \sup_{x\in X} \lvert f(x) \rvert$ pour $f \in
\Co(X,\R)$.

\begin{rappel}[Théorème de Dini] Supposons $X$ est compact et $(f_n)_n$
  est une suite de fonctions continues de $X$ dans $\R$. On suppose que
  \begin{enumerate}
    \item $(f_n)$ est croissante, c'est-à-dire que pour tout $n \in
      \N^*$ et pour tout $x \in X, f_n(x) \leqslant f_{n+1}(x)$ ;
    \item $(f_n)$ converge simplement vers $f$.
  \end{enumerate}
  Alors $(f_n)$ converge uniformément vers $f$.
\end{rappel}

\begin{lemme}
  On suppose que $X$ contient au moins deux éléments. Soit $H$ une
  partie de $\Co(X,\R)$ vérifiant les deux conditions suivantes
  \begin{enumerate}
    \item pour tout $u,v \in H, \sup(u,v) \in H$ et $\inf(u,v) \in H$ ;
    \item pour tout $(x_1,x_2) \in X^2$ tels que $x_1 \neq x_2$, pour
      tout $\alpha_1, \alpha_2 \in \R,$ il existe $u \in H$ telle que
      $u(x_1) = \alpha_1$ et $u(x_2) = \alpha_2$.
  \end{enumerate}
  Alors $H$ est dense dans $\Co(X,\R)$.
\end{lemme}

\begin{definition}
  Une partie $H$ de $\Co(X)$ telle que pour tout $(x,y) \in X$ avec $x
  \neq y$, il existe $h \in H$ pour lequel $h(x) \neq h(y)$ est dite
  \emph{séparante}.
\end{definition}

\begin{definition}
  Une partie $H$ de $\Co(X,R)$ est dite \emph{réticulée} si pour tout
  couple $(f,g) \in H, \sup(f,g) \in H$ et $\inf(f,g) \in H$.
\end{definition}

\begin{remarque} Un sous-espace vectoriel $H$ de $\Co(X,\R)$ est
  réticulé si et seulement si pour tout $h \in H, \lvert h \rvert \in
  H$.
\end{remarque}

\begin{lemme}
  Si $H$ est un sous-espace vectoriel de $\Co(X,\R)$ réticulé, séparant
  et contenant les fonctions constantes, alors $H$ est dense dans
  $\Co(X,\R)$.
\end{lemme}

\begin{theoreme}[Stone-Weierstrass réel]
  Toute sous-algèbre de $\Co(X,\R)$ séparante et contenant les fonctions
  constantes est dense dans $\Co(X,\R)$.
\end{theoreme}

\section*{Preuve}

\subsection*{Lemme 1}
Soit $f \in \Co(X,\R)$ et $\varepsilon > 0$. On cherche à approcher $f$
à $\varepsilon$ près par un élément de $H$. Fixons $x \in X$. D'après
l'hypothèse (ii), pour tout $ y \neq x$ il existe $u_y \in H$ telle que
$u_y(x) = f(x)$ et $u_y(y) = f(y)$.

Soit \[ \O_y  = \{ x' \in X, u_y(x') > f(x') - \varepsilon \} . \]
Pour tout $y\in X, \O_y$ est un ouvert contenant $x$ et $y$. On a donc
$X = \bigcup_{x\neq y} \O_y$ et par la propriété de Borel-Lebesgue, $X$
peut être recouvert par un nombre fini de $\O_y$. Ainsi, $X = \bigcup_{j
= 1}^r \O_{y_j}$, avec $y_j \neq x$ pour tout $j \in \llbracket 1, r
\rrbracket$.

Soit alors \[ v_x = \sup_{i \in \llbracket 1, r \rrbracket} \left(
u_{y_j} \right) .\] Par une récurrence immédiate, on a $v_x \in H$. On
sait aussi que $v_x(x) = f(x)$. Pour tout $x' \in X$, il existe $j \in
\llbracket 1, r \rrbracket$ tel que $x' \in \O_{y_j}$. Ainsi pour tout
$x' \in X, v_x(x') \geqslant u_{y_j}(x') > f(x') - \varepsilon$.

Par un raisonnement analogue, en posant \[ \Omega_x = \{ x' \in X,
v_x(x') < f(x') + \varepsilon \} \] et en prenant l'$\inf$ au lieu du
$\sup$, on obtient que $v(x') \geqslant v_{x_j}(x') < f(x') +
\varepsilon$.

On obtient donc  que $\lVert f - v \rVert_{\infty} < \varepsilon$ \qed

\subsection*{Lemme 4}

Si $X$ contient un seul élément, la résultat est clair.

Supposons que $X$ contient au moins deux éléments. Il nous suffit donc
de vérifier le (ii) du lemme précédent.

Soit $(x_1,x_2) \in X$ tels que $x_1 \neq x_2$. Comme $H$ est
spérarante, il existe $h \in H$ telle que $h(x_1) \neq h(x_2)$. Soit
$(\alpha_1, \alpha_2) \in \R^2$. Le système d'équation \[ u_n =
  \begin{cases} \lambda h(x_1) + \mu = \alpha_1 \\ \lambda h(x_2) + \mu
= \alpha_2 \end{cases} \] est un système de Cramer (le déterminant de la
matrice du système est non nul). Ainsi, il admet une unique solution
$(\lambda,\mu) \in \R^2$. Posons $f = \lambda h + \mu$. Comme $H$ est un
espace vectoriel, $f \in H$ et $f(x_1) = \alpha_1$ et $f(x_2) =
\alpha_2$. \qed


\subsection*{Théorème 5}
Si $H$ est une sous-algèbre de $\Co(X,\R)$ séparante et contenant les
fonctions constantes, alors il en est de même pour son adhérence
$\overline{H}$.

Montrons que $\overline{H}$ est réticulée afin d'appliquer le lemme
précédent.\\ Soit $f \in \overline{H}$ différente de la fonction nulle.
On définit la suite de polynômes $(P_n)_{n\in\N} \in \R[X]$ telle que \[
  \begin{cases}
    P_0 = 0 \\ \forall n \in \N^*, \forall x \in [-1,1], P_{n+1}(x) =
P_n(x) + \frac{1}2(x^2 - P_n^2(x)) \end{cases} . \]
Montrons que, pour tout $n \in \N$, pour tout $x \in [-1,1]$, \[ 0
\leqslant P_n(x) \leqslant P_{n+1}(x) \leqslant \lvert x \rvert .\]

Par récurrence sur $n \in \N$ :
\begin{itemize}
  \item Pour $n = 0$, $P_0 = 0 \leqslant P_1(x) = \frac{1}2x^2 \leqslant
    \lvert x \rvert $
  \item L'hypothèse de récurrence garantit que $0 \leqslant P_{n+1}(x)$.
    On sait que $P_{n+2}(x) = P_{n+1}(x) + \frac{1}2(x^2 -
    \underbrace{P_{n+1}^2(x)}_{\leqslant \lvert x \rvert})$ d'où
    $P_{n+2}(x) \geqslant P_{n+1}(x)$ pour tout $x \in [-1,1]$.

    Par un jeu de réécriture, on a \[ P_{n+2}(x) = \lvert x \rvert -
    (\lvert x \rvert - P_{n+1}(x))(1 - \frac{1}2 (\lvert x \rvert +
  P_{n+1}(x))) \geqslant 0. \]
\end{itemize}

On a donc bien le résultat. $(P_n)$ est croissante et majorée donc elle
converge vers $f$ telle que, pour tout $x \in [-1,1], f(x) \leqslant
\lvert x \rvert$ et en faisant tendre $n$ vers $+\infty$ dans la
relation de récurrence, on obtient $f^2(x) \leqslant \lvert x \rvert$.\\
D'après le théorème de Dini, on a même le convergence uniforme. Ainsi,
$\left(P_n\left(\frac{f}{\lVert f \rVert_{\infty}}\right)\right)_n$
converge uniformément vers $\frac{f}{\lVert f \rVert_{\infty}}$. Par
conséquent, $\left(\lVert f \rVert_{\infty} P_n\left(\frac{f}{\lVert f
\rVert_{\infty}}\right)\right)_n$ converge uniformément vers $\lvert f
\rvert$. Comme $H$ est une sous-algèbre et contient les fonctions
constantes, on a $\left(\lVert f \rVert_{\infty}
P_n\left(\frac{f}{\lVert f \rVert_{\infty}}\right)\right) \in
\overline{H}$. Ainsi, comme $\overline{H}$ est fermé, $\lvert f \rvert
\in \overline{H}$.\\ Par conséquent, $H$ est réticulée.

En appliquant le lemme précédent, $\overline{H}$ est dense dans
$\Co(X,\R)$. Ainsi, $H$ est dense dans $\Co(X,\R)$. \qed


\end{document}

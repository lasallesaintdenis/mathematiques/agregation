// Estimation de la probabilité de ruine

function pmax=perte_maxi(mu,lambda,p,n_max)
// Renvoie la perte maximale pour 1 simulation issue de 0, de paramètres donnés

  X=grand(1,n_max,'exp',lambda); // sinistres
  intertemps=grand(1,n_max,'exp',1/mu); // intervalles entre sinistres
  s=[0,cumsum(X-p*intertemps)]; // bénéfice après le sinistre suivant
  
  pmax=max(s);
endfunction


mu=3; // intensité des sinistres
lambda=5; // espérance du coût d'un sinistre
p=17; // taux de cotisation

n_max=1000; // durée de la simulation (en nb de sinistres)
nb=1000; // nombre de simulations pour l'estimation

pertes=[];
for i=1:nb
  pertes=[pertes,perte_maxi(mu,lambda,p,n_max)];
end

scf(1);clf;
plot2d2(gsort(pertes),(1:nb)/nb)
xtitle('Probabilité de ruine, pour une réserve donnée','$c$','$\psi(c)$')

scf(2);clf;
plot2d2(gsort(pertes),log((1:nb)/nb))
xtitle('Probabilité de ruine, pour une réserve donnée (ordonnée logarithmique)','$c$','$\log(\psi(c))$')

x=gsort(pertes);
[a,b,sig]=reglin(x,log((1:nb)/nb));
plot(x,a*x+b,'r');
legend(['Estimation','Régression'],1);

scf(1);
plot(x,exp(a*x+b),'r');
legend(['Estimation','Régression (du log)'],1);

disp('psi(c)='+string(exp(b))+'*exp('+string(a)+'*c)');

disp('lambda*mu/p='+string(lambda*mu/p));
disp('(p-lambda*mu)/(p*lambda)='+string((p-lambda*mu)/(p*lambda)));

disp(sig)

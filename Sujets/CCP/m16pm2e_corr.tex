\documentclass[12pt,frenchb,a4paper]{scrartcl}

\input{../../../commons.tex.inc}

\title{Corrigé CCP 2016}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}

\maketitle

\thispagestyle{fancy}

\section{Problème}

Soit $n$ un entier naturel non nul

\begin{enumerate}
  \item Soient $P$ et $Q$ deux polynômes non nuls à coefficients
    complxes.
    \begin{enumerate}
      \item On veut démontrer que si $P$ et $Q$ n'ont aucune racine
        complexe commune, alors ils sont premier entre eux.

        Supposons qu'ils ont une racine complexe commune et notons là
        $\alpha$. Alors il existe deux polynômes de degrés inférieurs
        $P_0$ et $Q_0$ tels que $P = (X - \alpha)P_0$ et $Q = (X -
        \alpha)Q_0$ et donc alors $P$ et $Q$ ne sont pas premiers entre
        eux.
      \item On suppose désormais $P$ et $Q$ premiers entre eux. Si $P$
        divise un polynôme $R$ à coefficient complexe, alors il existe
        un polynôme $A$, à coefficients complexes tel que $R = AP$. De
        même, si $Q$ divise $R$, alors il existe un polynôme $B$ à
        coefficients complexes tel que $R = BQ$. Or $P$ et $Q$ sont
        premiers entre eux, donc il existe deux polynômes $U,V$ à
        coefficients complexes tel que $UP + VQ = 1$. On peut donc écrire $R
        = RUP + RVQ = BQUP + APVQ = PQ(BU + AV)$.

        Donc $R$ est bien divisible par $PQ$.
    \end{enumerate}
  \item Soit $(P_i)_{1 \leq i \leq n}$ une famille de polynômes non nuls
    de $\R[X]$. On considère le polynôme $P \in \R[X]$ et la fraction
    rationnelle $Q \in \R(X)$ définis par $P =
    \displaystyle{\prod_{i=1}^n P_i }$ et $Q' =\dfrac{P'}{P}$.

    Montrons par récurrence que $Q = \displaystyle{\sum_{i=1}^n
    \frac{P'_i}{P_i}}$.

    Pour $n = 1$, la récurrence est initialisée.

    Soit $n$ un entier naturel et supposons la proposition «$Q =
    \displaystyle{\sum_{i=1}^n\frac{P'_i}{P_i}}$» vraie jusqu'au rang
    $n$. Écrivons $P = \displaystyle{\prod_{i=1}^{n+1} P_i }$ sous la
    forme $P = \displaystyle{\prod_{i=1}^n P_i } P_{n+1}$.

    On a alors
    $P' = \brk*{\displaystyle{\prod_{i=1}^n P_i }}' P_{n+1} +
    \brk*{\displaystyle{\prod_{i=1}^n P_i }} P'_{n+1}$

    En divisant par $P$, on a alors $Q + \dfrac{P'_{n+1}}{P_{n+1}}$, ce
    qui achève la récurrence.

    Conclusion, pour tout entier $n \geq 1$, on a $Q =
    \displaystyle\sum_{i=1}^n \frac{P'_i}{P_i}$.
\end{enumerate}

Interpolation de Hermite

Soit $I$ un intervalle non vide de $\R$, $p$ un entier naturel non nul,
$(x_i)_{1 \leq i \leq p}$ une famille d'éléments de $i$ distincts deux à
deux et $(a_i)_{1 ≤i ≤p}$ et $(b_i)_{1 ≤i ≤p}$ deux familles de réels
quelconques.

\begin{enumerate}[resume]
  \item
    \begin{enumerate}
      \item Soit $P \in \R[X]$ et $a$ un réel. On suppose que $P(a) =
        P'(a) = 0$. Montrons alors que $(X - a)^2$ divise $P$.

        Pour tout $x$ réel, on a $\tilde{P}(x) = P(a) + (x - a)P'(a) +
        \dfrac{1}{2} (x - a)^2P"(a) + (x - a)^2 R(x)$ où $R(a) ≠ 0$.

        On en déduit que $a$ est une racine double de $P$ et donc que
        $(X - a)^2$ divise $P$.
      \item Montrons que l'application $\varphi \colon \R_{2p - 1}[X]
        \to \R^{2p}$ définie par \[ \varphi(P) = \brk*{ P(x_1), \dots ,
        P(x_p), P'(x_1), \dots , P'(x_p)} \] est une application
        linéaire bijective de $\R_{2p-1}[X]$ sur $\R^{2p}$.

        Le caractère linéaire de $\varphi$ est une conséquence directe
        du fait que l'application qui à un polynôme associe sa valeur en
        un point est une application linéaire (et même une forme), ainsi
        que la linéarité de la dérivation des polynômes.

        Il faut donc démontrer l'aspect bijectif. Intéressons nous à
        $\ker \varphi$. Il s'agit donc des polynômes de degré $2p - 1$
        qui comportent $2p$ racines nulles (en effet, les $P'(x_1) =
        P(x_1) = 0$ entraîne que $x_1$ est une racine double). On a donc
        $\ker \varphi = \brk[c]{0}$ et donc $\varphi$ est injective.

        Quant à la surjectivité de $\varphi$, il suffit pour ça de
        prendre des polynômes qui conviennent !

        $\varphi$ est donc une application linéaire bijective.
      \item Puisque $\varphi$ est une application linéaire bijective, il
        existe un unique polynôme $P_H$ tel que $\varphi(P_H) = \brk{a_1,
        \dots , a_p, b_1, \dots , b_p}$.
    \end{enumerate}
  \item On se place dans le cas où $p = 2, x_1 = -1, x_2 = 1, a_1 = 1,
    a_2 = 0, b_1 = -1 $ et $b_2 = 2$.

    On cherche donc un polynôme $P$ tel que $(P(-1), P(1), P'(-1),
    P'(1)) = (1,0,-1,2)$ sous la forme d'un polynôme de degré 3.

    On est donc amené à résoudre le système suivant : \[ \left\{
      \begin{array}{lcr}
        -a + b -c + d &= & 1 \\
        a + b + c + d & = & 0 \\
        3a - 2b + c &= & -1 \\
      3a + 2b + c &=& 1 \end{array} \right. \]
      dont les solutions sont $a = , b = , c= $ et $d = $.
    \item Pour tout entier $i$ tel que $ 1 ≤ i ≤ p$ , on considère le
      polynôme $\displaystyle{Q_i = \prod_{\substack{j=1\\ i ≠ j} }^p
      \brk*{\frac{X - x_j}{x_i - x_j}}^2}$
      \begin{enumerate}
        \item Soit $i$ un entier vérifiant $1 ≤ i ≤ p$ et $k$ un entier tel
          que $1 ≤ k ≤ p$. On a facilement que $Q_i(x_k) = 0$, car pour $k ≠
          i$, on annule un des facteurs donc tout le produit. Il reste à
          calculer $Q_i(x_i)$. Tous les facteurs sont tous égaux à 1 et
          donc, $Q_i(x_i) = 1$.

          Calculons $Q'_i(X)$. On a \[Q'_i(X) = \sum_{k=1}^n \frac2{x_i -
            x_j} \prod_{\substack{j=1\\ i ≠ j , j ≠ k} }^p
          \brk*{\frac{X - x_j}{x_i - x_j}}^2  . \] ce qui permet d'écrire
          que $Q'_i(x_k) = 0$ pour $k≠i$ et $Q'_i(x_i) = \displaystyle{
          \sum_{\substack{j=1 \\ j≠i}}^p\frac2{x_i - x_j}}$.
        \item Soit $P$ le polynôme défini par la formule \[ P = \sum_{i=1}^p
            \brk[s]*{\brk*{1 - Q'_i(x_i)\brk*{X - x_i}}a_i +
          \brk*{X - x_i}b_i}Q_i . \]
          On vérifie aisément que $P(x_i) = a_i$ et il faut vérifier que
          $P'(x_i) = b_i$.

          Les termes de la somme se dérivent en $(-Q'_i(x_i)a_i + b_i)Q_i +
          \brk[s]*{\brk*{1 - Q'_i(x_i)\brk*{X - x_i}}a_i + \brk*{X -
          x_i}b_i}Q'_i$ et donc $P' = \displaystyle{ \sum_{i=1}^p
          (-Q'_i(x_i)a_i + b_i)Q_i + \brk[s]*{\brk*{1 - Q'_i(x_i)\brk*{X -
          x_i}}a_i + \brk*{X - x_i}b_i}Q'_i }$. En évaluant en $X = x_i$, on
          trouve $P'(x_i) = (-Q'_i(x_i)a_i + b_i) + a_iQ'_i(x_i) = b_i$.

          Le polynôme ainsi formé est bien un polynôme d'interpolation
          d'Hermite car de degré $2p-1$.
        \item 
      \end{enumerate}
\end{enumerate}



\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\parindent0pt

\usepackage{babel}

\title{104 -- Groupes finis. Exemples et applications.}
\author{Vincent-Xavier \bsc{Jumel}}
\date{18/09/2016}

\begin{document}
\maketitle

\begin{theoreme}
  L'ensemble $U_n$ des racines $n$-ièmes du nombre complexe 1 est un
  sous-groupe, cyclique et d'ordre $n$, du groupe multiplicatif $U$ des
  nombres complexes de module 1.
  \begin{proof}
    Une racine de $z = re^{i\theta}$ s'écrivant $Z_k = \sqrt[n]{r}
    e^{i\left(\frac{\theta}n +k\frac{2\pi}n\right)}, k\in\mathbf{Z}$, on
    peut écrire les racines de l'unité sous la forme \[ \omega_k =
    e^{ik\frac{2\pi}n},\ \  0\leqslant k \leqslant n-1. \]
    En posant $\omega = e^{\frac{2i\pi}n}$, on en déduit que $\omega_k =
    \omega^k$ et $\omega_0 = 1$.

    On constate maintenant que $U_n = \{1,\omega,\dots,\omega^{n-1}\}$
    est le sous-groupe monogène du groupe multiplicatif $U$ engendré par
    $\omega$.
  \end{proof}
\end{theoreme}

\textsc{Cas particuliers}
\begin{itemize}
  \item $n = 2$, les racines de l'unité sont 1 et $-1$
  \item $n = 3$, les racines de l'unité sont 1, $-\frac12 + i
    \frac{\sqrt{3}}2 = j$ et $-\frac12 - \frac{\sqrt{3}}2 = j^2 =
    \overline{j}$ ; on a $1 + j + j^2 = 0$
  \item $n = 4$, les racines de l'unité sont 1, $i$, -1 et $-i$
\end{itemize}

\begin{theoreme}
  Les sous-groupes de $(\mathbf{Z},+)$ sont les $(n\mathbf{Z},+)$, $n\in
  \mathbf{N}$.
\end{theoreme}
\begin{proof} Il est aisé de vérifier que pour tout $n \in \mathbf{N},\
  n\mathbf{Z}$ est un sous-groupe de $\mathbf{Z}$.

  Réciproquement, soit $H$ un sous-groupe de $\mathbf{Z}$.

  Si $H = \{0\}$, on peut écrire $H = 0\mathbf{Z}$. Sinon, il existe un
  élément non nul dans $H$ et son opposé pour constater que $H$ contient
  au moins un entier strictement positif. $H \cap \mathbf{N}^*$ est
  ainsi une partie non-vide de $\mathbf{N}$, elle contient un plus petit
  élément, que nous notons $n$. D'après $\{n\} \subset H$, on a le
  sous-groupe $n\mathbf{Z}$ engendré par $n$ qui est inclus dans $H$.

  Pour tout $x\in H$, une division euclidienne donne \[ x = nq +r \ 0
  \leqslant r < n. \] De $x \in H$ et $nq \in H$, il vient que $r \in
  H$et donc $r = 0$ et donc $x \in n\mathbf{Z}$. Finalement $H \subset
  n\mathbf{Z}$.
\end{proof}

\begin{theoreme}
  Pour tout $n \in \mathbf{N}$, le groupe-quotient
  $\mathbf{Z}/n\mathbf{Z}$ est appelé groupe des entiers rationels
  modulo $n$. Il est monogène. Si $n = 0$, il est isomorphe à
  $\mathbf{Z}$ ; si $n > 0$, il est cyclique d'ordre $n$.
\end{theoreme}

\begin{proof}
  Soit $\varphi_n : \mathbf{Z} \to \mathbf{Z}/n\mathbf{Z}$ la surjection
  canonique. On vérifie : \[ \forall x \in \mathbf{Z},\ \varphi_n(x) = x
  \varphi_n(1). \] $\varphi_n(1)$ est donc générateur de
  $\mathbf{Z}/n\mathbf{Z}$ qui est monogène.
  \begin{itemize}
    \item Si $n=0$, la relation d'équivalence selon le groupe
      $0\mathbf{Z}$ est l'égalité ; $\mathbf{Z}/\{0\} \cong \mathbf{Z}$.
    \item Soit $n \geqslant 1$. Tout $X \in  \mathbf{Z}/n\mathbf{Z}$ est
      l'image par $\varphi_n$ d'un et un seul élément de l'ensemble
      $\{0,1,\dots,n-1\}$, à savoir les restes de la division
      euclidienne par $n$ des entiers qui constituent la classe de $X$.
      La restriction de $\varphi_n$ à $\{0,1,\dots,n-1\}$ est ainsi une
      bijection ; $\mathbf{Z}/n\mathbf{Z}$ a pour cardinal $n$ et peut
      s'écrire $\{\overline{0},\overline{1},\dots,\overline{n-1}\}$, en
      notant $\overline{x}$ pour $\varphi_n(x)$.
  \end{itemize}
\end{proof}

\begin{theoreme}
  Les générateurs de $\mathbf{Z}/n\mathbf{Z}$, ($n \geqslant 1$), sont
  les $\overline{x}$ tels que $0 \leqslant x \leqslant n - 1$ et que $x$
  est premier avec $n$.
\end{theoreme}
\begin{proof}
  Soit $\overline{x}$, avec $0 \leqslant x \leqslant n - 1$, un élément
  de $\mathbf{Z}/n\mathbf{Z}$. Il est générateur si et seulement s'il
  existe $p \in\mathbf{Z}$ tel que $p\overline{x} = \overline{1}$, ce
  qui signifie que $px \equiv 1 [n]$, autrement dit qu'il existe $p$ et
  $q$ tels que $px = qn + 1 \iff px - qn =1$ qui est l'égalité de
  Bézout.
\end{proof}

\begin{theoreme} Soit $G$ un groupe monogène. S'il est fini, il est
  isomorphe à $\mathbf{Z}$ ; s'il est fini, d'ordre $n$, il est
  isomorphe à $\mathbf{Z}/n\mathbf{Z}$. Dans les deux cas, il est
  abélien.
\end{theoreme}
\begin{proof}
  Soit $a$ un générateur de $G$, dont tous les éléments sont de la forme
  $a^p$, ($p\in \mathbf{Z}$). De $a^{p+p'} = a^pa^{p'}$ on en déduit que
  l'application $f$ de $\mathbf{Z}$ dans $G$ définie par $p \mapsto a^p$
  est un morphisme surjectif de groupes ; $G$ est donc isomorphe à
  $\mathbf{Z}/\ker f$, où $\ker f$, qui est un sous-groupe de
  $\mathbf{Z}$ est la forme $k\mathbf{Z}$.

  Si $G$ est infini, il en est de même de $\mathbf{Z}/\ker f$ et donc
  $\ker f = \{0\}$. $f$ est donc injectif et les éléments $a^p$, ($p\in
  \mathbf{Z}$) sont deux à deux distincts ; à la fois surjectif et
  injectif, $f$ est un isomorphisme de $G$ sur $\mathbf{Z}$.

  Si $G$ est fini, il en est de même de $\mathbf{Z}/\ker f$ et donc $k =
  n$. $G \cong \mathbf{Z}/n\mathbf{Z}$ ; on a $G = \{a^0, a^1, \dots,
  a^{n-1}\}$ et $n$ est le plus petit entier $m$ strictement positif tel
  que $a^m = e$.

  Enfin, $G$ isomorphe à un groupe abélien est lui-même abélien.
\end{proof}
\begin{definition}
  Soit $G$ un groupe, d'élément neutre $e$, et $a$ un élément de $G$; le
  sous-groupe engendré par $a$, $gr(a)$ est un groupe monogène.
  \begin{itemize}
    \item Si $gr(a)$ est infini, on dit que $a$ est un élément
      d'\emph{ordre infini} de $G$.
    \item Si $gr(a)$ est fini, son ordre est appelé \emph{ordre de $a$
      dans $G$} et noté $\omega(a)$.
  \end{itemize}
\end{definition}

\begin{proposition}
  Dans un groupe fini, tout élément est d'ordre fini et son ordre divise
  l'ordre du groupe.
\end{proposition}
\begin{proof}
  Le résultat provient de la définition et du théorème 12.
\end{proof}

\begin{corollaire}
  Soit $G$ un groupe fini d'ordre $n$, d'élément neutre $e$. Tout $a$ de
  $G$ vérifie $a^n = e$.
\end{corollaire}
\begin{proof}
  Soit $\omega$ l'ordre de $a$ dans $G$. ; on a $a^{\omega} = e$ ;
  d'après la proposition précédente, $n = q\omega$ donc $a^n = e^q = e$.
\end{proof}

\begin{corollaire}
  Tout groupe $G$ d'ordre $p$ premier est cyclique (donc abélien) ; il
  est engendré par l'un quelconque de ses éléments autres que l'élément
  neutre $e$.
\end{corollaire}
\begin{proof}
  Soit $a\in G \setminus\{e\}$. On a $\omega(a) > 1$ ; comme $\omega(a)$
  divise $p$ premier, on a $\omega(a) = p$ et $gr(a) = G$.
\end{proof}
\begin{corollaire}
  Le produit de deux groupes finis $G$ et $G'$ est cyclique si et
  seulement si ces deux groupes sont cycliques, et d'ordres $n$ et $m$
  premiers entre eux. En particulier $\mathbf{Z}/n\mathbf{Z} \times
  \mathbf{Z}/m\mathbf{Z}$ est cyclique et donc isomorphe à
  $\mathbf{Z}/nm\mathbf{Z}$ si et seulement si $n$ et $m$ sont premiers
  entre eux.
\end{corollaire}
\begin{proof}
  Soient $x\in G$ et $x'\in G'$, d'ordre $\omega$ et $\omega'$
  (diviseurs de $n$ et $m$) ; $(x,x')^q = (e,e')$ s'écrivant : $(x^q =
  e) \wedge (x'^q = e')$, l'ordre de $(x,x') \in G \times G'$ est le
  ppcm de $\omega$ et $\omega'$ ; il s'agit de l'ordre $nm$ de $G \times
  G'$ si et seulement si : \[ \omega = n \wedge \omega' = m \wedge
  \mathop{\mathrm{ppcm}}(n,m) = nm.\]
\end{proof}

\begin{exemple}
  \textsc{Les groupes d'ordre 4} : l'un d'eux est $V_4 =
  \mathbf{Z}/2\mathbf{Z} \times \mathbf{Z}/2\mathbf{Z}$, non cyclique,
  non abélien qui est dit \emph{groupe de Klein}.

  Inversement, soit $G = \{e, a_1, a_2, a_3\}$ un groupe d'ordre 4 non
  cyclique. Les $a_i$ sont d'ordre 2 et on $a_i^2 = e$ ; pour $i \neq
  j$, $a_ia_j \notin \{ a_k^2, a_ie, ea_j \}$ donc $a_ia_j = a_k$. Les
  groupes non cycliques d'ordre 4 sont donc deux à deux isomorphes et
  donc isomorphes à $V_4$.
\end{exemple}



\end{document}

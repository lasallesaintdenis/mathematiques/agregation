\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}}
\newcommand{\Aut}{\mathop{Aut}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Construction de l'exponentielle et de $\pi$}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  On définit la fonction $\exp$ par $z \mapsto \sum_{n\in\N}
  \frac{z^n}{n!}$ :
  \begin{enumerate}[label=(\roman*)]
    \item $\exp$ est bien définie sur $\C$, elle est
      $\mathcal{C}^{\infty}$ et $\exp' = \exp$ ;
    \item $\forall (a,b) \in \C^2, e^{a+b} = e^ae^b$ ;
    \item $\forall z \in \C, \exp(z) \neq 0$ ;
    \item $\exp$ restreinte aux nombres réels ($\R$) est une fonction
      poisitive strictement croissante réalisant un
      $\mathcal{C}^{\infty}$-difféomorphisme entre $\R$ et $\R_+^*$ ;
    \item $\exists ! \pi > 0$ tel que $\frac{\pi}{2} = \inf\{ t > 0 \mid
      e^{it} = i \}$ ;
    \item $e^z = e^{z'} \iff z = z' + 2ik\pi, k \in \Z$ ;
    \item $\exp \colon \C \to \C^*$ est surjective.
  \end{enumerate}
\end{theoreme}

\section*{Preuve}

\subsection*{1 et 2}

Soit $z\in \C$. Posons $u_n(z) =  \frac{z^n}{n!}$. Alors $\left\lvert
\frac{u_{n+1}(z)}{u_n(z)} \right\rvert = \frac{\lvert z \rvert}{n+1}
\to 0$. Donc la série $\sum u_n(z)$ est absolument convergente (critère
de D'Alembert) et donc $\exp$ est définie sur $\C$.

Soit $A$ un réel positif. Soit $z$ un élément dans le disque de centre
$O$ et de rayon $A$. $\lvert u_n(z) \rvert \leqslant \frac{A^n}{n!}$ et
la série $\sum \frac{A^n}{n!}$ est convergente.

La série $\sum u_n(z)$ converge normalement sur tout disque de $\C$ et les
$u_n$ sont continues donc elle est continue.

De plus $\sum_{n\in\N} \frac{(a+b)^n}{n!} = \sum_{n\in\N} \sum_{k=0}^n
\frac{a^k b^{n-k} \binom{n}{k}}{n!} = \sum_{n\in\N} \sum_{k=0}^n
\frac{a^k}{k!}\frac{b^{n-k}}{(n-k)!} = \sum_{k=0}^{+\infty}
\frac{a^k}{k!} \sum_{n = k}^{+\infty} \frac{b^{n-k}}{(n-k)!}$

Cette dernière égalité est une conséquence du théorème de Fubini
appliqué à la mesure de comptage, qu'il est possible d'utiliser ici car
les deux sommes sont absolument convergentes.

On reconnaît ici un produit de Cauchy pour les deux séries.

On a donc $\sum_{n\in\N} \frac{(a+b)^n}{n!} = \sum_{k=0}^{+\infty}
\frac{a^k}{k!} \sum_{n = 0}^{+\infty} \frac{b^n}{n!} = e^ae^b$.

Démontrons que la dérivée d'exponentielle est elle même.

Soient $z$ et $k$ deux nombres complexes, $\frac{e^{z+k} - e^z}k = e^z
\frac{e^k - 1}k$

%$\exp$ est continue en 0 et $e^0 = 1$

Il faut donc montrer que $\lim_{k\to 0}\frac{e^k - 1}k = 1$, et on aura
$\lim_{k\to 0}\frac{e^{z+k} - e^z}k = e^z$, ce qui s'écrit encore $\exp' =
\exp$. Or $\frac1k \left(e^k - 1\right) = \frac1k \sum_{n=1}^{+\infty}
\frac{k^n}{n!} = \sum_{n=1}^{+\infty} \frac{k^{n-1}}{n!} =
\sum_{n=0}^{+\infty} \frac{k^n}{(n+1)!}$. On peut ensuite conclure en
prenant, la limite, avec la convention $0^0 \to 1$.

$\exp$ est donc holomorphe sur tout $\C$.

\subsection*{3}

Pour tout $z$ complexe, $e^ze^{-z} = e^0 = 1$ donc $\forall z \in \C,
e^z \neq 0$.

\subsection*{4}

D'après la définition, si $x \geqslant 0$, $\exp(x) \geqslant 1 > 0$.
D'autre part, $\exp$ est clairement croissante sur $\R_+$, avec
$\lim_{x\to+\infty}e^x = +\infty$.

De plus, $e^{-x} = \frac1{e^x}$ entraîne que $\lim_{x\to-\infty}e^x =
0$.

Soient $x$ et $y$ deux réels tels que $x< y <0$. On a successivement
$-x > -y > 0$ donc $e^{-x} > e^{-y} > 1$ et donc $\frac1{e^{-x}} <
\frac1{e^{-y}} < 1$ ou encore $e^x < e^y < 1$.

Comme $\exp' = \exp$ et que $\forall x \in \R, e^x \neq 0$, le théorème
des bijections réciproques nous permet de conclure que $\exp$ est un
$\mathcal{C}^{\infty}$-difféomorphisme de $\R$ dans $\R_+^*$.

\subsection*{5}

$\forall t \in \R, \left\lvert e^{it} \right\rvert^2 = e^{it}
\overline{e^{it}} = e^{it} e^{-it} = 1$ Posons, $\cos t = \Re(e^{it})$
et $\sin t = \Im(e^{it})$.

$\frac{\mathrm{d}}{\mathrm{d}t}e^{it} = \cos' t + i\sin' t = -\sin t + i
\cos t = ie^{it}$

De plus, $\cos t = \sum_{n\in\N}(-1)^n\frac{t^{2n}}{(2n)!}$, donc
$\cos(2) \leqslant -\frac13$ et $\cos(0) = 1$

Donc il existe au plus un $t_0 > 0 $ tel que $\cos(t_0) = 0$ d'après le théorème
des valeurs intermédiaires.

Or $\cos^2 t_0 + \sin^2 t_0 =1$ donc $\sin t_0 = \pm 1$.

De plus $\sin' t = \cos t > 0$ sur $[0,t_0[$ et $\sin(0) = 0$, donc
$\sin t_0 > 0$ et $\sin t_0 = 1$.

En posant $\pi = 2t_0$, on a l'égalité $e^{it_0} = i =
e^{i\frac{\pi}2}$.

\subsection*{6}
Soit $k \in \Z$.

$e^{2i\pi k} = \left(e^{i\frac{\pi}2} \right)^{4k} = i^{4k} = 1$

Donc $\forall z \in \C, \forall k\in \Z, e^{z + 2i\pi k} = e^z$.

Réciproquement, si $e^z = e^{z'}$, alors $e^{z - z'} = 1 = e^{i0}$.

On obtient donc $\Re(z) = \Re(z')$ et $e^{i\Im(z)} = e^{i\Im(z')}$

Montrons que $\forall y \in ]0;2\pi[, e^{iy} \neq 1$.

Soit $y \in ]0;2\pi[, e^{iy/4} = u + iv$. $\frac{y}4 < \frac{\pi}{2}
\implies u > 0$ et $v > 0$ et $e^{iy} = (u^2 - v^2 + 2iuv)^2 = u^4 + v^4
-6u^2v^2 + 4i(u^2 - v^2)uv$. Ainsi $e^{iy} \in \R \iff u^2 = v^2$ On
trouve alors $e^{iy} = -1 \neq 1$.

$\{ z \in \C \mid e^z = 1 \} = 2i\pi\Z$

\subsection*{7}

$\varphi \colon \left\{ \begin{array}{l}\C \to \U\\ t \mapsto
e^{it}\end{array}\right.$ est une surjection.

Soit $w \in \U$. Supposons que $w = u + iv$, avec $0\leqslant u
\leqslant 1$ et $v\geqslant 0$. La définition de $\pi$ montre qu'il
existe $t \leqslant \frac{\pi}2$ tel que $\cos t = u$ et $v = \sqrt{1 -
u^2} = \sin t$. Donc $w = e^{it}$

Si $u < 0$ et $v\geqslant 0$, alors $-iw$ satisfait les conditions
précédentes et donc $\exists t \in \R, w = ie^{it} =
e^{i(t+\frac{\pi}2)}$

Si $u < 0$ et $v<0$, alors $\exists t \in \R, w = -e^{it} =
e^{i(t+\pi)}$.

Si $w \neq 0$, alors on pose $\alpha = \frac{w}{\lvert w \rvert}$ et il
existe $x \in \R$ tel que $\lvert w \rvert = e^x$, et comme $\alpha$ est
ramené au cas précédent, il existe $t \in \R, \alpha = e^{it}$.

Donc $w = e^{x + it}$.



\end{document}

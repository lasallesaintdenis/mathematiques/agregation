\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\newcommand{\Rel}{\mathcal{R}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\GrpPerm}{\mathfrak{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Équation aux classes}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

On désigne ici par $G$ un groupe dont l'élément neutre est noté $e$ et par
$X$ un ensemble quelconque.

\begin{definition}
  On dit que $G$ \emph{opère} sur $X$ s'il existe une application \[ G
  \times X \to X \quad (s,x) \mapsto s \cdot x \] telle que
  \begin{enumerate}
    \item $\forall (s,t) \in G^2, \forall x \in X, s \cdot ( t \cdot x ) =
      (st) \cdot x$ ;
    \item $\forall x \in X, e \cdot x = x$.
  \end{enumerate}
\end{definition}

\begin{exemple}
  \begin{itemize}[label=--]
    \item Le groupe $G$ opère  sur lui-même par l'application \[ G \times G
      \to G \quad (s,x) \mapsto sx . \]
    \item Le groupe des perumtations $\GrpPerm$ d'un ensemble $X$ opère
      sur $X$ par l'application \[ \GrpPerm \times X  \to X \quad (s,x)
      \mapsto s(x) . \]
  \end{itemize}
\end{exemple}

On considère désormais que $G$ agit sur un ensemble $X$.

\begin{definition}
  La relation sur $X$ définie par \[  x \T y \iff \exists s \in G, y = s
  \cdot x \] est une relation d'équivalence, appelée relation
  \emph{d'intransitivité}. La classe d'équivalence d'un élément $x$ de $X$
  est $G_x = \{ s \cdot x , s \in G \}$, on l'appelle \emph{classe
  d'intransitivité} (ou \emph{orbite} ou \emph{trajectoire}) de $x$.
\end{definition}

\begin{definition}
  Le \emph{stabilisateur} d'un élément $x$ de $X$ est le sous-ensemble de
  $G$ défini par $S_x = \{ s \in G \mid s \cdot x = x \}$.
\end{definition}

\begin{proposition}
  Pour tout élément $x$ de $X, S_x$ est un sous-groupe de $G$.
\end{proposition}
\begin{proof}
  L'ensemble $S_x$ n'est pas vide car $e \in S_x$. Par ailleurs, pour tout
  $s,t \in S_x$, on a $t \cdot x = x$ donc $x = t^{-1} \cdot (t \cdot x)  =
  t^{-1} \cdot x$. Ainsi, $(st^{-1}) \cdot x = s \cdot (t^{-1} \cdot x ) = s
  \cdot x = x$, d'où $st^{-1} \in S_x$.
\end{proof}

\begin{theoreme}
  Si $G$ est fini, pour tout $x \in X$ on a $\card G = \card G_x \cdot \card
  S_x$.
\end{theoreme}
\begin{proof}
  On fixe $x$. Soit $\Rel_x$ la relation d'équivalence sur $G$ définie
  par : $s \Rel_x t \iff s \cdot x = t \cdot x$. On a $s \Rel_x t \iff
  (t^{-1} s) \cdot x = x \iff t^{-1} s \in S_x \iff s \in tS_x$. Les classes
  d'équivalences sont donc de la forme $tS_x, t \in G$, ce qui montre
  qu'elles sont toutes de cardinal $\card S_x$. Il y a autant de classes
  d'équivalences que de valeurs prises par $s \cdot x, x \in G$,
  c'est-à-dire qu'il y a $\card G_x$ classes d'équivalence. Donc $\card G =
  \card G_x \cdot \card S_x$.
\end{proof}

\begin{corollaire}[Équation aux classes]
  Si $X$ et $G$ sont finis, en désignant par $\Theta$ une partie de $X$
  contenant exactement un représentant de chacune des classes
  d'intransitivité, on a \[ \card X = \sum_{x \in \Theta} \card G_x =
  \sum_{x \in \Theta} \frac{\card G}{\card S_x} . \]
\end{corollaire}

\begin{remarque}
  $\Theta = X_{ / \T}$ où $\T$ est la relation d'intransitivité.
\end{remarque}

On peut appliquer ce résultat aux automorphismes intérieurs de façon à
donner le théorème suivant :
\begin{theoreme}
  Soit $G$ un groupe fini. Il existe une famille finie $(H_i)_{i\in I}$ de
  sous-groupes stricts de $G$ telle que \[ \card G = \card Z(G) + \sum_{i
  \in I} \frac{\card G}{\card H_i} \] où $Z(G)$ désigne le centre du groupe
  $G$.
\end{theoreme}

\begin{proof}
  Faison opérer $G$ sur lui-même par les automorphismes intérieurs : $G
  \times G \to G, \quad (s,x) \mapsto \varphi_s(x) = sxs^{-1}$. Si $x \in
  G$, on a $G_x = \{ sxs^{-1} , s \in G \}$ et $S_x = \{ s \in G \mid sx =
  xs\} $ (dans ce cas, $S_x$ est aussi appelé \emph{centralisateur} ou
  \emph{normalisateur} de $x$). D'après le corollaire précédent, il existe
  $\Theta \subset G$ tel que \[ \card G = \sum_{x \in \Theta} \frac{\card
    G}{\card S_x} . \] Or $S_x = G \iff \forall s \in G, sx = xs \iff x \in
    Z(G)$. En notant $\Theta' = \Theta \setminus Z(G)$, on a donc \[ \card G
      = \sum_{x \in Z(G)} \frac{\card G}{\card S_x} + \sum_{x \in \Theta'}
      \frac{\card G}{\card S_x} = \card Z(G) + \sum_{x \in \Theta'}
    \frac{\card G}{\card S_x} , \] d'où le théorème car les $(S_x)_{x\in
    Theta'}$ constituent une famille finie de sous-groupes stricts de $G$.
\end{proof}

\end{document}

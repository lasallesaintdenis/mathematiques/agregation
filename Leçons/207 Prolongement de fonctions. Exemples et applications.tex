\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}


\title{207 Prolongement de fonctions. Exemples et applications}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\begin{definition}
  Soit $f$ une application d'une partie de $D$ de $\R$ dans $R$. $f$ est
  dite \emph{croissante} (strictement) si et seulement si : \[ \forall (x,y)
  \in D^2,\ x < y \implies f(x) \leq f(y) \ (<) .\] $f$ est dite
  \emph{décroissante} si et seulement si $-f$ est croissante.

  $f$ est dite \emph{monotone} si et seulement si $f$ est croissante ou
  décroissante.
\end{definition}

\begin{proposition}
  Une application monotone injective est bijective si et seulement si
  elle est strictement monotone.
\end{proposition}

\begin{proposition}
  \begin{itemize}
    \item La composée de deux applications croissantes est croissante.
    \item La composée d'une application croissante et d'une application
      décroissante est décroissante.
  \end{itemize}
\end{proposition}

\begin{theoreme}
  Soient $D$ une partie de $\R$ et $f \colon D \to \R$ une application
  monotone, $a$ un point de $\overline{R}$ tel que $a$ soit adhérent à
  $D \cap ]a ; +\infty[$ (resp. $D \cap ]-\infty ; a[$). Alors $f$ admet
  une limite, finie ou infinie, à droite (resp. à gauche) au point $a$.
\end{theoreme}

\begin{corollaire}
  Soient $f \colon D \to \R$ croissante et $a$ un point de
  $\overline{R}$ tel que $a$ soit adhérent à $D \cap ]a ; +\infty[$.
  Alors $f$ admet une limite finie à droite au point $a$ si et seulement
  si $f$ est minorée sur $D \cap ]a ; +\infty[$.
\end{corollaire}

\begin{corollaire}
  Soient $I$ un intervalle de $\R$ et $f \colon I \to \R$ une
  application monotone et $a$ un point de $I$. Si $a \neq \sup I$, $f$
  admet une limite finie à droite $f(a^+)$ et si $a \neq \inf I$, $f$
  admet une limite finie à gauche $f(a^-)$.
\end{corollaire}

\begin{theoreme}
  Soit $f$ une application monotone d'un intervalle réel $I$ dans $\R$.
  L'ensemble des points de discontinuités de $f$ est au plus
  dénombrable.
\end{theoreme}

\begin{theoreme}
  Soient $I$ un intervalle de $\R$ et $f \colon I \to \R$ une
  application monotone. $f$ est continue sur $I$ si et seulement si
  $f(I)$ est un intervalle.
\end{theoreme}

\begin{corollaire}
  Soient $I$ un intervalle de $\R$ et $f \colon I \to \R$ une
  application continue et strictement monotone.

  Alors $J = f(I)$ est un intervalle et $f$ induit un homéomorphisme de
  $I$ sur $J$.
\end{corollaire}

\begin{theoreme}
  Soient $I$ et $J$ des intervalles  de $\R$, et $f$ un homéomorphisme
  de $I$ sur $J$. Alors $f$ est strictement monotone.
\end{theoreme}

\begin{theoreme}
  Soient $I$ un intervalle de $\R$ et $f \colon I \to \R$ une
  application continue sur $I$ et dérivable à droite sur
  $\stackrel{\circ}{I}$.
  \begin{enumerate}[label=\textit{\roman*)}]
    \item $f$ est constante si et seulement si $\forall t \in
      \stackrel{\circ}{I} f'_d(t) = 0$
    \item $f$ est croissante si et seulement si $\forall t \in
      \stackrel{\circ}{I} f'_d(t) > 0$
    \item $f$ est décroissante si et seulement si $\forall t \in
      \stackrel{\circ}{I} f'_d(t) < 0$
  \end{enumerate}
\end{theoreme}

\begin{theoreme}
  Soient $I$ un intervalle de $\R$ et $f \colon I \to \R$ une
  application continue sur $I$ et dérivable à droite sur
  $\stackrel{\circ}{I}$. Pour que $f$ soit strictement croissante sur
  $I$, il faut et il suffit que $f'_d \geq 0$ et que l'ensemble $X = \{
  t \in \stackrel{\circ}{I} \mid f'_d(t) = 0 \}$ soit d'intérieur vide.
\end{theoreme}

\begin{definition}
  Soient $I$ et $J$ deux intervalles de $\R$, $f \colon I \to J$ une
  bijection, et $n$ un entier naturel non nul. On dit que $f$ est un
  $\mathcal{C}^n$ difféomorphisme si et seulement si les applications
  $f$ et $f^{-1}$ sont des classe $\mathcal{C}^n$
\end{definition}

\begin{theoreme}
  Soient $I$ et $J$ deux intervalles de $\R$, $f \colon I \to J$ un
  homéomorphisme, et $t_0$ un point de $I$ en lequel $f$ est dérivable.
  Pour que $f^{-1}$ soit dérivable en $f(t_0)$, il faut et il suffit que
  $f'(t_0) \neq 0$ ; on a alors : \[ (f(^{-1})'[f(t_0)] =
  \frac{1}{f'(t_0)} \]
\end{theoreme}

\begin{theoreme}
  Soient $I$ et $J$ deux intervalles de $\R$, $f \colon I \to J$ un
  homéomorphisme, $n$ un entier naturel non nul, et $t_0$ un point de
  $I$ en lequel $f$ est dérivable.  Pour que $f$ soit un $\mathcal{C}^n$
  difféomorphisme, il faut et il suffit que $f$ soit de classe
  $\mathcal{C}^n$ et que $f'$ ne prenne la valeur 0 en aucun point de
  $I$.
\end{theoreme}



\end{document}

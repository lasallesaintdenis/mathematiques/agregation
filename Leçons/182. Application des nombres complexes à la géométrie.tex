\documentclass[a4paper,12pt,,twocolumn,landscape,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Pc}{\mathbf{P}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\ssi}{si, et seulement si }
\renewcommand{\implies}{\DOTSB \;\Rightarrow \;}
\renewcommand{\iff}{\DOTSB \;\Leftrightarrow \;}
\everymath{\displaystyle\everymath{}}
\newcommand{\Ccal}{\mathcal{C}}
\parindent0pt
\columnsep25pt

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{8 janvier 2014}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother


\begin{document}
\maketitle

\section{Géométrie euclidienne}
\subsection{Liens}
Le plan $\R^2$ est identifié à $\C$ par $(x,y)\mapsto x+iy$.
\begin{definition}[affixe]

  L'affixe de $M(x,y)$ est $Z_M=x+iy$.
  Le vecteur image de $z\in\C$ a pour coordonnées $(\Re z, \Im z)$.
\end{definition}

\begin{proposition}
  $\vec{u}\cdot\vec{v} = \Re[\overline{z_{\vec{u}}}\times z_{\vec{v}}]$
  \hspace{2cm}
  $det(\vec{u},\vec{v}) = \Im[\overline{z_{\vec{u}}}\times z_{\vec{v}}]$
\end{proposition}

Conséquence :
\begin{itemize}
  \item $\overrightarrow{AB}\cdot\overrightarrow{CD} =
    \Re[(\overline{z_B-z_A})(z_D-z_C)]$
  \item $\mathop{Vol}(ABDC)=\Im[(\overline{z_B-z_A})(z_D-z_C)]$
\end{itemize}

Application : 
\begin{itemize}
  \item $\overrightarrow{AB}$ et $\overrightarrow{CD}$ sont colinéaires
    si et seulement si $(\overline{z_B-z_A})(z_D-z_C)\in\R$
  \item La droite passant par $A$ et dirigée par $\vec{u}$ : $\lbrace
    z\in\C, (z-z_A)z_{\vec{u}}\in\R\rbrace$
  \item $\overrightarrow{AB}\perp\overrightarrow{CD}\equiv
    (\overline{z_B-z_A})(z_D-z_C)\in i\R$
  \item $||\overrightarrow{AB}||^2=|z_B-z-A|^2$
  \item Cercle de centre $O$, de rayon $R$ : $\lbrace z\in\C,
    |z-z_O|=R\rbrace$
\end{itemize}
\subsection{Angles}
\begin{proposition}
  $\vec{u},\vec{v}$ unitaires, alors il existe une unique rotation qui
  envoie $\vec{u}$ sur $\vec{v}$.
\end{proposition}

On peut définir la relation $\mathcal{R}$ par
$(\vec{u},\vec{v})\mathcal{R}(\vec{u'},\vec{v'})$ si c'est la même
rotation.

$\mathcal{A} = \lbrace\text{couples de vecteurs unitaires}\rbrace$
\[\Phi : \mathcal{A}\to O^+(\R^2)\]
\begin{definition}[angle orienté]
  L'angle orienté de $(\vec{u},\vec{v})$ est sa classe dans
  $\mathcal{A}/\mathcal{R}$.
\end{definition}
$\mathcal{A}/\mathcal{R}$ hérite de la structure de groupe de
$O^+(\R^2)$.

\begin{proposition}[relation de Chasles]
  $(\vec{u},\vec{v}) = (\vec{u},\vec{w}) + (\vec{w},\vec{v})$
\end{proposition}
On a les morphismes suivants :
\[\begin{array}{l}\R\to\U\to O^+(\R^2)\to\mathcal{A}/\mathcal{R}\\
  \theta\mapsto e^{i\theta}\mapsto(z\mapsto
e^{i\theta}z)\mapsto((0,1),(\cos\theta, \sin\theta))\end{array}\]
\begin{definition}[$\pi$]
  $\varphi:\R\to\U,\theta\mapsto e^{i\theta}$ est un morphisme
  surjectif. $\pi$ est défini par $\ker\varphi = 2\pi\Z$.
\end{definition}
Si $(\vec{u},\vec{v})\in\mathcal{A}/\mathcal{R}$ est associée par le
morphisme à $\theta\in\R$, on dit que $\theta$ est une mesure de
$(\vec{u},\vec{v})$.

Application : coordonnées polaires :
\begin{itemize}
  \item $\forall z\in\C\setminus\{0\},
    \exists!(r,\theta)\in\R_+^*\times[0,2\pi[$ tel que $z=re^{i\theta}$
  \item $ABC$ équilatéral $\equiv (z_A-z_B)^2+(z_B-z_C)^2+(z_C-z_A)^2=0$
  \item Les racines n-ièmes de l'unité sont les sommets d'un $n$-gone
    régulier
  \item théorème de l'angle au centre
\end{itemize}
\subsection{Transformations du plan}
\begin{definition}[isométries directes]
  $\lbrace(z\mapsto az+b), a\in\U, b\in\C\rbrace=\mathop{Isom}^+$
\end{definition}
C'est le groupe engendré par les rotations et translations.
\begin{definition}
  $\mathop{Isom}^+$ conserve les distances et les angles orientés.
\end{definition}
\begin{definition}
  $\mathop{Isom}=\langle\mathop{Isom}^+,(z\mapsto\bar{z})\rangle$
\end{definition}
On complète avec les symétries par rapport aux droites.
\begin{proposition}
  $\mathop{Isom}$ conserve les distances.
\end{proposition}
\begin{definition}[similitudes]
  $\langle\mathop{Isom},(z\mapsto Rz),R\in\R_+^*\rangle$
\end{definition}
\begin{proposition}
  Les similitudes conservent les rapports de distances.
\end{proposition}
Applications :
\begin{itemize}
  \item la développée d'une cycloïde est une cycloïde
  \item théorème de Napoléon
\end{itemize}
\subsection{Polynômes et barycentre}
Le barycentre de $A_1(\alpha_1),\dots A_n(\alpha_n)$ est l'unique $G$
tel que $z_G=\sum_{i=1}^n\alpha_iz_{A_i}$
\begin{theoreme}[Gauss-Lucas]
  Soit $P\in\C[X]$, les racines de $P'$ sont dans l'enveloppe convexe
  des racines de $P$.
\end{theoreme}
\begin{theoreme}[Ellipse de Steiner]
  Soient $A,B,C\in\mathcal{P}$ distincts et
  $P(X)=(X-z_A)(X-z_B)(X-z_C)$.

  L'ellipse dont les foyers ont pour affixe les racines de $P'$ et
  tangente à l'un des côtés est tangente aux trois côtés en leurs
  milieux.
\end{theoreme}
\begin{proposition}
  C'est l'unique ellipse de volume maximal parmi celles inscrites dans
  $ABC$.
\end{proposition}
\section{Droite projective complexe}
\begin{definition}
  $\Pc^1(\C) = \C^2\setminus\{(0,0)\}/\text{relation de colinéarité}$
  munit de la topologie quotient.
\end{definition}
Projection stéréographique : $\C\cup\{\infty\}\cong S$

\subsection{Homographie}
\begin{definition}
  $f:\Pc^1(\C)\to\Pc^1(\C)$ est une homographie si $ad-bc\neq 0$ et
  \[ f(z)=\left\lbrace\begin{array}{rrr}
      \frac{az+b}{cz+d} & \text{si} & z\in\C\setminus\{-\frac{d}{c}\} \\
      \infty & \text{si} & z=-\frac{d}{c} \\
  \frac{a}{c} & \text{si} & z=\infty \end{array}\right.\]
\end{definition}
On note $PGL_2(\C)$ l'ensemble des homographies.

Remarque : on peut définir ine homographie comme une application de
$GL_2(\C)$ que l'on applique à $C^2/\text{colinéarité}$.
\begin{proposition}
  On a un isomorphisme : $PGL_2(\C)\cong\frac{GL_2(\C)}{\lbrace \lambda
  I_2,\lambda\in\C^*\rbrace}$.
\end{proposition}
\begin{proposition}
  $PGL_2(\C)$ est un sous-groupe des homéomorphismes de $\Pc^1(\C)$.
\end{proposition}
\begin{proposition}
  \begin{itemize}
    \item $GL_2(\C)=\left\langle \left(\begin{array}{cc}1 & b \\ 0 &
      1\end{array}\right),\left(\begin{array}{cc}a & 0 \\ 0 &
      1\end{array}\right),\left(\begin{array}{cc}0 & 1 \\ 1 &
      0\end{array}\right),b\in\C,a\in\C^* \right\rangle$
    \item $PGL_2(\C)=\left\langle (z\mapsto z+b),(z\mapsto
      az),(z\mapsto\frac1z),b\in\C,a\in\C^* \right\rangle$
  \end{itemize}
\end{proposition}
Application :
\begin{itemize}
  \item point fixe et suite homographique
  \item action de $PGL_2(\C)$ sur $\Pc^1(\C)$
\end{itemize}
\subsection{Birapport}
\begin{theoreme}
  $PGL_2(\C)$ agit sur $\Pc^1(\C)$ de manière 3-transitive et
  simplement.
\end{theoreme}
En d'autres termes, il existe une unique homographie qui envoie trois
points distincts sur trois points distincts.
\begin{definition}[birapport]
  Soient $M,N,P$ trois points distincts et $Q\in\Pc^1(\C)$, on définit
  $[M,N,P,Q]$ comme l'image par $Q$ de l'unique homographie qui envoie
  $M$ sir 0, $N$ sur $\infty$, $P$ sur 1.
\end{definition}
Remarque : l'expression analytique de $[M,N,P,Q] =
\left(\frac{z_Q-z_N}{z_Q-z_M}\right)\times
\left(\frac{z_P-z_N}{z_P-z_M}\right)^{-1}$
\begin{proposition}
  \begin{itemize}
    \item $[a,b,c,d] = [b,a,c,d]^{-1}=[a,b,d,c]^{-1}$
    \item $[a,b,c,d] = [c,d,a,b] $
    \item $[a,b,c,d] + [a,c,b,d] = 1$
  \end{itemize}
\end{proposition}
\begin{proposition}
  Soient $a,b,c,d$ avec $a,b,c$ distincts et $a',b',c',d'$ avec
  $a',b',c'$ distincts, alors :

  Il existe une unique homographie que envoie $a,b,c,d$ sur
  $a',b',c',d'$ si et seulement si $[a,b,c,d] = [a',b',c',d']$
\end{proposition}
\begin{theoreme}
  Soit $f$ une bijection de $\Pc^1(\C)$ alors $f$ est une homographie si
  et seulement si $f$ conserve les birapports.
\end{theoreme}
\begin{proposition}
  4 points sont cocycliques ou alignés si et seulement si leur birapport
  est réel.
\end{proposition}
\begin{corollaire}
  Les homographies préservent les cercles et les droites
\end{corollaire}
\end{document}

// Illustration de la règle de classification par noyau (naif)
// On suppose Xi,Yi déjà tirés

h=0.1; 

// Test de la règle :

[X,Y]=point_alea();
Y_naif=classif_noyau(X,Xi,Yi,h,K_naif); // calcul de Y selon noyau naïf

if(Y_naif==1)
	plot(X(1),X(2),'+r') // croix rouge
else
	plot(X(1),X(2),'+b') // ou bleu, selon résultat de la règle
end

// dessin de la boule autour de X (support du noyau)
t=linspace(0,2*%pi,100);
plot(X(1)+h*cos(t),X(2)+h*sin(t),'k')




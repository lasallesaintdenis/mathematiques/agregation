function res=rand_exp(m,n,lambda)
    res=-log(rand(m,n))./lambda
endfunction

rand_exp(1,10,2)
grand(1,10,'exp',1/2)
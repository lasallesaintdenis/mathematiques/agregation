n=1000; // n^(-1/5) = 0.25
X1=grand(1,n,'nor',0,1/2); 
X2=grand(1,n,'nor',3,1); 
U=(rand(1,n)<0.4);
X=X1.*U+X2.*(1-U);
t=-5:0.01:5;

h=[1/50 1/10 1/5 1/2]; // diverses valeurs de h

scf(2);clf;

subplot(3,1,1);
plot(t,estim_rosenblatt(X,h(1),t),'r');
plot(t,estim_rosenblatt(X,h(2),t),'b');
plot(t,estim_rosenblatt(X,h(3),t),'g');
plot(t,estim_rosenblatt(X,h(4),t),'k');
xtitle('Rosenblatt');
legend(string(h(1)),string(h(2)),string(h(3)),string(h(4)));

subplot(3,1,2);
//scf(1);clf;
plot(t,estim_epan(X,h(1),t),'r');
plot(t,estim_epan(X,h(2),t),'b');
plot(t,estim_epan(X,h(3),t),'g');
plot(t,estim_epan(X,h(4),t),'k');
xtitle('Epanechnikov');

subplot(3,1,3);
//scf(2);clf;
plot(t,estim_gauss(X,h(1),t),'r');
plot(t,estim_gauss(X,h(2),t),'b');
plot(t,estim_gauss(X,h(3),t),'g');
plot(t,estim_gauss(X,h(4),t),'k');
xtitle('Gauss');


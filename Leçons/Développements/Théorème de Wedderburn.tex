\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Théorème de Wedderburn}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

Dans cet énoncé, le mot «corps» ne signifie pas «corps commutatif».

\begin{proposition}[Équation aux classes]
  Soit $G$ un groupe fini opérant sur un ensemble fini $E$ et $x_1,\dots x_q
  \in E$  soient les $G$-orbites deux à deux distinctes de $G$ dans $E$.
  Alors \begin{align*}
    \card(E) & = \card(G.x_1) + \cdots + \card(G.x_q) \\
             & = (G:G_{x_1}) + \cdots + (G:G_{x_q})   \\
  \end{align*}
\end{proposition}

\begin{definition}[Polynômes cyclotomiques]
  Pour tout $n\in N^*$, on note \[ \U_n = \left\{\exp\left(
  \frac{2ik\pi}n\right), k\in \Z \right\} \subset \C .\] On dit qu'un
  élément $x \in \U_n$ est une \emph{racine primitive $n$\up{ième} de
  l'unité} si $x$ engendre le groupe multiplicatif $\U_n$. On note
  $\Pi_n$ l'ensemble des racines primitives $n$\up{ième} de l'unité, et
  on définit le \emph{polynôme cyclotomique d'indice $n$} par \[ \Phi_n
  = \prod_{\xi \in \Pi_n} (X - \xi ) . \]
\end{definition}

\begin{theoreme}[Théorème de Wedderburn]\label{theo:wedderburn}
  Si $\K$ est un corps fini, il est commutatif.
\end{theoreme}

\begin{proof}
  Soit $p = \car(\K)$ et $Z$ le centre de $\K$. On a $\F_p \subset Z$. Si $r
  = [Z : \F_p] $ et $n = \dim_Z \K$, il vient que $\card(Z) = p^r = q,
  \card(\K) = q^n$. Pour $x \in \K^*$, notons $\K_x$ le centralisateur de
  $x$ dans $\K$, et soit $\K_x^* = \K^* \cap \K_x$. Comme $Z \in \K_x$, il
  existe $d(x) \in \N^*$ tel que $\card(\K_x) = q^{d(x)}$ et $\K_x^*$ étant
  un sous-groupe de $\K^*$, $q^{d(x)} - 1$ divise $q^n - 1$. Écrivons $n =
  s(x)d(x) +t(x)$ avec $0 \leqslant t(x) < d(x)$. Alors $q^n - 1$ est égal à
  : \[ ((q^{d(x)})^{s(x)} - 1)q^{t(x)} + q^{t(x)} - 1 = (q^{d(x)} - 1)
  \sum_{i=0}^{s(x) - 1} q^{id(x) + t(x)} + q^{t(x)} - 1 . \] On en déduit
  que $q^{d(x)} - 1$ divise $q^{t(x)} - 1$. Comme $q \geqslant 2$ et
  $0\leqslant t(x) < d(x)$, il vient que $t(x) = 0$ et donc $d(x) \mid n$.

  Pour $y \in \K^*$, dire que $y \in Z$ signifie que $d(y) = n$. Supposons
  $\K$ non commutatif. Alors $n > 1$.. Faisant opérer $\K^*$ sur lui-même
  par conjugaison, il résulte de l'équation aux classes : \[ q^n - 1 = q - 1
  + \sum_{d \in S} \frac{q^n - 1}{q^d - 1} \] où $S$ est un ensemble de
  diviseurs de $n$ distincs de $n$. En utilisant la définition des polynômes
  cyclotomiques, on obtient : \[ q^n - 1 = \prod_{m \mid  n} \phi_m(q),
  \quad q^d - 1 = \prod_{m \mid d} \phi_m(q) \] et pour $d \mid n, d \neq
  n, \phi_n(d)$ divise $(q^n - 1)/(q^d - 1)$. En utilisant l'égalité
  résultant de l'équation aux classe, on voit que $\phi_n(q)$ divise $q - 1$
  et $\lvert \phi_n(q) \rvert \leqslant q - 1$. Soit $\zeta \in \C$ une
  racine primitive n\up{ième} de l'unité. Comme $n \geqslant 2$, on a $\zeta
  \neq 1$ et $\lvert q - \zeta \rvert > q - 1$. Alors $\lvert \phi_n(q)
  \rvert > (q - 1)^{\varphi(n)} \geqslant q -1$. Contradiction.
\end{proof}


\end{document}

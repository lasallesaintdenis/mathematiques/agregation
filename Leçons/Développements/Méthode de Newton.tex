\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Méthode de Newton}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}


\begin{theoreme}
  Soit $f \colon [c,d] \to \R$ une fonction de classe $\Co^2$. On
  suppose $c < d, f(c) < 0 < f(d)$ et $f'(x) > 0$ pour tout $x \in
  [c,d]$. On considère la suite récurrente \[ \forall n \in \N, x_{n+1}
  = \phi(x_n) \text{ avec } \phi(x) = x - \frac{f(x)}{f'(x)} . \] Alors
  en notant $a$ l'unique valeur d'annulation de la fonction $f$, on a :
  \begin{enumerate}
    \item Il existe $\alpha > 0$ tel que pour tout $x_0 \in [a - \alpha,
      a + \alpha] = I, (x_n)_{n\in \N}$ converge vers $a$ de manière
      quadratique et il existe $C>0$ tel que pour tout $n \in \N$, on a
      \[ \lvert x_{n+1} - a \rvert \leqslant C \lvert x_n - a \rvert^2
      \]
    \item Si, de plus, pour tout $x \in [a,d], f''(x) > 0$, alors pour
      tout $x_0 \in [a,d]$, la suite $(x_n)_{n\in \N}$ est strictement
      décroissante (ou constante) et \[ \left\{ \begin{array}{r}
            \forall n \in \N, 0 \leqslant x_{n+1} - a \leqslant C (x_n -
            a)^2 \\
            x_{n+1} - a \underset{n\to \infty}{\sim}
            \frac{f''(a)}{2f'(a)}(x_n - a)^2 \text{ pour } x_0 > a .
        \end{array} \right. \]
    \end{enumerate}
\end{theoreme}

\section*{Preuve}
On sait que $f$ est strictement croissante et que $0 \in ]f(c), f(d)[$.
Ainsi, d'après le théorème des valeurs intermédiaires, il existe un
unique $a \in ]c,d[$ tel que $f(a) = 0$. Le but de la méthode de Newton
est d'approcher $a$ à partir d'une valeur grossière $x_0$ obtenue par
exemple par l'algorithme de dichotomie avec un algorithme à la vitesse
de convergence quadratique.

\begin{center}
  \begin{tikzpicture}[xscale=2]
    \draw [->] (-0.5,0) -- (4,0) node [below] {$x$} ;
    \draw [->] (0,-2) -- (0,4) node [left] {$y$} ;
    \draw[thick] plot [domain = 0.5:2.3] (\x, \x*\x - 2) ;
    \draw [dashed] (2,0) -- (2,2) node [draw,circle, inner sep=1pt] {} ;
    \draw plot [domain = 1.2:2.3] (\x, 4*\x - 6) ;
    \draw (2,0) node [below] {$x_0$} ;
    \draw (6/4,0) node [below right] {$x_1$} ;
    \draw ({sqrt(2)}, 0) node [above left] {$a$} ;
  \end{tikzpicture}
\end{center}

L'idée est de remplacer la courbe représentative de $f$ par sa tangente
au point $x_0$ : \[ y = f'(x_0) (x - x_0) + f(x_0) . \] L'abscisse $x_1$
du point d'intersection de cette tangente avec l'axe $y = 0$ est donné
par \[x_1 = x_0 - \frac{f(x_0)}{f'(x_0)} . \] $x_1$ est en général une
meilleure approximation de $a$ que $x_0$. On veut donc réitérer la
fonction \[ \phi(x) = x - \frac{f(x)}{f'(x)}, \] afin de créer une suite
de points $(x_n)_{n\in \N}$ convergeant vers $a$.

\begin{enumerate}
  \item Comme $f(a) = 0$, on peut écrire pour $x \in [c,d]$,
    \begin{align*} \phi(x) - a & = x - a - \frac{f(x) - f(a)}{f'(x)} \\
                               & = \frac{f(a) - f(x) - (a - x)f'(x)}
    {f'(x)} \end{align*} En appliquant la formule de Taylor Lagrange à
    l'ordre 2, il existe $z_x \in [ \min(a,x), \max(a,x)]$ tel que \[
    \phi(x) - a = \frac{1}2 \frac{f''(z_x)}{f'(x)}(x-a)^2 . \] Prenons $C
    = \frac{\max_{[c,d]} \lvert f'' \rvert}{2 \min_{[c,d]} \lvert
      f'\rvert}$ et on obtient l'inégalité \[ \lvert  \phi(x) - a \rvert
    \leqslant C \lvert x - a \rvert^2, x \in [c,d]. \]
    Soit $\alpha > 0$ assez petit pour que $C\alpha < 1$ et que
    l'intervalle $[a - \alpha, a + \alpha]$ soit contenu dans
    l'intervalle $[c,d]$. Alors $x \in I$ entraîne que $\lvert  \phi(x)
    - a \rvert \leqslant C\alpha^2 \leqslant \alpha$, d'où $\phi(I)
    \subset I$.  Ainsi, si $x_0 \in I$, on a donc pour tout $n \in \N,
    x_n \in I$ et \[ \lvert x_{n+1} - a \rvert = \lvert  \phi(x) - a
    \rvert \leqslant C \lvert x - a \rvert^2 \] d'où \[ C \lvert x - a
    \rvert \leqslant ( C \lvert x_0 - a \rvert)^{2n} \leqslant
    (C\alpha)^{2n} \] et la convergence quadratique de $x_n$ vers $a$
    puisque $C\alpha < 1$.
  \item Pour $a < x \leqslant d$, on a $f'(x) > 0$ et $f(x) \geqslant 0$
    d'où \[ \phi(x) = x - \frac{f(x)}{f'(x)} < x . \] D'après (i), on a
    d'autre part \[ \phi(x) - a = \frac{1}2
    \frac{f''(z_x)}{f'(x)}(x-a)^2 > 0 . \] Ces deux inégalités montrent
    que l'intervalle $I = [a,d]$ est stable par $\phi$ et que pour $a <
    x_0 \leqslant d$, les itérés de $x_n$ vérifient aussi $a < x
    \leqslant d$ et forment une suite strictement décroissante. Si $x_0
    = a$, la suite est constante. La suite $(x_n)$ admet donc une limite
    $l$, qui vérifie $\phi(l) = l$ donc $f(l) = 0$ et $l$ ne peut être
    que $a$.

    La convergence des $x_n$ vers $a$ est quadratique et on a, comme en
    (i), \[ 0 \leqslant x_{n+1} - a \leqslant C(x_n - a)^2 . \] Enfin,
    cette égalité est essentiellement optimale, si $a < x_0 \leqslant
    d$, on a $x_n > a$ pour tout $n \in \N^*$ et \[ \frac{x_{n+1} - a}{
    (x_n - a)^2} = \frac{1}2 \frac{f''(z_x)}{f'(x)} \] d'après (i), avec
    $a < z_x < x_n$. La fraction tend donc vers $\frac{f''(a)}{f'(a)}$
    lorsque $n \to \infty$.
\end{enumerate}
\qed

\end{document}

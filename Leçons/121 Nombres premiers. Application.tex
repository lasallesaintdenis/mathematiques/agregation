\documentclass[a4paper,12pt,,twocolumn,landscape,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{amsthm}
\usepackage{xstring}
\usepackage{babel}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Pc}{\mathbf{P}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\ssi}{si, et seulement si }
\renewcommand{\implies}{\DOTSB \;\Rightarrow \;}
\renewcommand{\iff}{\DOTSB \;\Leftrightarrow \;}
\everymath{\displaystyle\everymath{}}
\newcommand{\Ccal}{\mathcal{C}}
\parindent0pt
\columnsep25pt

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{8 janvier 2014}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother


\begin{document}
\maketitle

\section{Définitions et premières propriétés}

\begin{definition}
  $p$ est premier si et seulement si $p\mid ab \implies p\mid a
  \text{ ou } p\mid b$
\end{definition}

La factorisation d'un nombre en produit de nombres premiers est unique,
à l'ordre près.

\begin{proposition}
  Il y a une infinité de nombres premiers
\end{proposition}

\begin{theoreme}
  $\sum_{p\text{ premier}}=\frac1p = \infty$
\end{theoreme}

\begin{theoreme}[de Dirichlet]
  $a,b\in\N^*, a\wedge b = 1 \implies \{a+bn | n\in\N\}$ contient une
  infinité de nombres premiers.
\end{theoreme}

\begin{theoreme}
  $\Pi(x) = \#\{p\mid p\leq x\}$

  $\Pi(x)\sim \frac{x}{\ln x}$
\end{theoreme}

\begin{proposition}
  Soit $n\in\N^*, n\geq 1$
  \begin{description}
    \item[Fermat] $n\text{ premier} \implies k^{n-1}\equiv 1 \mod n,
      \forall k\wedge n = 1$
    \item[Wilson] $n\text{ premier} \iff (n-1)! \equiv -1 \mod n$
    \item[] $n\text{ premier} \begin{array}{ll}
        \implies & \#(\Z/n\Z)^* = n-1 \\
        \implies & \forall \overline{k}\in(\Z/n\Z)^* \overline{k}^{n-1}
        = 1\\
        \implies & k^{n-1} \equiv 1 \mod n
      \end{array}$
    \item[] $n\text{ non premier}\implies (n-1)!\wedge n \neq 1 \implies
      a\mid n, a\mid (n-1)! \implies (n-1)!\nequiv -1 \mod n$
    \item[] $n$ premier $\implies (\Z/n\Z)^*$ est un groupe.
  \end{description}
\end{proposition}

\begin{lemme}
  $G$ un groupe abélien fini, $\prod_{g\in G} g = \prod_{\substack{g\in
  G\\ g^2=1}} g$
\end{lemme}

\begin{definition}
  $M_p=2^p-1$ est un nombre de Mersenne.
\end{definition}

\begin{proposition}
  Supposons $n\geq 1$ tel que $2^n+1$ soit premier, alors $\exists
  k\in\N \mid n=2^k$
\end{proposition}

\begin{definition}
  $F_n=2^{2^n}+1$ est un nombre de Fermat.
\end{definition}

$F_0,\dots,F_4$ sont premiers.

\begin{proposition}
  $\forall n, F_{n+1} = F_0F_1\cdots F_n + 2$
\end{proposition}

\begin{corollaire}
  Les $F_n$ sont deux  à deux premiers.
\end{corollaire}

\begin{definition}[Indicatrice d'Euler]
  La fonction $\phi(n) = \#(\Z/n\Z)^* = \#\{1\leq k\leq n \mid k\wedge n
  =1\}$ est la fonction indicatrice d'Euler.
\end{definition}

\begin{theoreme}[de Gauss-Wantzel]
  On peut construire à la règle et au compas un polygône à $n$ côtés
  $\iff \phi(n)$ est une puissance de 2 $\iff n=2^lm_0m_1\dotsm_k$
\end{theoreme}

\begin{proposition}
  $\phi(n) = n \prod_{p\mid n}\frac{p-1}{p}$
\end{proposition}

\begin{proposition}
  $Z/nZ$ est intègre $\iff n$ premier.
\end{proposition}

\begin{proposition}
  $Z/nZ \setminus \{0\}$ est un corps $\iff n$ premier.
\end{proposition}

\section{Applications}

Codes correcteurs dans $\mathbf{F}_q$

RSA

\end{document}

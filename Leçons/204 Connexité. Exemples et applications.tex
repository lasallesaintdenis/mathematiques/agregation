\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\title{204 Connexité. Exemples et applications}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

Dans cette leçon, $\K$ désigne un corps commutatif.

\section{Généralités}

\begin{theoremedefinition}
  Soit $E$ un espace topologique. Les assertions suivantes sont
  équivalentes :
  \begin{enumerate}[label=(\roman*)]
    \item il existe une partie propre de $E$ (autre que $E$ et
      $\varnothing$), qui est à la fois ouverte et fermée ;
    \item il existe une partition de $E$ en deux ouverts ;
    \item il existe une partition de $E$ en deux fermés.
  \end{enumerate}
  Un espace topologique est dit \emph{connexe} si et seulement si il ne
  vérifie pas ces assertions.
\end{theoremedefinition}

\begin{definition}
  On appelle \emph{partie connexe} d'un espace topologique $E$ tout
  partie qui, munie de la topologie induite, est un espace connexe.
\end{definition}

\begin{definition}
  On appelle \emph{domaine} toute partie d'un espace topologique qui est
  à la fois ouverte et connexe.
\end{definition}

On a la caractérisation suivante pour les espaces connexes.
\begin{theoreme}
  Un espace topologique est connexe si et seulement si toute application
  continue de $E$ dans l'espace discret $\{0;1\}$ est constante.
\end{theoreme}

L'intérêt est réside dans la possibilité de déduire des propriétés sur
l'image par une fonction continue d'un connexe.
\begin{theoreme}
  Soient $E$ et $F$ des espaces topologiques, $f : E  \to F$ une
  application continue. Si $E$ est connexe, $f(E)$ est une partie
  connexe de $F$. Plus généralement, si $A$ est une partie connexe,
  $f(A)$ est une partie connexe.
\end{theoreme}

\begin{theoreme}
  Soit $A$ une partie connexe de l'espace topologique $E$. Alors toute
  partie $B$ telle que $A \subset B \subset \overline{A}$ est connexe.
  En particulier $\overline{A}$ est connexe.
\end{theoreme}

\begin{theoreme}
  Soit $(A_i)_{i\in I}$ une famille de parties connexes de $E$, telle
  qu'il existe $i_0 \in I$ vérifiant $\forall i \in I\ A_i \cap
  A_{i_0} \neq \varnothing$. Alors $A = \displaystyle \bigcup_{i\in I}
  A_i$ est une partie connexe de $E$.
\end{theoreme}

\begin{corollaire}
  Si $\displaystyle \bigcap_{i\in I}A_i \neq \varnothing,\ \displaystyle
  \bigcup_{i\in I} A_i$ est connexe.
\end{corollaire}

On peut aussi définir la notion de composante connexe.

\begin{theoremedefinition}
  Soit $a$ un point de $E$. Il existe une plus grande partie connexe de
  $E$ dont un point est $a$. C'est un fermé de $E$. On l'appelle
  \emph{composante connexe} de $a$ dans $E$.
\end{theoremedefinition}

On a aussi un résultat sur les espaces produits.

\begin{theoreme}
  Soient $E_1,\dots,E_p$ des espaces topologiques connexes non vides. Le
  produit $E = E_1 \times \cdots \times E_p$ est connexe si et seulement
  si chaque $E_i$ est connexe.
\end{theoreme}

\section{Exemples}

\begin{theoreme}
  $\R$ est connexe.
\end{theoreme}

On peut s'interroger sur les parties connexes de $\R$.

\begin{theoreme}
  Les parties connexes de $\R$ sont les intervalles de $\R$.
\end{theoreme}

On a l'application suivante :

\begin{theoreme}[des valeurs intermédiaires]
  Soient $E$ un espace topologique connexe, et $f \colon E\to \R$ une
  fonction numérique continue. Si $f$ prend les valeurs $\alpha$ et
  $\beta$, elle prend aussi toutes les valeurs $y \in ]\alpha ; \beta
  [$.
\end{theoreme}

\begin{definition}
  Soit $E$ un espace topologique. On appell \emph{chemin} tout
  application continue $\varphi \colon [\alpha;\beta] \to E$ d'un
  segment de $\R$ -- qu'on pourra le plus souvent supposer être $[0;1]$
  -- dans $E$ ; $\varphi([\alpha;\beta])$ est la \emph{trajectoire} du
  chemin ; $\varphi(\alpha)$ est l'\emph{origine} ; $\varphi(\beta)$ est
  l'extrémité ; on dit que $\varphi$ « joint les points »
  $\varphi(\alpha)$ et $\varphi(\beta)$.
\end{definition}

\begin{definition}
  Un espace topologique $E$ est dit \emph{connexe par arcs} si, et
  seulement si deux quelconques de ses points peuvent être joints par un
  chemin.
\end{definition}

L'avantage de la connexité par arcs est que cette notion fournit une
condition suffisante pour qu'un espace soit connexe.

\begin{theoreme}
  Soit $E$ un espace topologique connexe par arcs. Alors $E$ est
  connexe.
\end{theoreme}

\begin{proposition}
  Soient $E$ un espace topologique et $A$ une partie de $E$. La
  trajectoire de tout chemin joignant un point de l'intérieur de $A$ et
  de l'extérieur de $A$ rencontre la frontière de $A$.
\end{proposition}



\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème central limite}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Soit $f \colon I \times X \to \R$ une application. Supposons que
  \begin{enumerate}
    \item pour tout $u \in I, x\mapsto f(u,x) \in L_{\R}^1(\mu)$ ;
    \item $\mu(\diff x)$ --pp, $u \mapsto f(u,x)$ est dérivable sur tout
      l'intervalle $I$ ;
    \item $\mu(\diff x)$ --pp, pour tout $u \in I$.
  \end{enumerate}
  \[ \left\lvert \frac{\partial f}{\partial u}(u,x) \right\rvert
  \leqslant g(x) \text{ avec } g\in L_{\R}^1(\mu), \]
  alors la fonction $F(u) = \int_X f(u,x) \mu(\diff x)$ est définie et
  dérivable sur tout l'intervalle $i$, de dérivée \[ F'(u) = \int_X
  \frac{\partial f}{\partial u}(u,x) \mu(\diff x) . \]
\end{theoreme}

\begin{theoreme}[Levy (admis)]
  Soit, pour tout $n \in \N$, $X_n$ une variable aléatoire définie sur
  un espace probabilisé à valeurs dans $\R^d$ et $X$ une variable
  aléatoire définie sur un espace probabilisé à valeurs dans $\R^d$.

  Alors on a \[ X_n \xrightarrow{\L} X \iff \forall t \in \R^d,
  \phi_{X_n}(t) = \phi_X(t) . \]
\end{theoreme}

\begin{lemme}
  Pour tout $z \in \C$ et pour tout $n \in \N^*$, on a : \[ \left\lvert
    \exp(z) - \left( 1 + \frac{z}n \right)^n \right\rvert \leqslant
    \exp(\lvert z \rvert) - \left( 1 + \frac{\lvert z \rvert}n
    \right)^n . \] On obtient donc que pour tout $z \in \C$ \[ \lim_{n
  \to +\infty} \left( 1 + \frac{z}n \right)^n = \exp(z) . \]
\end{lemme}

\begin{proposition}
  Soit $X$ une variable aléatoire. Si $X$ admet un moment d'ordre $n$
  alors $\phi_X$ est de classe $\Co^n$ et pour tout $k \in \llbracket 1,
  n \rrbracket$ et tout $t \in \R$, on a : \[ \phi_X^{(k)} (t) = i^k
  \int_{\Omega} X^k \exp(itX) \diff \P . \]
\end{proposition}

\begin{lemme}
  Si la variable aléatoire réelle $X$ admet un moment d'ordre 2, sa
  fonction caractéristique $\phi_X$ admet un développement limité à
  l'ordre 2 en 0 donné par , pour tout $t \in \R$, \[ \phi_X(t) = 1 + it
  \E[X] - \frac{t^2}2 \E[X^2] + o\,(t^2) . \]
\end{lemme}

\begin{theoreme}
  Soit $X_n$ une suite de variables aléatoires iid à valeurs dans $\R$
  admettant un moment d'ordre 2. Alors \[ Y_n = \frac{1}{\sqrt{n}}
    \left[ \sum_{i = 1}^n (X_i - \E[X_i] ) \right] \xrightarrow{\L}
  \Normal(0, \Var(X_1)) . \]
\end{theoreme}

\section*{Preuve}


\subsection*{Lemme 1}

La formule du binôme de Newton donne, pour tout $z \in \C$, \[ \exp(z) -
  \left( 1 + \frac{z}n \right)^n = \sum_{j=0}^{+\infty} \frac{z^j}{j!} -
  \sum_{j=0}^n \binom{n}{j} \frac{z^j}{n^j} .\] De plus, on a \[
  \frac{\binom{n}{j}}{n^j} = \frac{1}{j!}\frac{n(n - 1) \cdots (n - (j -
   1))}{n^j} = \frac{1}{j!} \prod_{k=0}^{j-1} \left( 1 - \frac{k}n
  \right) .\] En reportant ce dernier résultat dans l'égalité
  précédente, on obtient \begin{align*} \exp(z) - \left( 1 + \frac{z}n
  \right)^n & = \sum_{j=0}^{+\infty} \frac{z^j}{j!} - \sum_{j=0}^n
  \frac{z^j}{j!} \prod_{k=0}^{j-1} \left( 1 - \frac{k}n\right) \\
  & = \sum_{j=n+1}^{+\infty} \frac{z^j}{j!} + \sum_{j=0}^n
  \frac{z^j}{j!} \left[ 1 - \prod_{k=0}^{j-1} \left( 1 -
  \frac{k}n\right) \right] . \end{align*} Or, on sait que $1 -
  \prod_{k=0}^{j-1} \left( 1 - \frac{k}n\right) \geqslant 0$, et on
  obtient donc : \[ \left\lvert \exp(z) - \left( 1 + \frac{z}n \right)^n
      \right\rvert \leqslant \sum_{j=n+1}^{+\infty} \frac{\lvert z
      \rvert^j}{j!} + \sum_{j=0}^n \frac{\lvert z \rvert^j}{j!} \left[ 1
  - \prod_{k=0}^{j-1} \left( 1 - \frac{k}n\right) \right] . \] Nous
  obtenons donc le résultat avec l'égalité précédemment démontrée
  appliquée à $\lvert z \rvert$. Enfin, comme $\ln\left(1 + \frac{\lvert
    z\rvert}n \right)^n = \lvert z \rvert + o\,(1)$, on a que
    $\lim_{n\to +\infty} \left(1 + \frac{\lvert z\rvert}n \right)^n =
    \exp(\lvert z \rvert)$, ce qui entraîne d'après l'inégalité prouvée
    que $\lim_{n\to +\infty} \left(1 + \frac{z}n \right)^n = \exp(z)$.
    \qed

\subsection*{Proposition 2}

On sait que
\begin{itemize}
  \item Pour tout $t \in \R, x \mapsto \exp(itx)$ est $\P_X$ intégrable
    ;
  \item pour tout $k \in \llbracket 1, n \rrbracket$, \[
    \frac{\partial^k}{\partial t^k} \exp(itx) = (ix)^k \exp(itx). \]
\end{itemize}
On a donc \[ \left\lvert \frac{\partial^k}{\partial t^k} \exp(itx)
\right\rvert \leqslant \lvert x \rvert^k . \] Par hypothèse, le terme de
droite est $\P_X$ intégrable. En appliquant $k$ fois le théorème de
dérivation sous le signe intégrale dépendant d'un paramètre, pour tout
$t \in \R$, \[  \phi_X^{(k)} (t) = i^k \int_{\Omega} X^k \exp(itX) \diff
\P . \] En particulier, \[  \phi_X^{(k)} (0) = i^k\E[X^k] . \] \qed

\subsection*{Lemme 3}

D'après la proposition précédente, $\phi$ est $\Co^2$, donc par la
formule de Taylor-Young, on a : \begin{align*} \phi_X(t) & = \phi_X(0) + t
\phi_X'(0) + \frac{t^2}2 \phi_X''(0) + o\,(t^2) \\ & = 1 + it\E[X] -
\frac{t^2}2 \E[X^2] + o\,(t^2) \end{align*} \qed

\subsection*{Théorème 4}

Les variables aléatoires $X_n$ sont indépendantes et de même loi, la
fonction caractéristique de $Y_n$ est donné par, pour tout $t \in \R$,
\[ \phi_{Y_n} \stackrel{\perp}{=} \prod_{i=1}^n \phi_{X_i -
  \E[X_i]}\left( \frac{t}{\sqrt{n}} \right) \stackrel{\text{même loi}}{=}
\left[ \phi_{X_i - \E[X_i]}\left( \frac{t}{\sqrt{n}} \right) \right]^n .
\] Le lemme précédent  appliqué à la variable aléatoire réelle centrée
$X_1 - \E[X_1]$, donne, pour tout $t \in \R$, \begin{align*} \phi_{Y_n}
  & = \left[ 1 - \frac{t^2}{2n}\E[(X_1 - E[X_1])^2] + o\,\left(
\frac{1}n \right) \right]^n \\ & = \left[ 1 - \frac{t^2}{2n}\Var(X_1) +
o\,\left( \frac{1}n \right) \right]^n . \end{align*}
On a précédemment montré que cela implique que \[ ÷\lim_{n\to +\infty}
\phi_{Y_n}(t) = \exp\left( - \frac{t^2}2\Var(X_1) \right) . \] Le terme
de gauche correspond à la fonction caractéristique d'une variable
aléatoire suivant une loi normale $\Normal(0, \Var(X_1))$. Donc d'après
le théorème de Levy, on obtient : \[ Y_n \xrightarrow{\L} \Normal(0,
\Var(X_1)) . \] \qed


\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}}
\newcommand{\Aut}{\mathop{Aut}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Étude d'une suite définie par récurrence}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

Soit $\lambda \in ]0;1]$. Étudier le comportement de la suite définie
par $x \in ]0;1[$ et $\forall n \in \N, x_{n+1} = 1 - \lambda x_n^2$.

\section*{Preuve}

\subsection*{$(x_{2n})$ et $(x_{2n+1})$ convergent}
$f \colon ]0;1[ \to ]0;1[, x \mapsto 1 - \lambda x^2$ est bien définie
car $f(]0;1[) \subset ]0;1[$.

En effet, $f$ décroît et $f(0) = 1$ et $f(1) = 1 - \lambda \geqslant 0$.

Donc $f\circ f$ croît et les deux suites $(x_{2n})$ et $(x_{2n+1})$ sont
monotones. De plus, elles sont à valeurs dans $]0;1[$ donc convergentes.

\subsection*{$f\circ f(x)$ et $g(x) = f \circ f(x) - x$}

$f \circ f (x) = 1 - \lambda (1 - \lambda x^2)^2 = - \lambda^3 x^4 + 2
\lambda^2 x^2 + (1 - \lambda)$

Les limites de $(x_{2n})$ et $(x_{2n+1})$ sont à chercher parmi les
points fixes de $f\circ f$, c'est à dire les zéros de $g$.

Or, un point fixe de $f$ est un point fixe de $f \circ f$. Donc un zéro
de $k:x\mapsto f(x) -x$ est un zéro de $g$ et on peut factoriser $g$ par
$k$ : \[ g(x) = (f(x) - x)k(x) = (-\lambda x^2 - x + 1)(\lambda^2 x^2 -
\lambda x +1 - \lambda). \]

\subsection*{Les zéros de $g$}
Si $g(l) = 0$, alors
\begin{itemize}
  \item soit $l$ est l'unique point fixe de $f$ dans $[0;1]$ : \[ l =
    \frac{-1 + \sqrt{1 + 4\lambda}}{2\lambda} ; \]
  \item soit $k(l) = 0$.
\end{itemize}

Le discriminant de $k$ vaut $\Delta(\lambda) = \lambda^2 + 4 (\lambda -
1)\lambda^2 = \lambda^2 (4\lambda - 3)$

\begin{description}
  \item[Cas 1 : $\lambda < \frac34$] Alors $\Delta(\lambda) < 0$. Donc
    un zéro de $g$ ne peut-être un zéro de $k$. Donc $f \circ f$ admet
    un unique point fixe, celui de $f$.

    Donc $\lim_{n\to+\infty} x_{2n} = \lim_{n\to+\infty} x_{2n+1} =
    \frac{-1 + \sqrt{1 + 4\lambda}}{2\lambda}$
  \item[Cas 2 : $\lambda = \frac34$] Alors $\Delta(\lambda) =0$, donc
    $k$ admet un zéro $l' = \frac1{2\lambda} = \frac{-1 + \sqrt{1 +
    4\lambda}}{2\lambda} = l$. Le zéro de $k$ est donc l'unique point
    fixe de $f$.

    Donc $\lim_{n\to+\infty} x_{2n} = \lim_{n\to+\infty} x_{2n+1} =
    \frac{1}{2\lambda}$

  \item[Cas 3 : $\lambda > \frac34$] Alors $\Delta(\lambda) > 0$ et $k$
    admet deux zéros $l_1 = \frac{1 + \sqrt{4\lambda-3}}{2\lambda}$ et
    $l_2 = \frac{1 - \sqrt{4\lambda-3}}{2\lambda}$.
\end{description}

\subsubsection*{$(x_n)$ ne converge pas vers $l$ sauf si $(x_n)$ est
stationnaire}

$f'(l)= -2\lambda l = 1 - \sqrt{1 + 4l} < -1$. Donc il existe un
voisinage $V$ de $l$ tel que $\forall x \in V, f'(x) < k$ si $k \in
]f'(l) ; -1 [$.

Si $x_n \to l$ alors à partir d'un rang $n_0, x_n \in V$ et $\lvert
f(x_n) - f(l) \rvert \geqslant \lvert k \rvert \lvert x_n -l \rvert$

D'où $\lvert x_{n+1} - l\rvert \geqslant \lvert k \rvert \lvert x_n -l
\rvert$ et donc $\lvert x_n -l \rvert \geqslant \lvert k \rvert^{n-n_0}
\lvert x_{n_0} -l \rvert \to +\infty$ si $x_{n_0} \neq l$.

Donc $(x_n)$ est stationnaire à partir d'un certain rang $n_0$.

Mais $x_{n+1} = l \iff 1 - \lambda x_n^2 = 1 - \lambda l^2 = l \iff x_n
= l$. Donc $(x_n)$ converge vers $l \iff x_0 = l$.

\subsubsection*{$x_{2n} \to l_1$ et $x_{2n+1} \to l_2$ ou inversement}

$(x_{2n})$ et $(x_{2n+1})$ ne peuvent converger vers la même limite,
sinon, celle-ci serait un point fixe de $f$, c'est-à-dire $l$, ce qui
est impossible.

De la même manière, $(x_{2n})$ ne peut converger vers $l$ et
$(x_{2n+1})$ non plus.

L'une converve vers $l_1 < l < l_2$

Si $x_0 < l$, alors $x_1 > l$ et $\forall n \in \N, x_{2n} < l$ et
$x_{2n+1} > l$. Donc $\lim_{n\to+\infty} x_{2n} = l_1$ et
$\lim_{n\to+\infty} x_{2n+1} = l_2$.

Si $x_0 > l$, c'est l'inverse.

\end{document}

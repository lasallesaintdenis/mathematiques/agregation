// Problème de ruine d'une compagnie d'assurance

function graphes(mu,lambda,p,n_max,fenetre)
// Dessine les pertes dans la fenêtre donnée (nombre entier), avec le nombre de sinistres donné
// pour les paramètres donnés

  X=grand(1,n_max,'exp',lambda); // sinistres
  intertemps=grand(1,n_max,'exp',1/mu); // intervalles entre sinistres
  
  s=[0,cumsum(X-p*intertemps)]; // bénéfice après le sinistre suivant
  
  scf(fenetre);clf;
  subplot(2,1,1);
  plot(0:n_max,s);
  xtitle('Pertes (juste après le n-ième sinistre)','$n$','$S_n$')
  
  subplot(2,1,2);
  r=0;t=0;
  R=[r];
  T=[t];
  for i=1:n_max
    r=r+p*intertemps(i); // avant le prochain sinistre
    t=t+intertemps(i);
    R=[R,r];T=[T,t];
    r=r-X(i); // après le sinistre
    R=[R,r];T=[T,t]; // (le temps ne change pas)
  end
  
  plot(T,-R);
  xtitle('Pertes (à tout instant)','$t$','$-R_t$')
  
endfunction


mu=3; // intensité des sinistres
lambda=5; // espérance du coût d'un sinistre

n_max=200; // nb de sinistres

graphes(mu,lambda,lambda*mu*0.5,n_max,0);
graphes(mu,lambda,lambda*mu,n_max,1);
graphes(mu,lambda,lambda*mu*2,n_max,2);

graphes(mu,lambda,lambda*mu*0.5,10,3);


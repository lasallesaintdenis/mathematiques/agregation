\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\newcommand{\Rel}{\mathcal{R}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\GrpPerm}{\mathfrak{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Somme de deux carrés}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

On cherche ici à donner une condition nécessaire et suffisante sur un entier
$n$ pour que celui-ci soit la somme de deux carrés.

On note $\A_2 = \{ x^2 + y^2 \mid (x,y) \in Z^2 \}$

\begin{lemme}
  Si $p > 2$ est premier, alors \[ -\dot{1} \quad \text{est un carré dans}
  \quad \Z /p\Z \iff p \equiv 1 \mod 4 . \]
\end{lemme}

\begin{proof}
  \emph{Condition nécessaire}. Supposons qu'il existe $x \in \Z / p\Z, x^2 =
  -\dot{1}$. Le groupe multiplicatif $(\Z / p\Z)^*$ étant d'ordre $p - 1$
  (car $p$ est premier) on a $x^{p - 1} = \dot{1}$. On a donc
  $(x^2)^{(p-1)/2} = (-\dot{1})^{(p-1)/2} = \dot{1}$ ce qui n'est possible
  que si $p - 1$ est pair, d'où la condition nécessaire.

  \emph{Condition suffisante}. Soit $k$ un entier tel que $p = 4k + 1$.
  Comme $p$ est premier, on a, d'après le théorème de Wilson, \[ 1 \cdot 2
    \cdots (2k) \cdot (2k + 1) \cdots (4k - 1) \cdot (4k) \equiv -1 \mod 4k
  +1 \] ce qui s'écrit aussi \[ 1 \cdot 2 \cdots (2k) \cdot (-2k) \cdots
  (-1) \equiv -1 \mod p \] ou encore \[ (-1)^{2k}(1 \cdot 2 \cdots (2k))^2
  \equiv -1 \mod p .\] En posant $x = 1 \cdot 2 \cdots
  (2k)$ dans $\Z / p\Z$, on obtient le résultat.
\end{proof}

\begin{remarque} La condition suffisante peut aussi se voir de la façon
  suivante. Comme $p$ est premier, $\Z / p\Z$ est un corps. L'équation
  $x^{(p-1)/2} = \dot{1}$ possède au plus $(p - 1)/2$ solutions dans $\Z /
  p\Z$. Comme $(\Z / p\Z)^*$ contient $p - 1 > (p-1)/2$ éléments, il existe
  donc $y \in (\Z / p\Z)^*, y = x^{(p-1)/2} \neq \dot{1}$. On a donc $y^2 =
  x^{p-1} = \dot{1}$ donc $(y - \dot{1})(y + \dot{1}) = \dot{0}$ et donc $y
  = -\dot{1}$ (car $y = \neq \dot{1}$). Or $p \equiv 1 \mod 4$, donc il
  existe un entier $k$ tel que  $(p - 1)/2 = 2k$. Si $z = x^k$, on a donc
  $z^2 = x^{2k} = x^{(p-1)/2} = y = -\dot{1}$, d'où le résultat.

\end{remarque}

\begin{theoreme}
  Un entier $n > 0$ est la somme de deux carrés d'entier si et seulement si
  tous les facteurs premiers de $n$ de la forme $4m + 3, m \in \N$ ont un
  exposant pair dans la décomposition en facteurs premiers de $n$.
\end{theoreme}

\begin{proof}
  Soient $X, Y \in \A_2$. $X$ s'écrit comme le module $\lvert x_1 + ix_2
  \lvert^2$ d'un nombre complexe $x$ et $Y$ comme le module $\lvert y_1 +
  iy_2 \rvert^2$ d'un nombre complexe $y$. Ainsi, $XY = \lvert xy
  \rvert^2 = (x_1^2 + y_1^2)^2 + (x_1y_2 - x_2y_1)^2$ est bien une somme de
  deux carrés.

  Soit $p$ un nombre premier tel que $p \equiv 1 \mod 4$. D'après le lemme,
  $-\dot{1}$ est un carré dans $\Z/p\Z$ et donc il existe $x \in \Z$ tel que
  $-1 \equiv x^2 \mod p$. On peut même choisir $0\leqslant x \leqslant p -
  1$. Comme $x^2 + 1 \equiv 0 \mod p$, il existe $m \in \Z$ tel que $x^2 + 1
  = mp$ et comme $0\leqslant x \leqslant p - 1$, on a $0 < m < p$. et donc
  $mp \in \A_2$.

  Soit $m_0$ le plus petit entier tel que $m_0p \in \A_2$. Il existe donc
  $x,y \in \Z$ tels que $x^2 + y^2 = m_0p$ Si $m_0$ divise $x$ et $y$, alors
  $m_0^2 \mid (x^2 + y^2)$ donc $m_0 | p$, ce qui est absurde car $1 < m_0
  \leqslant m < p$. Donc $m_0$ ne divise pas $x$ ou ne divise pas $y$. En
  désignant par $c$ et $d$ les entiers les plus proches de $x/m_0$ et
  $y/m_0$, les entiers $x_1 = x - cm_0$ et $y_1 = y - dm_0$ vérifient \[
    \lvert x_1 \rvert \leqslant \frac{1}2, \quad \lvert y_1 \rvert \leqslant
  \frac{1}2 \quad \text{et} \quad x_1^2 + y_1^2 > 0. \] Or $x_1 \equiv x
  \mod m_0$ et $y_1 \equiv y \mod m_0$ donc $x_1^2 + y_1^2 \equiv x^2 + y^2
  \mod m_0$, et donc il existe $m_1$ tel que $x_1^2 + y_2^2 = m_0m_1$. Comme
  $x_1^2 + y_2^2 > 0, m_1 > 0$. Par ailleurs $x_1^2 + y_2^2 \leqslant
  2\left( \frac{m_0}2 \right)^2 = \frac{m_0^2}2$ donc $m_1 < m_0$.

  D'après ce qui précède, on peut écrire que \[(x_1^2 + y_1^2)(x^2 + y^2) =
  m_0^2m_1p = (xx_1 + yy_1)^2 + (xy_1 - x_1y)^2 .\] Mais \[ \begin{cases}
    xx_1 + yy_1 =  x(x - cm_0) + y(y -dm_0) & =  m_0X \quad \text{avec} \quad X = p - cx -dy \in \Z \\
    xy_1 + yx_1 =  x(y - dm_0) + y(x -cm_0) & =  m_0Y \quad \text{avec} \quad X = cy -dx \in \Z .\\
    \end{cases}
  \] Donc $m_1 p = X^2 + Y^2 \in \A_2$. Or $1 \leqslant m_1 < m_0$, ce qui
  est contraire à l'hypothèse de minimalité faite sur $m_0$. Donc $m_0 = 1$
  et $p \in \A_2$.

  Pour la dernière partie, on peut raisonner en CN/CS :

  \emph{Condition nécessaire}. Soit $n = x^2 + y^2$, (avec $x, y \in \Z$) et
  $p$ un facteur premier de $n$ tel que son exposant dans la décomposition
  de $n$ en facteurs premiers soit impair. Notons $d = x \wedge y$. Les
  nombres $X = x/d$ et $Y = y/d$ sont premiers entre eux. Comme $n = d^2(
  X^2 + Y^2)$ est que l'exposant de $p$ est impair, on a nécessairement $p
  \mid X^2 + Y^2$. Or $p \nmid X$ (sinon $p \mid X$ donc $p \mid Y$, ce qui
  est absurde car $X \wedge Y = 1$). $\dot{X}$ est donc non nul dans
  $\Z/p\Z$. Comme $\dot{X}^2 + \dot{Y}^2 = \dot{0}$ dans $\Z/p\Z$, on a
  $(\dot{X}^{-1} \dot{Y})^2 = -\dot{1}$ donc $-\dot{1}$ est un carré et donc
  $p = 2$ ou $p \equiv 1 \mod 4$. D'où la condition nécessaire.

  \emph{Condition suffisante}. Soient $p_1, \dots p_k$ des facteurs premiers
  de $n$ dont l'exposant dans la décomposition en facteurs premiers de $n$
  est impair. On peut écrire $n = m^2 p_1\cdots p_k$, où $m \in \N^*$. Par
  hypothèse, on a pour tout $i, p_i \nequiv 3 \mod 4$, ce qui entraîne, les
  $p_i$ étant des nombres premiers que $p_i \equiv 1 \mod 4$ ou $p_i = 2$.
  Donc $p_i \in \A_2$ d'après la partie centrale et même $p_1\cdots p_k \in
  \A_2$ d'après le début. En remarquant que $m^2 = m^ + 0^2 \in \A_2$, on a
  $n \in \A_2$, d'où le résultat.
\end{proof}

\end{document}

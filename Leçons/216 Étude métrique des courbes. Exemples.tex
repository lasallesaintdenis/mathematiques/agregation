\documentclass[a4paper,10pt,twocolumn,landscape,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tikz-3dplot}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Pc}{\mathbf{P}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\dx}{\mathrm{d}\-}
\newcommand{\ssi}{si, et seulement si }
\renewcommand{\implies}{\DOTSB \;\Rightarrow \;}
\renewcommand{\iff}{\DOTSB \;\Leftrightarrow \;}
\everymath{\displaystyle\everymath{}}
\newcommand{\Ccal}{\mathcal{C}}
\parindent0pt
\columnsep25pt

\theoremstyle{nobreak}
\renewtheorem{proposition}{Proposition}
\renewtheorem{definition}{Définition}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{21 mai 2014}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
        \@author{} -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother


\begin{document}
\maketitle

\section{Arcs paramétrés de $\R^n$}
\subsection{Généralités}
\begin{definition}
  Un arc paramétré est une application $f$ de $I$ dans $\R^2$. Si $f$
  est de classe $\Ccal^k$ on dit que l'arc est de classe $\Ccal^k$.
\end{definition}

\begin{definition}
  Soit $f$ un arc paramétré. On note $\Gamma \colon = {f(t), t\in I}$
\end{definition}

\begin{definition}
  Soient $I$ et $J$ deux intervalles de $\R$ et $\varphi: J \to I$. On
  dit que $\varphi$ est un $\Ccal^k$ difféomorphisme ($k\geq 0$), si :
  \begin{enumerate}
    \item $\varphi$ est bijective
    \item $\varphi$ est de classe $\Ccal^k$
    \item $\varphi^{-1}$ est de classe $\Ccal^k$
  \end{enumerate}
\end{definition}

\begin{proposition}
  Un $\Ccal^k$ difféomorphisme ($k\geq 1$) de $J$ sur $I$ est
  strictement monotone
\end{proposition}

\begin{definition}[Changement de paramétrage admissible]
  Soient $f$ (resp. $g$) un arc paramétré de classe $\Ccal^k$ définis
  sur $I$ (resp. $J$). On dit que $g$ est un changement de paramétrage
  admissible de $f$ s'il existe un difféomorphisme $\varphi : J \to I$
  de classe $\Ccal^k$ tel que $g = f\circ\varphi$.
\end{definition}

On considère désormais que $f$ est un arc paramétré fixé sur $I$ de
classe $\Ccal^k$.
\begin{definition}[régulier, birégulier]
  Soit $t_0\in I$, on dit que $t_0$ est un point :
  \begin{itemize}
    \item singulier si $f'(t_0)=0$ ;
    \item régulier si $f'(t_0)\neq0$ ;
    \item birégulier si $(f'(t_0),f''(t_0))$ est libre.
  \end{itemize}
  Un arc est régulier (resp. birégulier) s'il est régulier (resp.
  birégulier) en chacun de ses points.
\end{definition}

\begin{proposition}
  Soit $f:I\to \R^2$ un arc paramétré de classe $\Ccal^k$, $k$
  «suffisament» grand, et $t\in I$. On note : $p = \min\left\{ p \in
  \N^* \mid f^{(p)}(t) \neq 0\right\}$ et  $q = \min\left\{ p \in \N^*
  \mid (f^{(p)}(t),f^{(q)}(t))\text{ est libre}\right\}$. Alors :
  \begin{itemize}
    \item pour $p, q$ pairs, on a un point de rebroussement de première
      espèce ;
    \item pour $p$ impair, $q$ pair ; le point singulier est dit
      d'allure normale ;
    \item pour $p,q$ impairs, on a un point d'inflexion ;
    \item pour $p$ pair et $q$ impair, le point est est un point de
      rebroussement de première espèce.
  \end{itemize}
\end{proposition}

\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,0.5) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,0.5) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{-\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,0.5) ;
  \draw plot [domain=0:0.7,smooth] (-\x,{\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=2]
  \draw [->] (-0.1,0) -- (1.1,0) ;
  \draw [->] (0,-0.1) -- (0,0.5) ;
  \draw plot [domain=0:0.7,smooth] (-\x,{-\x^2}) ;
  \draw plot [domain=0:0.7,smooth] (\x,{\x^3}) ;
\end{tikzpicture}

\subsection{Propriétés métriques, abscisse curviligne}
\begin{definition}[longueur d'arc]
  On appelle longueur de $f$ entre deux points $A(t_0)$ et $B(t_1)$ le
  nombre \[ l(\widearc{AB}) = \int_{t_0}^{t_1}\lVert
  f'(t)\rVert\,\mathrm{d}\-t \]
\end{definition}

\begin{definition}[abscisse curviligne]
  Soit $f : I\to \R^2$ un arc paramétré de classe au moins $\Ccal^1$, on
  appelle abscisse curviligne tout appplication de $s:I\to \R$ au moins
  $\Ccal^1$ vérifiant \[ s'^2 = x'^2 + y'^2 = \lVert f'\rVert^2 \]
\end{definition}

\begin{definition}[paramétrage normal]
  Si $g$ est un paramétrage admissible de $f$ telle que $\forall u\in J,
  \lVert g'\rVert$, on dit que celui-ci est normal.
\end{definition}

\begin{proposition}
  L'abscisse curviligne est un paramétrage admissible et normal.
\end{proposition}

\begin{definition}[vecteur tangent]
  Soit $A$ un point de $\Gamma$. on dit que $f$ admet un vecteur tangent
  en $A$ si $\lim_{M\to
  A}\frac{\overrightarrow{AM}}{\lVert\overrightarrow{AM}\rVert}$ existe
  et est finie. Dans ce cas, le vecteur limite s'appelle le vecteur
  tangent à $\Gamma$ en $A$.
\end{definition}

\begin{proposition}[cas d'un arc $\Ccal^1$]
  Si de plus, $f$ est de classe $\Ccal^1$, le vecteur tangent en tout
  point $t_0$ régulier est $T(t_0) = \frac{f'(t_0)}{\lVert
  f'(t_0)\rVert}$.
\end{proposition}
\begin{proposition}
  Si $f$ est régulier et paramétré normalement, alors : $T(s) = f'(s)$
\end{proposition}
\section{Courbes planes ; étude métrique}
\subsection{Inégalité isopérimétrique}
\begin{proposition}[formule de Héron]
  $ABC$ est un triangle de $R^2$, $BC = a ; CA = b ; AB = c$. On note
  $p = \frac{a+b+c}2$ le demi-périmètre. Alots $\mathcal{A} =
  \sqrt{p(p-a)(p-b)(p-c)}$
\end{proposition}
\begin{proposition}[inégalité isopérimétrique pour le triangle]
  Avec les notations précédents $4\pi\mathcal{A} \leq
  \mathcal{L}^2$
\end{proposition}
\begin{lemme}
  Soit $f$ une fonction de classe $\Ccal^1$ sur $[0;1]$ à valeurs dans
  $\C$ telle que $\int_0^1 f = 0$ et $f(0) = f(1)$. Alors \[ \int_0^1
  \lvert f\rvert^2 \leq \frac1{4\pi^2}\int_0^1 \lvert f'\rvert^2 \]
\end{lemme}
\begin{theoreme}[inégalité isopérimétrique]
  Soit $\Gamma$ une courbe du plan, régulière, de classe $\Ccal^1$
  fermée, sans points multiples. Soient $\mathcal{L}$ sa longueur et
  $\mathcal{A}$ l'aire quelle délimite. Alors \[ 4\pi\mathcal{A} \leq
  \mathcal{L}^2 \]
\end{theoreme}
\subsection{Propriétés métriques des courbes planes}
Dans l'ordre : cardidoïde, lemniscate de Bernoulli, cubique d'Agnesi,
Sextique de Cayley, Limaçon trisecteur, courbe de Kuipert

\begin{tikzpicture}[scale=0.7]
  \draw plot [domain=0:6.28,smooth] (\x*360/6.28:{1.7*(1+cos(\x r))}) ;
\end{tikzpicture}
\begin{tikzpicture}
  \draw plot [domain=-45:45,smooth] (\x:{sqrt(2*cos(2*\x))});
  \draw plot [domain=135:225,smooth] (\x:{sqrt(2*cos(2*\x))});
\end{tikzpicture}
\begin{tikzpicture}[scale=0.7]
  \draw plot [domain=-70:70] ({2*tan(\x)},{2*cos(\x)^2}) ;
\end{tikzpicture}

\begin{tikzpicture}[scale=0.4]
  \draw plot [smooth,domain=0:540] (\x:{1.5*(cos(\x)+3*cos(\x/3))}) ;
\end{tikzpicture}
\begin{tikzpicture}[scale=0.7]
  \draw plot [smooth,domain=0:360] (\x:{1.7*(1+2*cos(\x))}) ;
\end{tikzpicture}
\begin{tikzpicture}
  \draw plot [domain=-30:30,smooth] (\x:{1.7*cos(3*\x)^(1/3)}) ;
  \draw plot [domain=90:150,smooth] (\x:{1.7*cos(3*\x)^(1/3)}) ;
  \draw plot [domain=210:270,smooth] (\x:{1.7*cos(3*\x)^(1/3)}) ;
\end{tikzpicture}

Il existe une application de classe $\Ccal^1$ $\varphi : I\to\R$ telle
que : $T(s) = \cos\varphi(s)\vec{\imath} + \sin\varphi(s)\vec{\jmath}$.
On appelle courbure (ici algébrique) au point $s$ la quantité $\gamma =
\gamma(s) = \frac{\mathrm{d}\-\varphi}{\mathrm{d}\-s}$.

\hfill
\begin{tikzpicture}[scale=2]
  \draw plot [domain=0:1.2] (\x,{\x^2}) ;
  \draw[->] (0.3,{0.3^2})--+(1,0.3*2) ;
  \draw[dashed] (0.3,{0.3^2})--+(1,0) ;
  \draw[->] (0.3+0.7,{0.3^2}) arc(0:31:0.7cm) ;
  \draw[->] (0.7,{0.7^2})--+(1,0.7*2) ;
  \draw[dashed] (0.7,{0.7^2})--+(1,0) ;
  \draw[->] (0.7+0.7,{0.7^2}) arc(0:55:0.7cm) ;
\end{tikzpicture}
\hfill
\begin{tikzpicture}[scale=2]
  \draw plot [domain=0:1.2] (\x,{-\x^2}) ;
  \draw[->] (0.3,{-0.3^2})--+(1,-0.3*2) ;
  \draw[dashed] (0.3,{-0.3^2})--+(1,0) ;
  \draw[->] (0.3+0.7,{-0.3^2}) arc(0:-31:0.7cm) ;
  \draw[->] (0.7,{0-.7^2})--+(1,-0.7*2) ;
  \draw[dashed] (0.7,{-0.7^2})--+(1,0) ;
  \draw[->] (0.7+0.7,{-0.7^2}) arc(0:-55:0.7cm) ;
\end{tikzpicture}
\hfill~

\begin{definition}[rayon de courbure]
  \[ R = \frac1\gamma \]
\end{definition}

\begin{proposition}
  En coordonnées cartésiennes : \[ \gamma = \frac{x'y'' - x''y'}{
  \sqrt[3]{x'^2 + y'^2}} \]
  En coordonnés polaires : \[ \gamma = \frac{\rho^2 + 2\rho'^2 -
  \rho\rho'}{\sqrt[3]{\rho^2 + \rho'^2}} \]
\end{proposition}

\begin{definition}[repère de Frenet]
  On assoie au point $s$ un repère local $(M(s),T(s),N(s))$, appelé
  repère de Frenet où $T(s)$ est le vecteur tangent et $N(s)$ est le
  vecteur directement orthogonal à $T(s)$.
\end{definition}

\begin{proposition}
  \[ \left(\begin{array}{c}
        \tfrac{\dx{T}}{\dx{s}} \\
        \tfrac{\dx{N}}{\dx{s}}
    \end{array}\right) = 
    \left(\begin{array}{cc}
        0 & \gamma \\
        -\gamma & 0
    \end{array}\right)
    \left(\begin{array}{c}
        T \\
        N
    \end{array}\right)
  \]
\end{proposition}

\begin{definition}[cercle osculateur]
  Soit $C(s)$ le point défini par \[ \overrightarrow{M(s)C(s)} =
  R\overrightarrow{N(s)} \]
\end{definition}
\begin{proposition}
  La courbure détermine une courbe à un déplacement près.
\end{proposition}

\subsection{Développées de courbes}

\begin{definition}[développée]
  C'est le lieu géométrique des centres de courbures.
\end{definition}

\begin{definition}[enveloppe d'une famille de droites]
  On considère une famille de droite de $\R^2$. On appelle enveloppe de
  cette famille toute courbe $\Gamma$ de $\R^2$ telle que la tangente en
  chaque point de $\Gamma$ soit une droite de cette famille et que
  chaque droite soit une tangente de $\Gamma$.
\end{definition}

\begin{definition}[lien développée/enveloppe]
  La développée est l'enveloppe des normales.
\end{definition}

\section{Courbes gauches}
\subsection{Extension des notions définies dans $\R^2$}
On peut sans problème étendre les notions de trajectoire, de régularité,
birégulatrité, abscisse curviligne, vecteur tangent unitaire (en terme
de dérivées) aux courbes de $\R^3$.

En revanche, on ne dispose plus de la caractérisation du vecteur tangent
unitaire par les angles.
\subsection{Propriétés métriques ; repère de Frenet-Serret}
Les formules de Frenet nous permettent d'écrire $\frac{\d{T}}{\d{s}} =
\gamma N$. Comme $N$ est de norme 1, on peut désormais définir $\gamma$
ainsi.
\begin{definition}
  On appelle courbure $\gamma$ le réel
  $\lVert\frac{\dx{T}}{\dx{s}}\rVert$ ; on pose $R := \frac1\gamma$.
\end{definition}
\begin{definition}[Repère de Frenet-Serret]
  On appelle vecteur binormal noté $B$ le vecteur défini par $B=T \wedge
  N$.

  Ainsi définit $(T,N,B)$ forme un triède direct.
\end{definition}
Comme dans $\R^3$ une courbe peut se déformer dans deux directions, on
rajoute une composante de torsion :
\begin{definition}[torsion]
  On appelle torsion le scalaire défini par \[ \tau = \langle
  B,\frac{\dx{N}}{\dx{s}}\rangle \] C'est une grandeur algébrique.
\end{definition}
\begin{proposition}[Formules de Frenet-Serret]
  \[ \left(\begin{array}{c}
        \tfrac{\dx{T}}{\dx{s}} \\
        \tfrac{\dx{N}}{\dx{s}} \\
        \tfrac{\dx{B}}{\dx{s}}
    \end{array}\right) = 
    \left(\begin{array}{ccc}
        0 & \gamma & 0 \\
        -\gamma & 0 & \tau \\
        0 & -\tau & 0
    \end{array}\right)
    \left(\begin{array}{c}
        T \\
        N \\
        B
    \end{array}\right)
  \]
\end{proposition}

\begin{definition}[plan osculateur]
  C'est le plan passant par $M(s)$ et dirigé par $f'(s)$ et $f''(s)$.
\end{definition}

\subsection{Exemple : cas de l'hélice à pas constant}

On peut en donner la paramétrisation suivante :
\[\begin{cases}
  x(t) = r\cos(t) \\
  y(t) = r\sin(t) \\
  z(t) = ht
\end{cases}\]

\begin{center}
  \tdplotsetmaincoords{60}{130}
  \begin{tikzpicture}[tdplot_main_coords]
    \draw [->] (0,0,0) -- (2,0,0) ;
    \draw [->] (0,0,0) -- (0,2,0) ;
    \draw [->] (0,0,0) -- (0,0,2) ;
    \draw plot [domain = 0:1440,smooth,samples=1000] ({cos(\x)},{sin(\x)},0.0025*\x) ;
  \end{tikzpicture}
\end{center}

\end{document}

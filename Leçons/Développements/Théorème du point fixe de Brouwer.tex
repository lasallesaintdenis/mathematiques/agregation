\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème du point fixe de Brouwer}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Toute application continue de la boule unité fermée de $\R^n$ dans
  elle-même  admet un point fixe.
\end{theoreme}

\section*{Preuve}

Éléments de la preuve :
\begin{itemize}
  \item On démontre le théorème dans le cas $n = 1$ et on en déduit qu'un
    segment n'est pas homéomorphe à un cercle.
  \item Dans le cas où $n \geqslant 2$, on procède par l'absurde en
    supposant qu'il n'y a pas de point fixe et on montre qu'il existe une
    rétraction de la sphère sur la boule, c'est à dire une application
    continue $G \colon \B \to \S$ telle que $G|_{\S} = \Id_{\S}$
  \item Dans le cas $n = 2$, on peut utiliser une telle rétractaction pour
    construire deux lacets homotopes d'indices différents, ce qui est
    absurde.
\end{itemize}


\begin{proof}
  Soit $F$ une fonction continue de $[a,b]$ dans $[a,b]$. On a
  nécessairement $F(a) \geqslant a$ et $F(b) \leqslant b$ et si on considère
  la fonction $\varphi$ d'expression sur $[a,b], \varphi(x) = F(x) - x$, on
  a $\varphi(a) \geqslant 0$ et $\varphi(b) \leqslant 0$ et elle s'annule au
  moins une fois d'après le théorème des valeurs intermédiaires.

  Si à la place du segment $[a,b]$, on considère un cercle la proposition
  n'est plus vraie. En effet, pour les rotations qui laissent invariante le
  cercle dans son ensemble, la seule qui laisse chaque point invariant est
  l'identité.
\end{proof}

On suppose par l'absurde qu'il existe $F \colon \B^n \to \B^n$ continue
sans point fixe.

\begin{proof}
  On suppose donc que pour tout $x\in \B, F(x) \neq x$. Le point
  d'intersection de la sphère unité avec la demi-droite issue de $F(x)$
  passant par $x$ s'obtient en résolvant le système \[ G(x) - F(x) = \lambda
  (x - F(x)) \quad \text{et} \quad \lVert G(x) \rVert^2 = 1 . \] d'inconnue
  $\lambda > 0$ (fonction de $x$). On peut réécrire cette égalité sous la
  forme \[ \lVert \lambda (x - F(x)) + F(x) \rVert^2 - 1 = , \] qui peut se
  développer en une équation $P_x(\lambda)$ avec \[ P_x(\lambda) = \lambda^2
    \lVert x - F(x) \rVert^2 + 2 \lambda (x - F(x)) \cdot F(x) + \lVert F(x)
    \rVert^2 - 1 .\] On a \[ P_x(0) = \lVert F(x) \rVert^2 - 1 \leqslant 0,
    \quad P_x(1) = \lVert x \rVert^2 - 1 \leqslant 0 \quad \text{et} \quad
  P_x(\lambda) \xrightarrow[\lambda \pm \infty]{} +\infty.\] Cette équation
  admet donc une racine $\lambda \leqslant 0$ et une racine $\lambda
  \geqslant 1$. Le discriminant \[ \Delta(x) = ((x - F(x) \cdot F(x))^2 +
  \lVert x - F(x)\rVert^2 \left( 1 - \lVert F(x) \rVert^2 \right) \] est
  strictment positif pour tout $x \in \B$, puisque $P_x$ possède deux
  racines distinctes. La racine positive est \[ \lambda(x) = \frac{ -(x -
  F(x)) \cdot F(x) + \sqrt{\Delta(x)}}{ \lVert x - F(x) \rVert^2} . \] De
  plus, si $x \in \S, P_x(1) = 0$ et donc $\lambda(x) = 1$ dans ce cas.
  La fonction $x \mapsto \lambda(x)$ et donc $x \mapsto G(x)$ sont
  continues sur $\B$ et $G(x) = x$ si $x \in \S$.
\end{proof}

\begin{wrapfigure}[10]{l}{5cm}
  \begin{tikzpicture}
    \draw[name path=c] plot [domain = 0:360,smooth] ({2*cos(\x)},{2*sin(\x)});
    \draw (0,0) node [circle,draw,fill,inner sep = 0.7pt] {} ;
    \draw (0,0) node [below left] {$O$} ;
    \node (a) at (50:2.9) {};
    \node (b) at (160:1.3) {} ;
    \draw (b) node [circle,draw,fill,inner sep = 0.7pt] {} ;
    \draw (b) node [below] {$F(x)$} ;
    \draw [name path=l] (a) -- (b) ;
    \path [name intersections={of=l and c,by=e}] ;
    \draw (e) node [circle,draw,fill,inner sep = 0.7pt] {} ;
    \draw (e) node [above] {$G(x)$} ;
    \node (cc) at (e) [circle through=(a)] {};
    \coordinate (f) at (intersection 1 of cc and a--b) ;
    %\path [name intersections={of=cc and l,by=f}];
    \draw (f) node [circle,draw,fill,inner sep = 0.7pt] {} ;
    \draw (f) node [below] {$x$} ;
  \end{tikzpicture}
\end{wrapfigure}

La figure ci-contre illustre la construction dans le cas où $x \neq F(x)$ et
$G(x) \in \S$ et avec $G(x) \in \S$.

Le dessin illustre aussi le caractère continu de la déformation qui permet
d'avoir $G(x) = x$ si $x \in \S$.

On se place dans le cas $n = 2$ et on identifie $\R^2$ à $\C$. On cherche
maintenant à construire deux lacets homotopes d'indices différents, ce qui
fera apparaître une contradiction.

\begin{proof}
  On pose $x = (s \cos 2\pi t, s \sin 2\pi t)$. Pour $s$ fixé, lorsque $t$
  varie de 0 à 1, son image $G(x) = \gamma_s(t)$ parcourt un chemin continue
  fermé (lacet) contenu dans le cercle $\S$. En particulier $\gamma_0(t) =
  G(0)$ est le lacet réduit au point $G(0)$ et \[ \gamma_1(t) = G( \cos
  2\pi t , \sin 2\pi t) = ( \cos 2\pi t , \sin 2\pi t) \] est le cercle
  unité $\S$ parcouru dans le sens direct. Leurs indices (nombre de tours
    autour de ) sont \[ \frac{1}{2i\pi} \int_{\gamma_0} \frac{\diff z}z = 0
    \quad \text{et} \quad \frac{1}{2i\pi} \int_{\gamma_1} \frac{\diff z}z =
  1 . \]

  Or l'application $(s,t) \gamma_s(t)$ est continue de $[0,1] \times [0,1]$
  à valeurs dans le cercle unité et déforme continûment $\gamma_0$ en
  $\gamma_1$ sans rencontrer l'origine. Les deux indices devraient donc être
  égaux.

  Il ne peut donc pas exister de rétraction du disque $\B$ sur le cercle
  $\S$, d'où le théorème de Brouwer pour $n = 2$.
\end{proof}

\end{document}

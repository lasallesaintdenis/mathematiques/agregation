// Illustration de la règle de classification

r=0.25; 

// Test de la règle :

[X,Y]=point_alea();
Y_hist=classif_histo(X,Xi,Yi,r); // calcul de Y selon règle de l'histogramme

if(Y_hist==1)
	plot(X(1),X(2),'+r') // croix rouge
else
	plot(X(1),X(2),'+b') // ou bleu, selon résultat de la règle
end

// dessin du carré A(X)
	x1min=floor(X(1)/r)*r;
	x1max=x1min+r;
	x2min=floor(X(2)/r)*r;
	x2max=x2min+r;
plot([x1min,x1max,x1max,x1min,x1min],[x2max,x2max,x2min,x2min,x2max],'-k')




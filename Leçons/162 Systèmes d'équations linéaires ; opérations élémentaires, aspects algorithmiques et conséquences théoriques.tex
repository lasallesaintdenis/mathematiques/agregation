\documentclass[a4paper,12pt,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{ntheorem}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{9 octobre 2013}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother


\newcommand{\K}{\mathbf{K}}
\newcommand{\ssi}{si, et seulement si }

\begin{document}
\maketitle

On suppose $\K$ un corps commutatif.

\section{Définitions, généralités}
\subsection{Généralités}

\begin{definition}
  Système d'équation, matrice associée, solutions du système, rang du
  système, systèmes équivalents
\end{definition}

Dans la suite, on note $A$ une matrice de $\mathcal{M}_{n,p}(\K)$ et $x$
et $b$ deux vecteurs de $\mathcal{M}_{1,p}(\K)$.

\begin{proposition}
  L'ensemble des solutions est vide si $b\notin ImA$, un sous espace
  affine de $\K$ de la dimension et de la direction de $\ker A$ sinon.
\end{proposition}

Parmi les applications : résolutions de problèmes aux éléments finis,
recherche de base d'un s.e.v., inversion de matrices.

\subsection{Systèmes particuliers}

Systèmes de Cramer : On suppose ici que $A$ est carrée.

\begin{definition}
  Le système associé à $A$ est dit \emph{de Cramer} \ssi $A$ est
  inversible.
\end{definition}

On pose $c_j = (a_{i,j})_{1\leq i \leq n}$, vecteurs colonnes de $A$.

\begin{proposition}
  \emph{Formules de Cramer :} 
  \[
    x_i = \frac{\det(c_1,\ldots,c_{i-1},b,c_{i+1},\ldots,c_n)}{\det A}
  \]
\end{proposition}

\emph{Remarque :} Le coût est en $O(n\times n!)$

Systèmes triangulaires : Ici on suppose que $A$ est inversible et on
suppose de plus qu'elle est triangulaire supérieure. Dans ce cas, la
connaissance de $a_nx_n=b_n$ donne rapidement la connaissance de $x_n$
puis celle de $a_{n-1,n-1}x_{n-1} + a_{n-1,n}x_n = b_{n-1}$ etc.

La complexité d'un tel calcul est $O(n^2)$.

Systèmes échelonnés : Un système est dit échelonnés si le lignes
commencent par un nombre strictcement plus grand de zéro que la ligne
précédente.\\
Plus précisément, il existe une famille $(j_k)$ telle que :
\begin{enumerate}
  \item $0\leq j_1<\cdots<j_r\leq n$
  \item $\forall i \leq n , \forall j \leq j_r , a_{i,j} = 0 , \forall
    j_r < j \leq n, a_{i,j}\neq 0$
\end{enumerate}

Les inconnues $x_{j_1},\dots,x_{j_r}$ sont dites principales, les autres
sont dites secondaires.
\begin{theoreme} (Rouché-Fontené)
  Si $\exists i>r / b_i \neq 0$ le système n'admet pas de solutions,
  sinon, celui-ci est de dimension $n-r$.
\end{theoreme}

\section{Méthodes directes}

\subsection{Matrices élémentaires}

On appelle matrices de dilatation les matrices qui permettent de
«normaliser» le premier coeffiecient non nul d'une ligne.

On appelle matrice de transvection les matrices qui permettent
d'additionner (ou soustraire) deux lignes.

On appelle matrices de permutation les matrices qui permettent
d'échanger deux lignes.

\emph{Application :} les dilatations-transvections engendrent $GL_n(\K)$
et les transvections engendrent $\mathcal{S}^+(\K)$

\subsection{Pivot de Gauss}

Dans le cas du pivot de Gauss, on transforme le système en un système
équivalent par échange de ligne avec $a_{1,1} \neq 0$. Il constituera le
premier pivot, qui permet d'éliminer les autres coefficients $a_{i,1}$.

On réitère le procédé sur la matrices $A_1=(a_{i,j})_{2\leq i,j\leq n}$

On obtient ainsi une matrice triangulaire supérieure si le système
comporte une unique solution ou une matrice échelonnée si le système
possède 0 ou plusieures solutions.

Le coût de cette décomposition est en $O(n^3)$

\subsection{Factorisation LU}

\begin{definition}
  Pour $A = (a_{ij})$, on appelle mineurs principaux d'ordre $k$ le
  déterminant de $A_k = (a_{ij})_{1\leq i,j \leq k}$
\end{definition}

Si tous les mineurs d'ordre $k$ sont non-nuls, on peut trouver deux
matrices $L$ et $U$, $L$ triangulaire inférieure, $U$ triangulaire
supérieure, telle que $A=LU$

Dans les cas d'une matrice tri-diagonale, le côut de la décomposition
$LU$ est en $O(n)$.

\subsection{Décomposition de Choleski}

\begin{proposition}
  Si $A$ est symétrique définie positive, on peut trouver $L$
  triangulaire inférieure telle que $A=L\vphantom{L}^tL$
\end{proposition}

De plus, dans la décomposition précédente, on a :
\begin{itemize}
  \item $\displaystyle{i\neq k, l_{ik} = \frac{a_{ik} - \sum_{j=1}^{k-1}
    l_{ij}l_{kj}}{l_{kk}}}$
  \item $\displaystyle{\sqrt{a_{kk}-\sum{l_{kj}^2}}}$
\end{itemize}

\subsection{Factorisation QR}

\begin{definition}
  On appelle matrice de Householder les matrices de la forme $H(v) = I -
  2\displaystyle\frac{vv^*}{v^*v}$, où $v$ est un vecteur non nul de
  $\mathbf{C}$
\end{definition}

\begin{proposition}
  Étant donnée une matrice $A$, il exsite deux matrices $Q$ et $R$, $Q$
  unitaire et $R$ triangulaire supérieure telles que $A=QR$
\end{proposition}

\section{Méthodes itératives}

On suppose que $A$ est inversible et qu'il exsite $M$ et $N$ telles que
$A=M-N$ et $M$ est «facile à inverser».

$Ax=b \Leftrightarrow Mx = Nx + b \Leftrightarrow x = M^{-1}(Nx+b)$

Cette dernière égalité est de la forme $x_{n+1} = Bx_n+c$

Ici, on aura plus précisément $x_{n+1} = M^{-1}Nx_n+M^{-1}b$, avec
$M^{-1}N = I - M^{-1}A$.

\subsection{Méthode de Jacobi}

On pose ici $A = D - (E + F)$ où $D$ est la matrice diagonale, et $-E$ et
$-F$ des éléments de la «surdiagonale» et de la «sousdiagonale».

On cherche à résoudre $Dx_{k+1}=(E+F)x_k+b$

\subsection{Méthode de Gauss-Seidel}

On pose ici $A = (D - E) - F$ où $D$ est la matrice diagonale, et $-E$ et
$-F$ des éléments de la «surdiagonale» et de la «sousdiagonale».

On cherche désormais à résoudre $(D-E)x_{k+1}=Fx_k+b$.

\subsection{Méthode de relaxation}

Si la méthode précédent converge, on peut introduire un paramète
$\omega$, et écrire que $A = \left(\frac{D}{\omega} -E\right) -
\left(\frac{1-\omega}{\omega}D+F\right)$

On a donc que $\left(\frac{D}{\omega} -E\right)x_{k+1} =
\left(\frac{1-\omega}{\omega}D+F\right)x_k+b$

\subsection{Méthode du gradient}

On suppose ici que $A$ est une matrice carrée de «grande dimension»,
mais très creuse. De telles matrices sont courantes dans les problèmes
issus de la physique.

\begin{proposition}
  Soit $x_0$ un élément de $\mathbb{R}^n$ et $V$ un s.e.v. de
  $\mathbb{R}^n$, $f:\mathbb{R}^n \rightarrow \mathbb{R}$ qui admet un
  minimum local en $\overline{x}$, alors $\nabla f(\overline x)$ est
  orthogonal à $V$
\end{proposition}

Pour une matrice symétrique définie positive, la solution $x$ du système
$Ax=b$ est aussi l'unique minimum de $f:x\mapsto \frac12 ((Ax|x) -
(b|x))$.

On peut donc construire une suite $(x_n)$ définie par :
\[
  \left\{\begin{array}{l}
      x_{n+1}=x_n+a_{n+1}r_n\\
      r_n=-\nabla f(x_n) = b-Ax_n\\
      a_{n+1} = \frac{\left||r_n\right||^2}{(Ar_n|r_n)}
      \end{array}\right.
    \]
\begin{definition}
  Pour une matrice inversible, on pose $cond(A) =
  \left||A\right||\left|\left|A^{-1}\right|\right| $
\end{definition}

Pour $cond(A)$ proche de 1, le système sera peu soumis aux variations
des conditions initiales.

\end{document}

\section*{Développements}
\begin{itemize}
  \item Factorisation QR
  \item Méthode du gradient
\end{itemize}

\end{document}

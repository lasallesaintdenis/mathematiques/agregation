\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\newcommand{\Rel}{\mathcal{R}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\GrpPerm}{\mathfrak{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Simplicité de $\A_5$}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{definition}
  On appelle \emph{groupe alterné} et on note $\A_n$ le sous-groupe de
  $\GrpPerm_n$ constitué des permutations paires.
\end{definition}

\begin{proposition}
  Si $n \geqslant 3$, les cycles de longueur 3 engendrent $\A_n$.
\end{proposition}
\begin{proof}
  Tout élément de $\GrpPerm_n$ peut s'écrire comme un produit de
  transpositions et une transposition est de signature -1, $\A_n$ est
  l'ensemble des produits pairs de transpositions.

  Appellons $\A'_n$ le sous-groupe de $\GrpPerm_n$ engendré par les cycles
  de longueur 3. On a $\A'_n \subset \A_n$ car un cycle de longueur 3 est de
  signature 1. Il faut encore montrer l'inclusion réciproque. Or,
  \begin{itemize}
    \item si $i,j,k,l$ sont deux à deux distincts, $(i,j)(k,l) =
      (i,j,k)(j,k,l)$ ;
    \item si $i,j,k$ sont deux à deux distincts, $(i,j)(i,k) = (i,k,j)$ ;
    \item si $i \neq j, (i,j)(j,i) = \Id$.
  \end{itemize}
\end{proof}

\begin{proposition}
  Soit $p \geqslant 5$ un nombre premier. Si $H$ est un sous-groupe du
  groupe symétrique $\GrpPerm_p$ tel que $[\GrpPerm_p : H ] \leqslant p -
  1$, alors $[\GrpPerm_p : H ] \in \{ 1, 2 \}$.
\end{proposition}
\begin{proof}
  Montrons que $H$ contient tous les cycles de longueur $p$. Soit $\gamma
  \in \GrpPerm_p$ un cycle de longueur $p$. Pour tout entier $i$, l'ensemble
  $\gamma^i H$ est de cardinal $\card H$. Comme $H$ est d'indice au plus $p
  - 1$ dans $\GrpPerm_p$, les ensembles $H, \gamma H, \dots, \gamma^{p - 1}
  H$ ne peuvent pas être deux à deux disjoints (sinon, $\card \GrpPerm_p
  \geqslant \sum_{i=0}^{p-1} \card \gamma^i H = p\card H$). Donc il existe
  deux entiers $i$ et $j, 0 \leqslant i < j \leqslant p - 1$ tels que
  $\gamma^i H \cap \gamma^j H \neq \varnothing$. On en déduit que $\gamma^{j
  -i } \in H$. Or $1 \leqslant j -i < p$ donc $\gamma^{j -i }$ engendre le
  sous-groupe $\langle \gamma \rangle$ d'ordre $p$, ce qui entraîne $\gamma
  \in \langle \gamma^{j -i } \rangle \subset H$.

  Montrons que $H$ contient tous les cycles d'ordre 3. Comme $p > 3$, et
  qu'on vient de montrer que $H$ contient les cycles d'ordre $p$, il suffit
  de remarquer qu'un cycle d'ordre 3 s'écrit \[ (i,j,k) = (k,j,i,a_1,a_2,
  \dots, a_{p - 3})(i,k,j,a_{p - 3}, \dots, a_2, a_1) \in H . \]

  Comme $H$ contient tous les cycles de longueur 3, on $\A_p \subset H$ et
  donc $\card H \geqslant \card \A_p = \frac{1}2 \card \GrpPerm_p$, d'où $ [
  \GrpPerm_p : H ] \in \{ 1 , 2 \}$.
\end{proof}

\begin{remarque}
  Avec $p = 5$, on a un contre-exemple au fait que si $H$ est un sous-groupe
  de $G$, alors l'ordre de $H$ divise celui de $G$. En effet, bien que 30 ou
  40 divise $120 = \card \GrpPerm_5$, il n'existe pas de sous-groupe de
  $\GrpPerm_5$ d'ordre 30 ou 40.
\end{remarque}

\end{document}

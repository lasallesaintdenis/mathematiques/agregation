\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Décomposition de Dunford}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{proposition}
  Soit $f \in \L(E)$ et $F \in \K[X]$ un polynôme annulateur de $f$.
  Soit $F = \beta M_1^{\alpha_1} \dots M_s^{\alpha_s}$ la décomposition
  en facteurs irréductibles de $\K[X]$ du polynôme $F$. Pour tout $i \in
  \llbracket 1,s \rrbracket$, on note $N_i = \ker M_i^{\alpha_i}(f)$. On
  a alors $E = N_1 \oplus \cdots \oplus N_s$ et pour tout $i \in
  \llbracket 1,s \rrbracket$, la projection sur $N_i$ par rapport à
  $\oplus_{j\neq i}N_j$ est un polynôme en $f$.
\end{proposition}

\begin{theoreme}
  Soit $f \in \L(E)$ tel que son polynôme caractéristique soit scindé
  sur $\K$. Il existe un unique couple $(d,n)$ d'endomorphismes tels que
  \begin{enumerate}[label=(\roman*)]
    \item $d$ est diagnalisable et $n$ nilpotente
    \item $f = d +n$
    \item $d\circ n = n \circ d$
  \end{enumerate}
  De plus, $d$ et $n$ sont des polynômes en $f$.
\end{theoreme}

\section*{Preuve}

\subsection*{Proposition 1}
On a, d'après le lemme des noyaux, $E = N_1 \oplus \cdots \oplus N_s$.
Pour tout $i \in \llbracket 1,s \rrbracket$, notons $Q_i = \prod_{j\neq
i} M_i^{\alpha_i}$. Aucun facteur n'est commun à tous les $Q_i$, on peut
appliquer l'égalité de Bezout , on voit qu'il existe $U_1,\dots,U_s \in
\K[X]$ tels que $U_1 Q_1 + \cdots + U_s Q_s = 1$, de sorte que \[ \Id_E
= U_1(f) \circ Q_1(f) + \cdots + U_s(f) \circ Q_s(f).\] Pour tout $i \in
\llbracket 1,s \rrbracket$, on pose $P_i = U_iQ_i $ et $p_i = P_i(f)$.
On a \[ \Id = \sum_{i=1}^s p_i .\] Par ailleurs, pour tout $i \neq j$,
$F$ divise $Q_iQ_j$ donc \begin{align*}\forall j \neq i, p_i \circ p_j &
= U_iQi(f) \circ U_jQ_j(f) \\ & = U_iU_j(f) \circ Q_iQ_j(f) \\ & = 0.
\end{align*} On déduit de ces deux égalités que pour tout $i \in
\llbracket 1,s \rrbracket, p_i = \sum_{j = 1}^s p_i \circ p_j$ et que
$p_i = p_i^2$. Les $p_i$ sont donc des projecteurs.

Montrons que, pour tout $i \in \llbracket 1,s \rrbracket, \im(p_i) =
N_i$.

Soit $y = p_i(x) \in \im(p_i)$. On a \begin{align*} M_i^{\alpha_i}(y) &
= M_i^{\alpha_i}(f) \circ P_i(f)(x) \\ & = M_i^{\alpha_i}(f) \circ
U_iQ_i(f)(x) \\ & = U_i(f) \circ F(f)(x) \\ & = 0\end{align*} ce qui
prouve que $\im(p_i) \subset \ker M_i^{\alpha_i}(f) = N_i$

Montrons l'inclusion réciproque. Soit $x \in N_i = \ker
M_i^{\alpha_i}(f)$. D'après ce qu'on a vu, $x = p_1(x) + \cdots +
p_s(x)$. Or, pour tout $j \neq i, p_j(x) = U_J(f) \circ Q_j(f)(x) = 0$
car $M_i^{\alpha_i}$ divise $Q_j$, donc finalement $x = p_i(x) \in
\im(p_i)$. Donc $\im(p_i) = N_i$.

Il ne reste plus qu'à montrer que pour tout $i \in \llbracket 1,s
\rrbracket$ \[ \ker(p_i) = \oplus_{j\neq i}N_j .\] Pour tout $j \neq i$,
on a $N_j \subset \ker(p_i)$ car si $x \in N_j$, alors \[ p_i(x) =
U_i(f) \circ Q_i(f)(x) = 0 \] car $M_i^{\alpha_i}$ divise $Q_i$. On en
déduit que $\oplus_{j\neq i}N_j \subset \ker(p_i)$.

Soit maintenant $x \in \ker(p_i)$. D'après ce qui précède, $x = \sum_{j
\neq i} p_j(x)$ donc $x \in \oplus_{j\neq i}N_j$. Finalement, $\ker(p_i)
= \oplus_{j\neq i}N_j$. \qed

\subsection*{Théorème 2}

Écrivons $\chi_f(X) = (-1)^n \prod_{i=1}^s (X - \lambda_i)^{\alpha_i}$
et pour tout $i \in \llbracket 1,s \rrbracket$, notons $N_i = \ker(f -
\lambda_i)^{\alpha_i}$. On applique la proposition précédente pour $F =
\chi_f$ et pour tout $i \in \llbracket 1,s \rrbracket$, $M_i = (f -
\lambda_i)^{\alpha_i}$. En utilisant les notations précédentes, pour
tout $i \in \llbracket 1,s \rrbracket$, $p_i = P_i(f)$ est le
projecteur sur $N_i$ parallèlement à $\oplus_{j\neq i}N_j$. Posons $d =
\sum_{i=1}^s \lambda_i p_i$ qui est diagonalisable car $p_i \circ p_j =
p_j \circ p_i$ et $p_i$ est diagonalisable, pour tout $i,j \in
\llbracket 1,s \rrbracket$. Ainsi, les $p_i$ sont codiagnalisables.
Posons $n = f - d$. On sait que pour tout $q \in \N$, $n^q =
\sum_{i=1}^s(f - \lambda_i \Id)^ap_i$ (par récurrence sur $q$).

Posons $q = \sup_{i \in \llbracket 1,s \rrbracket}\alpha_i$. On a donc
\[ n^q = \sum_{i=1}^s (f - \lambda_i)^qp_i = \sum_{i=1}^s (( X -
  \lambda_i)^qP_i)(f). \] Or \[ \chi_f = \prod_{i=1}^s (X - \lambda_i)
  ^{\alpha_i} = (X - \lambda_i)^{\alpha_i} \prod_{j \neq i} (X -
\lambda_j)^{\alpha_j} = (X - \lambda_i)^{\alpha_i} Q_i.\] On a donc
$U_i\chi_f = (X - \lambda_i)^{\alpha_i} P_i$. Ainsi, $\chi_f \mid (X -
\lambda_i)^{\alpha_i}P_i \mid (X - \lambda_i)^q P_i$. Ainsi, $n^q = 0$.


Supposons qu'il existe $(d',n')$ un autre couple d'endomorphismes
vérifiant les conditions du théorème. On a donc \[ f = d + n = d' + n'.
\] $d'$ commute avec $d'$ et avec $n'$ donc $d'$ commute avec $f$ donc
$d'$ commute avec $d$. De même, $n'$ commute avec $n$. Donc $d$ et $d'$
sont codiagnalisables. Ainsi, $d - d'$ est diagonalisable et $n - n'$
est nilpotente. Donc \[ d - d' = n' - n = 0 .\] \qed

\end{document}

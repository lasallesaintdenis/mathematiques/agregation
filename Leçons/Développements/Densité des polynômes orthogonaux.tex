\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Densité des polynômes orthogonaux}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}[Principe du prolongement analytique]
Soit $U$ un ouvert connexe. Si deux fonctions analytiques coïncident sur
un sous-ensemble $D \subset U$ ayant un point d'accumulation dans $U$,
alors sont égales sur $U$.
\end{theoreme}

\begin{theoreme}[Intégrale dépendant d'un paramètre holomorphe]
  Soit $(X,\T,\mu)$ un espace mesuré et $U$ un ouvert de $\C$ et $f
  \colon U \times X \to \C$. Posons \[ \forall z \in \C, F(z) = \int_X
  f(z,x) \diff \mu(x) .\] Supposons que
  \begin{enumerate}
    \item pour tout $z \in U$, l'application $x \mapsto f(z,x)$ est
      mesurable ;
    \item il existe une partie $N \subset X$ de mesure nulle telle que
      pour tout $x \notin N$, l'application $z \mapsto f(z,x)$ est
      holomorphe ;
    \item pour tout compact $K \subset U$, il existe $g \in L^1(X)$
      (indépendante de $z$) telle que $\lvert f(z,x) \rvert \leqslant
      g(x)$ pour tout $x \notin N$ et pour tout $z \in K$.
  \end{enumerate}
  Alors $F$ est holomorphe sur $U$ et pour tout $z \in U$ et pour tout
  $n \in \N$, \[ F^{(n)}(z) = \int_X \frac{\partial^n f}{\partial
  z^n}(z,x) \diff \mu(x) .\]
\end{theoreme}

\begin{definition}
  Soit $H$ un espace vectoriel sur le corps $\K = \R$ ou $\C$. L'espace
  $H$ est dit \emph{préhilbertien} s'il est muni d'un produit scalaire
  (produit scalaire hermitien si $\K = \C$). Ainsi, un espace
  préhilbertien est un couple $(H, \langle \cdot, \cdot \rangle)$ où $H$
  est un $\K$-espace vectoriel et $\langle \cdot, \cdot \rangle$ un
  produit scalaire sur $H$. Si un espace préhilbertien est complet pour
  la norme $\lVert \cdot \rVert$ associée au produit scalaire, on dit
  que c'est un \emph{espace de Hilbert}.
\end{definition}

\begin{definition}
  Soit $(H, \langle \cdot, \cdot \rangle)$ un espace de Hilbert. On dit
  qu'une famille $(e_i)_{i\in I}$ est une base hilbertienne si elle est
  : \begin{enumerate}
    \item orthogonale : $\langle e_i , e_j \rangle = 0$ pour tout $i,j
      \in I, i \neq j$ ;
    \item normée : $\langle e_i , e_i \rangle = 1$ pour tout $i \in I$ ;
    \item totale : $H = \overline{\Vect(e_i, i \in I)}$.
  \end{enumerate}
\end{definition}

\begin{definition}
  Soit $(E,d)$ un espace métrique. On dit que $(E,d)$ est
  \emph{séparable} si tout ouvert non vide de $E$ contient au moins un
  point d'une partie dénombrable de $E$.
\end{definition}

\begin{theoreme}
  Soit $(H, \langle \cdot, \cdot \rangle)$ un espace de Hilbert
  séparable et $(e_n)_{n\in\N}$ une famille orthonormée de $H$. Les
  propriétés suivantes sont équivalentes.
  \begin{enumerate}
    \item La famille orthonormée $(e_n)_{n\in\N}$ est une base
      hilbertienne.
    \item Pour tout $x \in H$, \[ x = \sum_{n=0}^{+\infty} \langle x,
        e_n \rangle e_n ,\] ce qui signifie que \[ \lim_{N\to+\infty}
        \left \lVert x - \sum_{n=0}^N \langle x, e_n \rangle e_n \right
      \rVert = 0 . \]
    \item Pour tout $x \in H$, \[ \lVert X \rVert^2 =
      \sum_{n=0}^{+\infty} \lvert \langle x, e_n \rangle \rvert^2 . \]
    \item On a \[ (e_n,n\in\N)^{\perp} = \{ 0 \} . \]
  \end{enumerate}
  De plus, l'application \[ \begin{array}{lcr}
    \Delta : H & \to & l^2(\N) \\ x & \mapsto & (\langle x, e_n
  \rangle)_{n\in\N} \end{array} \] est bien définie et réalise une
  isométrie surjective de $H$ sur $l^2(\N)$.
\end{theoreme}

\begin{definition}
  Soit $I$ un intervalle de $\R$. On appelle \emph{fonction poids} toute
  fonction $\rho \colon I \to \R$ mesurable, strictement positive  et
  telle que \[ \forall n \in \N, \int_I \lvert x \rvert^n \rho(x) \diff
  x < + \infty .\]
\end{definition}

On note $L^2(I,\rho)$ l'espace des fonctions de carré intégrable pour la
mesure de densité $\rho$ par rapport à la mesure de Lebesgue,
c'est-à-dire munie du produit scalaire \[ \langle f, g \rangle = \int_I
f(x) \overline{g(x)} \rho(x) \diff x . \] L'espace $L^2(I,\rho)$ est un
espace de Hilbert.

\begin{definition}
  Il existe une unique famille $(P_n)_{n\in \N}$ de polynômes unitaires
  orthogonaux deux à deux tels que $\deg(P_n) = n$. Cette famille
  s'appelle la famille des \emph{polynômes orthogonaux} associés ) la
  fonction $\rho$.
\end{definition}

\begin{theoreme}[Base hilbertienne de polynômes orthogonaux]
  Soit $I$ un intervalle de $\R$ et $\rho$ une fonction poids. S'il
  existe $\alpha > 0$ tel que : \[ \int_I e^{\alpha \lvert x \rvert}
    \rho(x) \diff x < + \infty \] alors la famille de polynômes $\left(
  \frac{P_n}{\lVert P_n \rVert_{\rho}} \right)_{n\in \N}$ forme une base
  hilbertienne de $L^2(I,\rho)$ pour la norme $\lVert \cdot
  \rVert_{\rho}$.
\end{theoreme}

\section*{Preuve}

\subsection*{Théorème 8}

Par définition, $\left(\frac{P_n}{\lVert P_n \rVert_{\rho}}
\right)_{n\in \N}$ est une famille orthonormée.

On veut montrer que $\left\{ \frac{P_n}{\lVert P_n \rVert_{\rho}} , n
  \in \N \right\}^{\perp} = \{ x \mapsto x^n, n \in \N \}^{\perp} = \{ 0
\}$. On sait déjà, par définition du poids que $x \mapsto x^n \in
L^2(I,\rho)$.

Soit $f \in L^2(I,\rho)$. On va utiliser la fonction auxiliaire $\phi$
définie par \[ \phi(x) = \begin{cases} f(x)\rho(x) \text{ si } x \in I
\\ 0 \text{ sinon}\end{cases} . \]

Montrons que $\phi \in L^1(\R)$. On a, pour tout $t \geqslant 0, t
\leqslant \frac{1}2(1 + t^2)$. Ainsi, on obtient l'égalité \[ \forall x
  \in I, \lvert f(x) \rvert \rho(x) \leqslant \frac{1}2(1 + \lvert
f(x) \rvert^2) \rho(x). \] Comme $\rho$ et $\rho f^2$ sont intégrables
sur $I$, on en déduit que $\phi \in L^1(\R)$. On peut donc considérer sa
transformée de Fourier, c'est-à-dire pour $\omega \in \R$, \[
\hat{\phi}(\omega) = \int_I f(x) e^{-i\omega x}\rho(x) \diff x . \]
Montrons que $\hat{\phi}$ se prolonge en une fonction $F$ holomorphe sur
\[ \B_{\alpha} = \left\{ z \in \C \mid \Im(z) < \frac{\alpha}2 \right\} .
\]
\begin{center}
  \begin{tikzpicture}
    \path [fill,gray!25!white] (-3.4,-1.2) rectangle (3.4,1.2) ;
    \draw (-3.4,0) -- (3.4,0) ;
    \draw (0,-2.2) -- (0,2.2) ;
    \draw [very thick] (-3.4,-1.2) -- (3.4,-1.2) ;
    \draw [very thick] (-3.4,1.2) -- (3.4,1.2) ;
    \node at (0,1.2) [above right] { \scriptsize $ \frac{\alpha}2 $ } ;
    \node at (0,1.2) [below left] { \scriptsize $ \B_{\alpha} $ } ;
    \node at (0,-1.2) [below right] { \scriptsize $ -\frac{\alpha}2 $ } ;
  \end{tikzpicture}
\end{center}
Posons, maintenant, \[ g(z,x) = e^{-izx} f(x) \rho(x). \] Soit $z \in
\B_{\alpha}$. On a \[ \int_I \lvert g(z,x) \rvert \diff x \leqslant
  \int_I e^{\frac{\alpha \lvert x \rvert}{2}} \lvert f(x) \rvert \rho(x)
  \diff x . \] En utilisant l'inégalité de Cauchy-Scwartz, on obtient \[
  \int_I e^{\frac{\alpha \lvert x \rvert}{2}} \lvert f(x) \rvert \rho(x)
  \diff x \leqslant \left(  \int_I e^{\alpha \lvert x \rvert} \rho(x)
  \diff x \right)^{\frac{1}2} \left(  \int_I \lvert f(x) \rvert^2
\rho(x) \diff x \right)^{\frac{1}2} < + \infty .\] On définit la
fonction $F$ par : \[ \forall z \in \B_{\alpha}, F(z) = \int_I e^{-izx}
f(x) \rho(x) \diff x = \int_I g(z,x) \diff x . \] Cette fonction est
bien définie d'après l'inégalité ci-dessus. On veut maintenant montrer
qu'elle satisfait les hypothèses d'holomorphie sous le signe intégrale.

\begin{itemize}
  \item Pour tout $z \in \B_{\alpha}$, l'application $z \mapsto g(z,x)$
    est mesurable sur $I$.
  \item Pour preque tout $x \in I$, l'application $z \mapsto g(z,x)$ est
    holomorphe sur $\B_{\alpha}$.
  \item Pour tout $z \in \C$ tel que $\Im(z) \leqslant \frac{\alpha}2$,
    on a \[ \lvert g(z,x) \rvert \leqslant h(x) = e^{\frac{\alpha \lvert
    x \rvert}{2}} \lvert f(x) \rvert \rho(x) \] qui est intégrable (voir
    l'inégalité utilisant Cauchy-Scwartz)
\end{itemize}
La fonction $F$ est donc holomorphe sur $\B_{\alpha}$ et coïncide sur
$\R$ avec $\hat{\phi}$. On peut donc calculer les dérivées de $F$ : \[
  \forall z \in \B_{\alpha}, F^{(n)}(z)  = (-i)^n \int_I x^n e^{-izx}
f(x) \rho(x) \diff x . \] Ainsi, on obtient, en prenant la valeur en 0 :
\[ F^{(n)}(0) = (-i)^n \int_I x^n  f(x) \rho(x) \diff x = \langle f, g_n
\rangle_{\rho} \] où $g_n \colon x \mapsto x^n$ sur $I$.

Supposons maintenant que $f \in \Vect(x^n)^{\perp}$, c'est-à-dire que
pour tout $n \in \N, \langle f, g_n \rangle_{\rho} = 0$. On obtient
alors \[ F^{(n)}(0) = 0. \]
L'unicité du développement en série entière d'une fonction holomorphe
montre que $F \equiv 0$ au voisinage de 0. Le théorème de prolongement
analytique implique alors que $F \equiv 0$ sur le connexe $\B_{\alpha}$
entier donc en particulier sur l'axe des réels. On en déduit que
$\hat{\phi} \equiv 0$. Comme $\phi$ est une fonction intégrable,
l'injectivité de la transformée de Fourier implique que $\phi \equiv 0$.
Comme $\rho(x) > 0$, on en déduit que $f(x) =0 $ pour tout $x \in I$.
On a donc montré qu'une fonction orthogonale à tous les polynômes est
nulle. On a donc bien que les polynômes orthogonaux forment une base
hilbertienne de $L^2(I,\rho)$





\end{document}

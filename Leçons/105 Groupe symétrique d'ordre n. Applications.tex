\documentclass[a4paper,12pt,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{amsthm}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}

\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{9 octobre 2013}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}


\newcommand{\K}{\mathbf{K}}
\newcommand{\ssi}{si, et seulement si }

\begin{document}
\maketitle

\section{Généralités}

\begin{proposition}
  L'ensemble des permutations de $E$ dans $E$ forme un groupe pour la loi
  de composition, noté $S(E)$.\\
  Si $E$ est fini de cardinal $n$, $S(E)=S_n$ est fini de cardinal $n!$.
\end{proposition}

\begin{definition}
  Le support d'une permutation $\sigma\in S_n$ est
  $\mathrm{Supp}(\sigma) = \{i \in N_n | \sigma(i)\neq i \}$
\end{definition}

\begin{proposition}
  Deux permutations à support disjoints commuttent.
\end{proposition}
On peut considérer l'action de $S_n$ sur $N_n = \{1,…,n\}$.

\begin{definition}
  On dit que $\sigma\in S_n$ est un cycle si l'unique $\sigma$-orbite
  $\mathcal{O}$ des classes à gauche de $\sigma$ existe et que
  $\#\mathcal{O}=q>1$. $q$ est alors la longueur du cycle, noté
  $q$-cycle, d'ordre $q$. Un 2-cycle est une transposition.
\end{definition}

\begin{theoreme}
  Pour deux $p$-cycles $\sigma$ et $\sigma' \in S_n$, il existe
  $u\in S_n$ tel que $\sigma'=u\sigma u^{-1}$ (deux $p$-cycles sont
  conjugués dans $S_n$).
\end{theoreme}


\begin{proposition}
  \begin{enumerate}
    \item Les permutations se décomposent comme produit de cycles
      disjoints, de façon unique, à l'ordre près. ($S_n$ est engendré
      par les cycles)
    \item Si $n>1$, toute permutation de $S_n$ est un produit de
      transpositions.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  Si $n>1$, $S_n$ est engendré par :
  \begin{enumerate}
    \item les $n-1$ transpositions $(1,i)$, avec $2\leq i \leq n$
    \item les $n-1$ transpositions $(i,i+1)$, avec $1\leq i \leq n-1$
    \item $(1,2)$ et le $n$-cycle $(1,2,…,n)$
  \end{enumerate}
\end{proposition}

\section{Propriétés remarquables}

\begin{definition}
  On définit la signature d'une permutation par \[ \varepsilon =
    \prod_{1\leq i<j\leq n}{\frac{\sigma(i)-\sigma(j)}{i-j}} \]
\end{definition}

\begin{proposition}
  Les propriétés de la signature sont :
  \begin{enumerate}
    \item $\varepsilon(Id) = 1$
    \item $\varepsilon(\tau) = -1$, avec $\tau$ une transposition.
    \item $\varepsilon(\sigma\tau) = -\varepsilon(\sigma)$
    \item $\varepsilon$ est un morphisme de groupe dans $\{-1,1\}$.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  $A_n = \mathrm{Ker}(\varepsilon)$ est un sous-groupe distingué de
  $S_n$, d'indice 2 et de cardinal $\frac{n!}2$
\end{proposition}

\begin{proposition}
  Pour $n\geq 3$,
  \begin{enumerate}
    \item $A_n$ est engendré par les permutations $(1,i)(1,j)$, où
      $2\leq i < j \leq n$ ;
    \item $A_n$ est engendré par les 3-cycles de la forme $(1,2,i)$,
      avec $3\leq i\leq n$ ;
    \item $A_n$ est engendré par les éléments $s^2$, $s\in S_n$.
  \end{enumerate}
\end{proposition}

\begin{definition}
  Un groupe est simple s'il est distinct de $\{e\}$ et si ses seuls
  sous-groupes distingués sont $\{e\}$ et lui-même.
\end{definition}

\begin{lemme}
  Pour $n\geq3$, si $H$ est un sous-groupe distingué de $A_n$ contenant
  un 3-cycle, alors $H=A_n$
\end{lemme}

\begin{proposition}
  $A_n$ est simple si $n\notin\{1,2,4,\}$.
\end{proposition}

\section{Application}

\begin{theoreme}
  Tout groupe $G$ est isomorphe à un sous-groupe de $S(G)$ (Cayley)
\end{theoreme}

\begin{theoreme}
  Si $\sigma$ et $\tau$ sont deux permutations conjugués, alors leurs
  matrices de permutations sont sembables (Brauer)
\end{theoreme}

\textsc{Application} : Polynômes symétriques (ils sont définis comme
invariant par l'action d'un sosu-groupe de $\mathfrak{S}$)


\end{document}

\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{$\C$ est algébriquement clos}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}\label{theo:rel_coeff_rac}
  Soient $n \in \N^*$ et $(\alpha_1,\dots,\alpha_n) \in \K^n$, et $P \in
  \K[X]$ : \[ P = a_nX^n + \cdots + a_1X + a_0, a_n \neq 0 . \]
  Pour $1 \leqslant i \leqslant n$, on pose $\sigma_i = \Sigma_i(\alpha_1,
  \dots,\alpha_n)$. Les conditions suivantes sont équivalentes :
  \begin{enumerate}
    \item $(\alpha_1,\dots,\alpha_n)$ est un système de racines du polynôme
      $P$.
    \item Pour $1 \leqslant i \leqslant n$, on a $\sigma_i = (-1)^ia_{n-i} /
      a_n$.
  \end{enumerate}
\end{theoreme}

\begin{lemme}\label{lem:identif_sous_corps}
  Soient $P \in \K[X]$ un polynôme irréductible, $(P)$ l'idéal de $\K[X]$
  engendré par $P$ et $\Lc = \K[X]/(P)$. Alors, $\Lc$ est un corps
  commutatif ; on peut identifier $\K$ a un sous-corps de $\Lc$, et il
  existe $\alpha \in \Lc$ tel que $P(\alpha) = 0$.
\end{lemme}

\begin{proposition}\label{prop:scinde_surcorps}
  Soit $P \in \K[X]$ un polynôme non constant. Il existe un corps commutatif
  $\Lc$ possédant les propriétés suivantes :
  \begin{enumerate}
    \item $\K$ s'identifie à un sous-corps de $\Lc$.
    \item Considéré comme élément de $\Lc[X], P$ est scindé sur $\Lc$.
  \end{enumerate}
\end{proposition}

\begin{theoreme}[Théorème de D'Alembert]\label{theo:scinde_sur_C}
  Le corps $\C$ des nombres complexes est algébriquement clos.
\end{theoreme}

\begin{proof}[\ref{theo:rel_coeff_rac}]~\\[-6mm]
  \begin{itemize}[label=---]
    \item $(i) \implies (ii)$ L'écriture des polynômes symétriques
      élémentaires permet d'obtenir \[ \prod_{i=1}^n (X - \alpha_i) = X^n +
      \sum_{k=0}^{n-1} (-1)^{n-k}\sigma_{n -k}X^k \] et dire que
      $(\alpha_1,\dots,\alpha_n)$ est un système de racines de $P$ signifie
      que \[ P = \sum_{k=0}^n a_kX^k = a_n\prod_{i=1}^n (X - \alpha_i) . \]
      L'uncité des coefficients d'un polynôme permet de conclure.
    \item $(ii) \implies (i)$ Si $(ii)$ est vérifiée, $(i)$ l'est aussi car
      \[ \prod_{i=1}^n (X - \alpha_i) = X^n + \sum_{k=0}^{n-1}
      \frac{a_k}{a_n} X^k . \]
  \end{itemize}
\end{proof}

\begin{proof}[\ref{lem:identif_sous_corps}]
  Comme $P$ est irréductible dans $\K[X]$, $(P)$ est un idéal maximal de
  $\K[X]$, et $\Lc$ est un corps commutatif. Soit $\phi \colon \K[X] \to
  \Lc$ la surjection canonique. La restriction de $\phi$ à $\K$ est
  injective, donc $\K$ s'identifie à un sous-corps de $\Lc$. Enfin, si
  $\alpha = \phi(X)$, on a $P(\alpha) = 0$.
\end{proof}

\begin{proof}[\ref{prop:scinde_surcorps}]~\\[-6mm]
  \begin{itemize}[label=---]
    \item Posons $n \deg(P)$. Raisonnons par récurrence sur $n$, le cas $n =
      1$ étant clair. Supposons $n \geqslant 2$, et soit $Q$ un facteur
      irréductible de $P$. D'après \ref{lem:identif_sous_corps}, il existe
      un corps commutatif $\mathbf{M}$ et $\alpha \in \mathbf{M}$ tels que :
      \begin{itemize}[label=\textbullet]
        \item $\K$ s'identifie à un sous-corps de $\mathbf{M}$.
        \item $Q(\alpha) = 0$.
      \end{itemize}
      Il existe donc $P_1 \in \mathbf{M}[X]$ tel que $P = (X - \alpha)P_1$.
      Comme $\deg(P_1) < n$, il existe un corps commutatif $\Lc$ tel que :
      \begin{itemize}[label=\textbullet]
        \item $\mathbf{M}$ s'identifie à un sous-corps de $\Lc$
        \item $P_1$ est scindé sur $\Lc$.
      \end{itemize}
      Ainsi, $\K$ s'identifie à un sous-corps de $\Lc$ et $P$ est scindé sur
      $\Lc$.
  \end{itemize}
\end{proof}

\begin{proof}[\ref{theo:scinde_sur_C}]
  Soit $P \in \C[X]$. Quitte à remplacer $P$ par $F = P\overline{P}$, on
  peut supposer $P \in \R[X]$. On peut aussi supposer $P$ unitaire.

  Écrivons le degré de $P$ sous la forme $ d = 2^nq$. Montrons par
  récurrence sur $n$ : $(\mathcal{P}_n)$ : « tout polynôme $P \in \R[X]$
  de degré $2^nq$ avec $q$ impair admet une racine dans $\C$.»

  $(\mathcal{P}_0)$ est vraie : en effet, si $P \in \R[X]$ est de degré
  impair, on a $\lim_{x \to -\infty} = -\infty$, $\lim_{x \to +\infty} =
  +\infty$ et $P$ continue, donc d'après le théorème des valeurs
  intermédiaires, $P(x) = 0$ admet au moins une solution.

  Supposons $(\mathcal{P}_n)$ vraie. Soit $\K$ une extension de $\C$ telle
  que $P$ s'écrive $P = \prod_{i = 1}^d (X - x_i)$ avec $x_i \in \K$.

  Soit $c \in \R$. Si $i \leqslant j$, on pose $y_{ij} = x_i + x_j +
  cx_ix_j \in \K$. Ces éléments sont au nombres de $\frac{d(d+1)}{2} =
  2^{n-1}q(d+1) = 2^{n-1}q'$, avec $q'$ impair.

  Regardons $Q(X)= \prod_{i\leqslant j} (X - y_{ij})$. Les coefficients de
  $Q$ s'expriment comme des polynômes symétriques élémentaires en les
  $y_{ij}$ pour $i \leqslant j$. Ce sont donc des éléments de $\R$, comme
  les coefficeients de $Q_c$. D'autre part, $\deg(Q_c) = 2^{n-1}q(d+1) =
  2^{n-1}q'$ où $q'$ est impair. D'après l'hypothèse de récurrence, $Q_c$
  possède une racine $z_c \in \C$ et \[ y_c = y_{i(c),j(c)} = x_{i(c)} +
  x_{j(c)} + cx_{i(c)} x_{j(c)} . \] Le corps $\R$ étant infini, il existe
  des réels distincts $c$ et $d$ tels que $i(c) = i(d) = r$ et $j(c) = j(d)
  = s$ de sorte que \[ x_r + x_s + cx_rx_s = y_c \in \C \text{ et } x_r +
  x_s + dx_sx_r = y_d \in \C. \] Posant $u = x_r + x_s$ et $v = x_rx_s$, on
  a $u,v \in \C$. On en déduit que $x_r$ et $x_s$ sont racines de $X^2 - uX
  + v \in \C[X]$. Or une telle équation possède toujours des solutions dans
  $\C$.
\end{proof}

\section*{Heuristique de la preuve}

Revoir les détails de la preuve dans Gourdon pp.84 et 85 ainsi que 63

\end{document}

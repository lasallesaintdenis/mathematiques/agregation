\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\parindent0pt

\usepackage{babel}

\title{109 -- Exemples de parties génératrices d'un groupe. Application}
\author{Vincent-Xavier \bsc{Jumel}}
\date{19/09/2016}

\begin{document}
\maketitle

\section{Généralités}

\begin{definition}
  On appelle \emph{groupe} tout couple $(G,*)$ vérifiant
  \begin{enumerate}[label=(\roman*)]
    \item la loi $*$ est associative ;
    \item $(G,*)$ possède un élément neutre ;
    \item tout élement de $G$ possède un symétrique dans $G$ pour la loi
      $*$.
  \end{enumerate}
\end{definition}

\begin{proposition}
  $H$ est un sous-groupe de $G$ si et seulement si $H \neq \varnothing$
  et $\forall (x,y) \in H^2,\ xy^{-1} \in H$.
\end{proposition}

\begin{theoreme}
  Soient $G$ un groupe et $(H_i)_{i\in I}$ une famille de sous-groupes
  de $G$. Alors l'intersection $H = \bigcap_{i\in I} H_i$ est un
  sous-groupe de $G$.
\end{theoreme}
\begin{proof}[Preuve]
  On a : $\forall i \in I,\ e\in H_i$ ; d'où $e \in H \neq \varnothing$

  On a : $\forall i \in I,\ \forall (x,y) \in H_i^2,\ xy^{-1} \in H_i$
  d'où $\forall (x,y) \in H^2,\ xy^{-1} \in H$.
\end{proof}

\begin{definition}
  On appelle \emph{sous-groupe engendré par une partie $A$}, et on note
  $gr(A)$, le plus petit sous-groupe, au sens de l'inclusion, contenant
  $A$.
\end{definition}

\begin{remarque}
  $gr(A)$ existe pour tout $A \in \mathscr{P}(G)$ avec comme cas
  particuliers $gr(\varnothing) = \{e\}$
\end{remarque}

\begin{definition}
  Si $gr(A) = G$, on dit que $A$ est une partie génératrice de $G$.
\end{definition}

\begin{proposition}
  $gr(A)$ est l'intersection de tous les sous-groupes de $G$ contenant
  $A$.
\end{proposition}
\begin{proof}
\end{proof}

\begin{theoreme}
  Soit $G$ un groupe d'élément neutre $e$, et $A$ une partie de $G$.
  Désignons par $H$ l'ensemble des éléments de $G$ qui peuvent s'écrire
  \[ a_1^{\varepsilon_1}a_2^{\varepsilon_2}\cdots a_n^{\varepsilon_n}\
    \text{avec}\ a_i \in A\ \text{et} \ \varepsilon_n \in \{ -1 ; +1\}
  \] en convenant que $H = \{e\}$ si $A = \varnothing$.

  Alors $H$ est le sous-groupe engendré par $A$.
\end{theoreme}
\begin{proof}
  $H$ est non vide, car $A \subset H$ et si $A = \varnothing$, alors $H
  = \{e\}$. De plus, si \[ x = a_1^{\varepsilon_1}a_2^{\varepsilon_2}
  \cdots a_n^{\varepsilon_n} \ \text{et } y =
  b_1^{\omega_1}b_2^{\omega_2}\cdots b_n^{\omega_n} \] sont deux
  éléments de $H$, alors on peut écrire $xy^{-1} =
  a_1^{\varepsilon_1}a_2^{\varepsilon_2}\cdots a_n^{\varepsilon_n}
  b_1^{-\omega_1}b_2^{-\omega_2}\cdots b_n^{-\omega_n}$ qui est un
  élément de $H$. Ainsi, $H$ est un sous-groupe de $G$ contenant $A$.

  Réciproquement soit $L$ un sous-groupe de $G$ contenant $A$. Pour
  toute famille finie $(a_1,a_2,\dots,a_n)$ d'éléments de $A$,
  $a_1^{-1}, a_2^{-1}, \dots, a_n^{-1}$ sont des éléments de $L$ et il
  en est de même pour tout produit de la forme
  $a_1^{\varepsilon_1}a_2^{\varepsilon_2}\cdots a_n^{\varepsilon_n}$.
  Ainsi $H \subset L$ et $H$ est bien le plus petit sous-groupe de $G$
  contenant $A$.
\end{proof}

\begin{remarques}
  \begin{enumerate}[label=\alph*)]
    \item La réunion de sous-groupe d'un groupe $G$ n'est en général pas
      un sous-groupe de $G$.
    \item L'ensemble des sous-groupes de $G$, ordonné par inclusion, est
      un treillis. En effet, à tout couple $(H_1,H_2)$ de sous-groupes
      de $G$ on peut associer \[\inf(H_1,H_2) = H_1 \cap H_2 \ \text{et}
      \ \sup(H_1,H_2) = gr(H_1 \cup H_2) \]
  \end{enumerate}
\end{remarques}

\begin{definition}
  On qualifie de \emph{groupe monogène} tout groupe ayant une partie
  génératrice réduite à un élément et de \emph{groupe cyclique} tout
  groupe monogène fini.
\end{definition}

\begin{definition}
  On appelle \emph{groupe symétrique de $E$} et on note
  $\mathfrak{S}(E)$ le groupe de l'ensemble des permutations de $E$,
  c'est-à-dire des bijections de $E$ dans $E$, muni de la loi $\circ$.
\end{definition}

\begin{proposition}
  Si $\# E = n < +\infty$, alors $\# \mathfrak{S}(E) = n!$
\end{proposition}

\section{Groupe monogène, groupe fini}

\begin{theoreme} Soit $G$ un groupe monogène. S'il est fini, il est
  isomorphe à $\mathbf{Z}$ ; s'il est fini, d'ordre $n$, il est
  isomorphe à $\mathbf{Z}/n\mathbf{Z}$. Dans les deux cas, il est
  abélien.
\end{theoreme}
\begin{proof}
  Soit $a$ un générateur de $G$, dont tous les éléments sont de la forme
  $a^p$, ($p\in \mathbf{Z}$). De $a^{p+p'} = a^pa^{p'}$ on en déduit que
  l'application $f$ de $\mathbf{Z}$ dans $G$ définie par $p \mapsto a^p$
  est un morphisme surjectif de groupes ; $G$ est donc isomorphe à
  $\mathbf{Z}/\ker f$, où $\ker f$, qui est un sous-groupe de
  $\mathbf{Z}$ est la forme $k\mathbf{Z}$.

  Si $G$ est infini, il en est de même de $\mathbf{Z}/\ker f$ et donc
  $\ker f = \{0\}$. $f$ est donc injectif et les éléments $a^p$, ($p\in
  \mathbf{Z}$) sont deux à deux distincts ; à la fois surjectif et
  injectif, $f$ est un isomorphisme de $G$ sur $\mathbf{Z}$.

  Si $G$ est fini, il en est de même de $\mathbf{Z}/\ker f$ et donc $k =
  n$. $G \cong \mathbf{Z}/n\mathbf{Z}$ ; on a $G = \{a^0, a^1, \dots,
  a^{n-1}\}$ et $n$ est le plus petit entier $m$ strictement positif tel
  que $a^m = e$.

  Enfin, $G$ isomorphe à un groupe abélien est lui-même abélien.
\end{proof}
\begin{definition}
  Soit $G$ un groupe, d'élément neutre $e$, et $a$ un élément de $G$; le
  sous-groupe engendré par $a$, $gr(a)$ est un groupe monogène.
  \begin{itemize}
    \item Si $gr(a)$ est infini, on dit que $a$ est un élément
      d'\emph{ordre infini} de $G$.
    \item Si $gr(a)$ est fini, son ordre est appelé \emph{ordre de $a$
      dans $G$} et noté $\omega(a)$.
  \end{itemize}
\end{definition}

\begin{proposition}
  Dans un groupe fini, tout élément est d'ordre fini et son ordre divise
  l'ordre du groupe.
\end{proposition}
\begin{proof}
  Le résultat provient de la définition et du théorème 12.
\end{proof}

\begin{corollaire}
  Soit $G$ un groupe fini d'ordre $n$, d'élément neutre $e$. Tout $a$ de
  $G$ vérifie $a^n = e$.
\end{corollaire}
\begin{proof}
  Soit $\omega$ l'ordre de $a$ dans $G$. ; on a $a^{\omega} = e$ ;
  d'après la proposition précédente, $n = q\omega$ donc $a^n = e^q = e$.
\end{proof}

\begin{corollaire}
  Tout groupe $G$ d'ordre $p$ premier est cyclique (donc abélien) ; il
  est engendré par l'un quelconque de ses éléments autres que l'élément
  neutre $e$.
\end{corollaire}
\begin{proof}
  Soit $a\in G \setminus\{e\}$. On a $\omega(a) > 1$ ; comme $\omega(a)$
  divise $p$ premier, on a $\omega(a) = p$ et $gr(a) = G$.
\end{proof}
\begin{corollaire}
  Le produit de deux groupes finis $G$ et $G'$ est cyclique si et
  seulement si ces deux groupes sont cycliques, et d'ordres $n$ et $m$
  premiers entre eux. En particulier $\mathbf{Z}/n\mathbf{Z} \times
  \mathbf{Z}/m\mathbf{Z}$ est cyclique et donc isomorphe à
  $\mathbf{Z}/nm\mathbf{Z}$ si et seulement si $n$ et $m$ sont premiers
  entre eux.
\end{corollaire}
\begin{proof}
  Soient $x\in G$ et $x'\in G'$, d'ordre $\omega$ et $\omega'$
  (diviseurs de $n$ et $m$) ; $(x,x')^q = (e,e')$ s'écrivant : $(x^q =
  e) \wedge (x'^q = e')$, l'ordre de $(x,x') \in G \times G'$ est le
  ppcm de $\omega$ et $\omega'$ ; il s'agit de l'ordre $nm$ de $G \times
  G'$ si et seulement si : \[ \omega = n \wedge \omega' = m \wedge
  \mathop{\mathrm{ppcm}}(n,m) = nm.\]
\end{proof}

\begin{exemple}
  \textsc{Les groupes d'ordre 4} : l'un d'eux est $V_4 =
  \mathbf{Z}/2\mathbf{Z} \times \mathbf{Z}/2\mathbf{Z}$, non cyclique,
  non abélien qui est dit \emph{groupe de Klein}.

  Inversement, soit $G = \{e, a_1, a_2, a_3\}$ un groupe d'ordre 4 non
  cyclique. Les $a_i$ sont d'ordre 2 et on $a_i^2 = e$ ; pour $i \neq
  j$, $a_ia_j \notin \{ a_k^2, a_ie, ea_j \}$ donc $a_ia_j = a_k$. Les
  groupes non cycliques d'ordre 4 sont donc deux à deux isomorphes et
  donc isomorphes à $V_4$.
\end{exemple}

\section{Applications}

\begin{definition}[Module]
\end{definition}

\begin{proposition}
  Les sous-modules d'un $A$-module sont les sous-groupes stables pour la
  loi externe.
\end{proposition}

\begin{definition}
  On appelle \emph{sous-modules} de $E$ \emph{engendré par $X$} le plus
  petit sous-module de $E$ contenant $X$.
\end{definition}

\begin{remarque}
  Cette définition est analogue à la précédent de sous-groupe et conduit
  à quelques propriétés analogues.
\end{remarque}

\end{document}
